﻿#ifndef SCROLLTEXTWIDGET_H
#define SCROLLTEXTWIDGET_H

#include <QWidget>
#include <QLabel>

namespace Ui {
class ScrollTextWidget;
}

class ScrollTextWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScrollTextWidget(QWidget *parent = 0);
    void setScrollText(const QStringList &text);
    ~ScrollTextWidget();

    void testTemp();

    /*!
     * \brief killTimerId
     * 中止定时器
     */
    void killTimerId();

    /*!
     * \brief startTimerId
     * 启用定时器
     */
    void startTimerId();

private:
    Ui::ScrollTextWidget *ui;
    int mTimerId;
    void timerEvent(QTimerEvent *e);
    void showEvent(QShowEvent *e);
    int mX;
    int mTextIndex;
    int mTotalItem;
    QStringList mTextList;

    QList<QLabel*> mScrollLab;
};

#endif // SCROLLTEXTWIDGET_H
