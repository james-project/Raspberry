#include "scrolltextwidget.h"
#include "ui_scrolltextwidget.h"
#include <QTimerEvent>
#include <QDebug>


ScrollTextWidget::ScrollTextWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScrollTextWidget)
{
    ui->setupUi(this);
    mTimerId = this->startTimer(10);//30
    mTextIndex = 0;
    mTotalItem = 1;
    ui->label->setFont(QFont("DejaVu Sans",25));
}

void ScrollTextWidget::testTemp()
{
    for(int lop=0; lop<mScrollLab.size(); lop++)
    {
        delete mScrollLab.at(lop);
    }
    mScrollLab.clear();

#define space "                                            "
    for(int lop=0; lop<mTextList.size(); lop++)
    {
        QLabel *lab = new QLabel(mTextList.at(lop)+space,this);
        lab->setFont(QFont("DejaVu Sans",25));
        lab->setStyleSheet("color: rgb(255, 0, 0);");
        mScrollLab.append(lab);
        lab->show();
    }

    QFontMetrics fontMetrics(QFont("DejaVu Sans",25));

    int offset = this->width(),width;
    for(int lop=0; lop<mTextList.size(); lop++)
    {
        if(!mScrollLab.at(lop)->text().isEmpty())
        {
            mScrollLab.at(lop)->move(offset,0);
            width = fontMetrics.width(mScrollLab.at(lop)->text());
            mScrollLab.at(lop)->resize(fontMetrics.width(mScrollLab.at(lop)->text()),this->height());
            offset += width;
        };
    }
}

void ScrollTextWidget::killTimerId()
{
    this->killTimer(mTimerId);
}

void ScrollTextWidget::startTimerId()
{
    mTimerId = this->startTimer(10);
}

void ScrollTextWidget::setScrollText(const QStringList &list)
{
    mTextIndex = 0;
    mTextList = list;
    if(mTextList.isEmpty())
    {
//        mTextList.append(tr("Welcome !"));
        mTextList.append(tr(" "));
    }

    mTotalItem = mTextList.size();

    int tmp = 0;
    QString tmpStr;
    for(int lop=mTextList.size(); lop<6; lop++)
    {
        if(tmp >= mTextList.size())
        {
            tmp = 0;
        }
        tmpStr = mTextList.at(tmp);
        mTextList.append(tmpStr);
        tmp ++;
    }

//    qDebug()<<"\n\n\n\nsetScrollText====="<<mTextList;

    testTemp();
}

void ScrollTextWidget::showEvent(QShowEvent *e)
{
    QWidget::showEvent(e);
//    mX = this->width();
//    ui->label->move(this->width(),0);
}

void ScrollTextWidget::timerEvent(QTimerEvent *e)
{
#if 0
//    if(e->timerId()==mTimerId)
//    {
//        static int delTimes = 0;

//        QFontMetrics fontMetrics(QFont("DejaVu Sans",25));
//        int textWid=fontMetrics.width(mTextList.at(mTextIndex));

//        if(delTimes == 0)
//        {
//            mX-=1;
//            if(mX<-textWid)
//            {
//                mX = this->width();
//                mTextIndex++;
//                if(mTextIndex>= mTotalItem)
//                {
//                    mTextIndex= 0;
//                }
//                delTimes = 10;
//            }
//            ui->label->move(mX,0);
//        }
//        else
//        {
//            delTimes -= 1;
//            if(delTimes <= 1)
//            {
//                ui->label->setText(mTextList.at(mTextIndex));
//            }
//        }
//    }
#endif

    if(e->timerId()==mTimerId)
    {
        QFontMetrics fontMetrics(QFont("DejaVu Sans",25));
        int size = mTextList.size();
        for(int lop=0; lop<size; lop++)
        {
            int textWid=fontMetrics.width(mScrollLab.at(lop)->text());
            int currposx = mScrollLab.at(lop)->x();


            if(currposx < -textWid)
            {
                if(lop == 0)//是0，在move到size()-1后
                {
                    int lastWidth = fontMetrics.width(mScrollLab.at(size-1)->text());
                    int lastPosX = mScrollLab.at(size-1)->x();

                    if(lastPosX + lastWidth < this->width())
                    {
                        //qDebug()<<lop<<"==move=00="<<lastWidth<<lastPosX<<"|go to|"<<lastWidth+lastPosX;
                        mScrollLab.at(lop)->move(this->width(),0);
                    }
                    else
                    {
                        //qDebug()<<lop<<"==move=01="<<lastWidth<<lastPosX<<"|go to|"<<lastWidth+lastPosX;
                        mScrollLab.at(lop)->move(lastPosX + lastWidth,0);
                    }
                }
                else//移动到前一序号坐标后面
                {
                    int lastWidth = fontMetrics.width(mScrollLab.at(lop-1)->text());
                    int lastPosX = mScrollLab.at(lop-1)->x();
                    if(lastPosX + lastWidth < this->width())
                    {
                        //qDebug()<<lop<<"==move=22="<<lastWidth<<lastPosX<<"|go to|"<<lastWidth+lastPosX;
                        mScrollLab.at(lop)->move(this->width(),0);
                    }
                    else
                    {
                        //qDebug()<<lop<<"==move=23="<<lastWidth<<lastPosX<<"|go to|"<<lastWidth+lastPosX;
                        mScrollLab.at(lop)->move(lastPosX + lastWidth,0);
                    }
                }
            }
            else
            {
                mScrollLab.at(lop)->move(currposx-1,0);
            }
        }
    }
}

ScrollTextWidget::~ScrollTextWidget()
{
    delete ui;
}
