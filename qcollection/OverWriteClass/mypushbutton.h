#ifndef MYPUSHBUTTON_H
#define MYPUSHBUTTON_H
#include <QTimer>
#include <QPushButton>
class MyPushButton : public QPushButton
{
    Q_OBJECT
public:
    explicit MyPushButton(QWidget *parent = 0);
private:
    bool eventFilter(QObject *, QEvent *);
    void  mousePressEvent(QMouseEvent *);
    void  mouseReleaseEvent(QMouseEvent *);
signals:
    void longclicked();
    void clicked();
private slots:
    void timerTimeout();
    void longPress();
private:
    bool mIsDown;
    QTimer mTimer;
};

#endif // MYPUSHBUTTON_H
