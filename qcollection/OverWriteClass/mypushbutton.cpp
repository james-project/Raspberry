#include "mypushbutton.h"
#include <QWidget>
#include <QKeyEvent>
#include <QMouseEvent>
#include "common.h"
#include "tdebug.h"
MyPushButton::MyPushButton(QWidget *parent) :
    QPushButton(parent)
{
    this->installEventFilter(this);
    mIsDown=false;
    mTimer.setInterval(1000);
    mTimer.setSingleShot(true);
    connect(&mTimer,SIGNAL(timeout()),this,SLOT(timerTimeout()));
}

void MyPushButton::timerTimeout()
{
    if(mIsDown)
    {
        emit longclicked();
    }
}
bool MyPushButton::eventFilter(QObject *obj, QEvent *event)
{
    QEvent::Type type;
    type=event->type();
    if(this->isEnabled())
    {
        switch((unsigned int)type)
        {
        case QEvent::MouseButtonPress:
            mIsDown=true;
            if(mTimer.isActive()==false)
            {
                mTimer.start();
            }
            break;
        case QEvent::MouseButtonRelease:
            if(mTimer.isActive())
            {
                mTimer.stop();
                emit clicked();
            }
            mIsDown=false;
            break;
        }
    }
    return QPushButton::eventFilter(obj,event);
}
void MyPushButton::mousePressEvent(QMouseEvent *event)
{
    QPushButton::mousePressEvent(event);
}
void MyPushButton::longPress()
{

}
void MyPushButton::mouseReleaseEvent(QMouseEvent *event)
{
    QPushButton::mouseReleaseEvent(event);
}
