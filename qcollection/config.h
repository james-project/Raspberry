#ifndef CONFIG_H
#define CONFIG_H
#define NETWORK_EN
#define USE_MYSQL

#undef USE_MYSQL

#ifdef USE_MYSQL
    #define AUTO_INCREMENT "auto_increment"
    #define SQL_CheckDayOfWeek(week_days) "SUBSTR("#week_days" , dayofweek(now()) , 1) = 1"
    #define SQL_DateTime_Now "NOW()"
    #define SQL_Time_now "localtime()"
    #define SQL_MAX "GREATEST"
    #define SQL_MIN "LEAST"
#else
    #define AUTO_INCREMENT "autoincrement"
    #define SQL_CheckDayOfWeek(week_days) "SUBSTR("#week_days" , strftime('%w','now')+1 , 1) = '1'"
    #define SQL_DateTime_Now "datetime('now')"
    #define SQL_Time_now "time('now')"
    #define SQL_MAX "MAX"
    #define SQL_MIN "MIN"
#endif
#endif // CONFIG_H
