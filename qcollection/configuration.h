#ifndef CONFIGURATION_H
#define CONFIGURATION_H
enum CONTROL_TYPE
{
    NONE,//无控件获得焦点
    BADPRODUCT_NUM,
    WELDSTANDARD_NO, //标准规范号
    PRE_TIME,
    ADD_TIME,
    SLOWRISE_TIME,
    WELD_1_TIME, //焊接1时间
    COOL_TIME,   //冷却时间
    WELD_2_TIME, //焊接2时间
    HOLD_TIME,   //维持时间
    STOP_TIME,   //停止时间
    WELDPRE_NUM, //焊接预制数
    WELD_1_V,
    WELD_2_V
};

#endif // CONFIGURATION_H
