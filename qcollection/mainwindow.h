#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QMessageBox>
#include <QFile>
#include <QHBoxLayout>
#include "common.h"
#include "ProductionOrder/productionordermanagement.h"
#include "Device/DeviceState/devicestate.h"

class MainWidget;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    ~MainWindow();
    static MainWindow * getMainWindow();
    void startInit();
    QLayout *mainLayout();

    /*!
     * \brief initDeviceWidgetSelect
     * init the device widget from the setting menu;
     */
    void initDeviceWidgetSelect();
    /*!
     * \brief keyPressEvent : this is for barcode and QR-code press scan;
     * \param keyEvent
     */
    void keyPressEvent(QKeyEvent* keyEvent);
    /*!
     * \brief keyReleaseEvent : this is for barcode and QR-code rezlease scan;
     * \param keyEvent
     */
    void keyReleaseEvent(QKeyEvent* keyEvent);
    /*!
     * \brief showBarcodeText
     * parse the barcode text
     */
    virtual void showBarcodeText(  QString &);
protected:
    void changeEvent(QEvent *e);

private slots:
    void openSetting();
private:
    explicit MainWindow(QWidget *parent = 0);
    static MainWindow *mMainWindow;
    Ui::MainWindow *ui;

    QString m_barcode;    

signals:
    void sendOperator( QString , QString, DeviceState::DeviceStion );
    void sendQC(QString , QString, DeviceState::DeviceStion);
    void sendFirstCheck(QString , QString, DeviceState::DeviceStion);
    void sendMainTain(QString , QString, DeviceState::DeviceStion);

    void sendMaterial(QString, QString, QString);
    void sendGongZhang(QString, QString);
    void sig_GongZhuangOk();
public:
   MainWidget *mMainWidget;
   //QString _strBarcode;
    static ProductionVariant  _productionVariant;

};

#endif // MAINWINDOW_H
