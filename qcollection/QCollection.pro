



include(Modbus/Modbus.pri)
include(http/http.pri)

#-------------------------------------------------
#
# Project created by QtCreator 2010-07-14T17:54:25
#
#-------------------------------------------------
QT       += serialport
QT       += xml
#if QT_VERSION >= 0x050000
QT       += core gui network sql widgets
greaterThan(QT_MAJOR_VERSION,4): QT += widgets

DEFINES += QT_NO_DEBUG_OUTPUT

qcollection.path = $$[QT_INSTALL_EXAMPLES]/linguist/

RC_FILE = myico.rc
TARGET = qcollection
TEMPLATE = app

INCLUDEPATH += $$PWD/Dialog/
INCLUDEPATH += $$PWD/Device/
INCLUDEPATH += $$PWD/Device/HardWare/AmmeterScale/
INCLUDEPATH += $$PWD/Device/Setting/

LIBS += -lwiringPi

SOURCES += main.cpp\
        mainwindow.cpp \
        qextserial/qextserialport.cpp \
    DataBase/database.cpp \
    DataBase/databasequery.cpp \
    DataBase/dbtabbase.cpp \
    DataBase/dbtab_mashwelder.cpp \
    input/numberkey.cpp \
    input/inputdialog.cpp \
    Dialog/mymessagedialog.cpp \
    Dialog/tdialog.cpp \
    Device/mashwelder.cpp \
    Device/Setting/standardparasetting.cpp \
    Device/Setting/weldinfosetting.cpp \
    scrollwarn/scrolltextwidget.cpp \
    login/logindialog.cpp \
    DataBase/dbtab_user.cpp \
    DataBase/dbtab_misc.cpp \
    Device/Setting/contactusset.cpp \
    DataBase/dbtab_warn.cpp \
    Device/Setting/warnrecordquery.cpp \
    Device/Setting/operatoreditorset.cpp \
    Device/Setting/logonrightsedit.cpp \
    Device/HardWare/Gpio/ultrasonicthread.cpp \
    Device/HardWare/TemperatureDetect/temperaturethread.cpp \
    Device/HardWare/AmmeterScale/ammeterscale.cpp \
    Device/PlasticPresses/plasticpresses.cpp \
    Device/PlasticPresses/mainmenu.cpp \
    Device/PlasticPresses/processselection.cpp \
    Device/PlasticPresses/delaytimesetting1.cpp \
    Device/PlasticPresses/delaytimesetting2.cpp \
    Device/PlasticPresses/plasticpressessinfo.cpp \
    Device/PlasticPresses/inputstatus.cpp \
    Device/PlasticPresses/outputstatus.cpp \
    MitsubishiPLC/mitsubishiwnd.cpp \
    OverWriteClass/mypushbutton.cpp \
    Device/DeviceState/devicestate.cpp \
    ProductionOrder/productionordermanagement.cpp \
    Socket/tcpsocket.cpp \
    Device/PunchingMachine/punchingmachine.cpp \
    Device/ReadUsbPort/readusbport.cpp \
    Device/HttpInteractiveData/httpinteractivedata.cpp \
    Device/HttpInteractiveData/mashwelderinteractivedata.cpp \
    Dialog/fullscreenmodaldlg.cpp \
    DataBase/dbtab_productionorder.cpp \
    Device/HardWare/AccelarateScale/accelaratescale.cpp \
    globalvar.cpp \
    SettingReader/settingreader.cpp \
    Setting/setting.cpp \
    Setting/subwidget.cpp \
    Setting/BasicSetting/basicsettingwidget.cpp \
    Setting/subsettingmodel.cpp \
    Setting/settinglistmodel.cpp \
    Setting/subsettingwidget.cpp \
    Setting/mainsettingdelegate.cpp \
    Setting/subsettingdelegate.cpp \
    Setting/subsubsettingdelegate.cpp \
    Dialog/passworddialog.cpp \
    Setting/other/datestring.cpp \
    Setting/other/drawtext.cpp \
    Setting/other/timestring.cpp \
    View/tlistview.cpp \
    Setting/Communication/comsettingwidget.cpp \
    Setting/Communication/serversettings.cpp \
    Device/mainwidget.cpp \
    ProductionOrder/productionlistview.cpp \
    dataview/mymodel.cpp \
    dataview/productionorderlistmodel.cpp \
    dataview/productionorderdelegate.cpp \
    ProductionOrder/reportlistviewfororder.cpp \
    ProductionOrder/orderdetailinfo.cpp \
    scale/scaleamendcalibration.cpp \
    Setting/BasicSetting/electrodesetwidget.cpp \
    deviceidsettings.cpp \
    videoip.cpp \
    lock_dlg.cpp \
    temperaturesettings.cpp \
    DataBase/dbtab_punchingmachine.cpp \
    DataBase/dbtab_plasticpresses.cpp \
    perssiondlg.cpp \
    connectdlg.cpp

HEADERS  += mainwindow.h \
        qextserial/qextserialport_global.h \
        qextserial/qextserialport.h \
    DataBase/database.h \
    DataBase/databasequery.h \
    common.h \
    config.h \
    tdebug.h \
    DataBase/dbtabbase.h \
    DataBase/dbtab_mashwelder.h \
    input/numberkey.h \
    input/inputdialog.h \
    Dialog/mymessagedialog.h \
    Dialog/tdialog.h \
    Device/mashwelder.h \
    Device/Setting/standardparasetting.h \
    Device/Setting/weldinfosetting.h \
    scrollwarn/scrolltextwidget.h \
    login/logindialog.h \
    DataBase/dbtab_user.h \
    DataBase/dbtab_misc.h \
    Device/Setting/contactusset.h \
    DataBase/dbtab_warn.h \
    Device/Setting/warnrecordquery.h \
    Device/Setting/operatoreditorset.h \
    Device/Setting/logonrightsedit.h \
    Device/HardWare/Gpio/ultrasonicthread.h \
    Device/HardWare/TemperatureDetect/temperaturethread.h \
    Device/HardWare/AmmeterScale/ammeterscale.h \
    Device/PlasticPresses/plasticpresses.h \
    Device/PlasticPresses/mainmenu.h \
    Device/PlasticPresses/processselection.h \
    Device/PlasticPresses/delaytimesetting1.h \
    Device/PlasticPresses/delaytimesetting2.h \
    Device/PlasticPresses/plasticpressessinfo.h \
    Device/PlasticPresses/inputstatus.h \
    Device/PlasticPresses/outputstatus.h \
    MitsubishiPLC/mitsubishiwnd.h \
    OverWriteClass/mypushbutton.h \
    Device/DeviceState/devicestate.h \
    ProductionOrder/productionordermanagement.h \
    Socket/tcpsocket.h \
    Device/PunchingMachine/punchingmachine.h \
    Device/ReadUsbPort/readusbport.h \
    Device/HttpInteractiveData/httpinteractivedata.h \
    Device/HttpInteractiveData/mashwelderinteractivedata.h \
    Dialog/fullscreenmodaldlg.h \
    DataBase/dbtab_productionorder.h \
    Device/HardWare/AccelarateScale/accelaratescale.h \
    globalvar.h \
    SettingReader/settingreader.h \
    Setting/setting.h \
    Setting/subwidget.h \
    Setting/BasicSetting/basicsettingwidget.h \
    Setting/subsettingmodel.h \
    Setting/settinglistmodel.h \
    Setting/subsettingwidget.h \
    Setting/mainsettingdelegate.h \
    Setting/subsettingdelegate.h \
    Setting/subsubsettingdelegate.h \
    Dialog/passworddialog.h \
    Setting/other/datestring.h \
    Setting/other/drawtext.h \
    Setting/other/timestring.h \
    View/tlistview.h \
    Setting/Communication/comsettingwidget.h \
    Setting/Communication/serversettings.h \
    Device/mainwidget.h \
    ProductionOrder/productionlistview.h \
    dataview/mymodel.h \
    dataview/productionorderlistmodel.h \
    dataview/productionorderdelegate.h \
    ProductionOrder/reportlistviewfororder.h \
    ProductionOrder/orderdetailinfo.h \
    scale/scaleamendcalibration.h \
    Setting/BasicSetting/electrodesetwidget.h \
    productinfo.h \
    configuration.h \
    deviceidsettings.h \
    videoip.h \
    lock_dlg.h \
    temperaturesettings.h \
    DataBase/dbtab_punchingmachine.h \
    DataBase/dbtab_plasticpresses.h \
    perssiondlg.h \
    connectdlg.h


win32 {
     SOURCES += qextserial/qextserialport_win.cpp
}

unix {
     SOURCES += qextserial/qextserialport_unix.cpp
}

FORMS    += mainwindow.ui \
    input/numberkey.ui \
    input/inputdialog.ui \
    Dialog/mymessagedialog.ui \
    Device/mashwelder.ui \
    Device/Setting/standardparasetting.ui \
    Device/Setting/weldinfosetting.ui \
    scrollwarn/scrolltextwidget.ui \
    login/logindialog.ui \
    Device/Setting/contactusset.ui \
    Device/Setting/warnrecordquery.ui \
    Device/Setting/operatoreditorset.ui \
    Device/Setting/logonrightsedit.ui \
    Device/PlasticPresses/plasticpresses.ui \
    Device/PlasticPresses/mainmenu.ui \
    Device/PlasticPresses/processselection.ui \
    Device/PlasticPresses/delaytimesetting1.ui \
    Device/PlasticPresses/delaytimesetting2.ui \
    Device/PlasticPresses/plasticpressessinfo.ui \
    Device/PlasticPresses/inputstatus.ui \
    Device/PlasticPresses/outputstatus.ui \
    ProductionOrder/productionordermanagement.ui \
    Device/PunchingMachine/punchingmachine.ui \
    Dialog/fullscreenmodaldlg.ui \
    Setting/setting.ui \
    Setting/subsettingwidget.ui \
    Dialog/passworddialog.ui \
    Setting/Communication/serversettings.ui \
    ProductionOrder/productionlistview.ui \
    ProductionOrder/reportlistviewfororder.ui \
    ProductionOrder/orderdetailinfo.ui \
    Setting/BasicSetting/electrodesetwidget.ui \
    deviceidsettings.ui \
    videoip.ui \
    lock_dlg.ui \
    temperaturesettings.ui \
    perssiondlg.ui \
    connectdlg.ui

RESOURCES += \
    images.qrc \
    src.qrc
RC_FILE += myico.rc

OTHER_FILES += \
    myico.rc

TRANSLATIONS +=\

    tr/Language_cn.ts
