#ifndef __TDEBUG_H__
#define __TDEBUG_H__
#define __DEBUG__
#include <QDebug>
#ifdef __DEBUG__
#include <sys/time.h>
#define DDDBUG qDebug( "File: %s, Func: %s,  Line: %d", __FILE__, __FUNCTION__, __LINE__);
#define tDebug qDebug
#define DDDValue(v)  tDebug()<<#v":==="<<v
#define DDDFun tDebug()<<"FUNC ="<<__FUNCTION__
#define DDDFile tDebug()<<"FILE ="<<__FILE__
#define DDDLine tDebug()<<"FILE ="<<__FILE__<<"LINE ="<<__LINE__
#define DDDWarnning DDDBUG; tDebug()<<"TScale Warnning:"
#define DDDStr(text) tDebug()<<#text
#define DDDValue2(t,v) tDebug()<<#t<<"==="<<v
#define DDDHex(v) qDebug(#v"0x%02x ",v)
#define DDDTime(str)\
do{\
    struct timeval time; \
    gettimeofday(&time,NULL);\
    qDebug()<<#str<<time.tv_sec<<time.tv_usec;\
}while(0)
#else
#define DDDBUG
#define tDebug
#define DDDValue(v)
#define DDDFun
#define DDDFile
#define DDDLine
#define DDDWarnning
#define DDDStr(text)
#define DDDValue2(t,v)
#define DDDTime(str)
#endif
#endif // //tDebug_H
