#include "deviceidsettings.h"
#include "ui_deviceidsettings.h"
#include "../../mainwindow.h"
#include "../../mainwidget.h"
#include <QSettings>
#include "common.h"

const QString DeviceIdSettings::SETTING_DEVICE_ID_GROUP = "device";
const QString DeviceIdSettings::SETTING_DEVICE_ID = "device_id";

DeviceIdSettings::DeviceIdSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DeviceIdSettings)
{
    ui->setupUi(this);
    ui->lineEdit->setText(getDeviceId());
    //ui->lineEdit->setText( MainWindow::getMainWindow()->mMainWidget->getDeviceInfoObj()->deviceId);
}

QString DeviceIdSettings::getDeviceId()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    return set.value(SETTING_DEVICE_ID_GROUP+"/"+SETTING_DEVICE_ID,"000").toString();
}

DeviceIdSettings::~DeviceIdSettings()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(SETTING_DEVICE_ID_GROUP);
    set.setValue(SETTING_DEVICE_ID,ui->lineEdit->text());
    set.endGroup();
    delete ui;
}

void DeviceIdSettings::on_btn_ok_clicked()
{
   //MainWindow::getMainWindow()->mMainWidget->getDeviceInfoObj()->deviceId = ui->lineEdit->text();
   //delete ui;
}
