#include "temperaturesettings.h"
#include "ui_temperaturesettings.h"

TemperatureSettings::TemperatureSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TemperatureSettings)
{
    ui->setupUi(this);
}

TemperatureSettings::~TemperatureSettings()
{
    delete ui;
}
