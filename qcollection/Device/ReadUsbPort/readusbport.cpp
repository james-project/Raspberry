#include "readusbport.h"
#include <QDebug>
#include "Setting/BasicSetting/basicsettingwidget.h"

ReadUsbPort::ReadUsbPort(QObject *parent) : QObject(parent)
{
}

ReadUsbPort *ReadUsbPort::m_ReadUsbPort = NULL;
ReadUsbPort *ReadUsbPort::getobj()
{
    if(m_ReadUsbPort == NULL)
        m_ReadUsbPort = new ReadUsbPort();
    return m_ReadUsbPort;
}

void ReadUsbPort::getUsbPortName(UsbPortName &portName)
{
    char buf[256*4];
    memset(buf,0,sizeof(buf));
    FILE *fp = popen("ls -l /sys/class/tty/ttyUSB*","r");
    fread(buf,4,256,fp);
    QString readBuf = QString(buf);
    QStringList readlist;
    readlist.clear();
    readlist = readBuf.split("\n");
    QString portList;
    int mod = BasicSettingWidget::getOprtMode();
    switch (mod) {
    case BasicSettingWidget::MASHWELDER_MODE:
        for (int i =0; i<readlist.length();i++) {
            if ( !QString(readlist.at(i)).isEmpty()) {
                portList = readlist.at(i);
                if (portList.contains("1-1.2:1.0")) {//dianbiao
                    portName.port0_name = portList.right(7);
                }
                else if (portList.contains("1-1.5:1.0")) {//dianzicheng
                    portName.port1_name = portList.right(7);
                }
                else if(portList.contains("1-1.4:1.0")) {//kongzhiqi
                    portName.port2_name = portList.right(7);
                }
                else if(portList.contains(/*"1-1.3.1/1-1.3.1:1.0"*/"1-1.3.4/1-1.3.4:1.0")) {
                    portName.port3_name = portList.right(7);
                }
                else {
                }
            }
        }
        break;
    case BasicSettingWidget::PLASTIC_MODE:
        for (int i =0; i<readlist.length();i++) {
            if ( !QString(readlist.at(i)).isEmpty()) {
                portList = readlist.at(i);
                if (portList.contains("1-1.5:1.0")) {
                    portName.port0_name = portList.right(7);
                }
                else if (portList.contains("1-1.2:1.0")) {//dianbiao
                    portName.port1_name = portList.right(7);
                }
                else if(portList.contains("1-1.4:1.0")) {//kongzhiqi
                    portName.port2_name = portList.right(7);
                }
                else if(portList.contains(/*"1-1.3.1/1-1.3.1:1.0"*/"1-1.3.4/1-1.3.4:1.0")) {
                    portName.port3_name = portList.right(7);
                }
                else {
                }
            }
        }
        break;
    case BasicSettingWidget::PUNCHING_MODE:
        for (int i =0; i<readlist.length();i++) {
            if ( !QString(readlist.at(i)).isEmpty()) {
                portList = readlist.at(i);
                if (portList.contains("1-1.2:1.0")) {
                    portName.port0_name = portList.right(7); //dianbiao
                }
                else if (portList.contains("1-1.5:1.0")) {//dianzicheng
                    portName.port1_name = portList.right(7);
                }

                else if(portList.contains("1-1.3.1")) {//jiguang1
                    portName.port2_name = portList.right(7);
                }
                else if(portList.contains("1-1.3.2")) {//jiguang2
                    portName.port3_name = portList.right(7);
                }
                /*else if(portList.contains("1-1.3.3")) {//jiguang1
                    portName.port2_name = portList.right(7);
                }
                else if(portList.contains("1-1.3.4")) {//jiguang2
                    portName.port3_name = portList.right(7);
                }*/
                else {
                }
            }
        }
        break;
    default:
        for (int i =0; i<readlist.length();i++) {
            if ( !QString(readlist.at(i)).isEmpty()) {
                portList = readlist.at(i);
                if (portList.contains("1-1.4:1.0")) {
                    portName.port0_name = portList.right(7);
                }
                else if (portList.contains("1-1.3:1.0")) {//dianbiao
                    portName.port1_name = portList.right(7);
                }
                else if(portList.contains("1-1.2:1.0")) {//kongzhiqi
                    portName.port2_name = portList.right(7);
                }
                else if(portList.contains(/*"1-1.3.1/1-1.3.1:1.0"*/"1-1.3.4/1-1.3.4:1.0")) {
                    portName.port3_name = portList.right(7);
                }
                else {
                }
            }
        }
        break;
    }

    pclose(fp);
}

