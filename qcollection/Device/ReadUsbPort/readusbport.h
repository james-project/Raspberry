#ifndef READUSBPORT_H
#define READUSBPORT_H
#include <QObject>


typedef struct __UsbPortName
{
    QString port0_name;
    QString port1_name;
    QString port2_name;
    QString port3_name;
}UsbPortName;


class ReadUsbPort : public QObject
{
    Q_OBJECT
public:
    explicit ReadUsbPort(QObject *parent = 0);
    static ReadUsbPort *getobj();
    void getUsbPortName(UsbPortName &portName);
signals:

public slots:
private:
    static ReadUsbPort *m_ReadUsbPort;
};

#endif // READUSBPORT_H
