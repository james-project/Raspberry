#ifndef MASHWELDER_H
#define MASHWELDER_H

#include <QWidget>
#include "Setting/standardparasetting.h"
#include "Setting/weldinfosetting.h"
#include "common.h"
#include "http/HttpServer/httpserver.h"
#include "Modbus/ModbusRTU/modbusrtumaster.h"
#include "Setting/contactusset.h"
#include "Setting/warnrecordquery.h"
#include "Setting/logonrightsedit.h"
//#include "qextserial/qextserialport.h"
#include "Device/HardWare/Gpio/ultrasonicthread.h"
#include "Device/HardWare/TemperatureDetect/temperaturethread.h"
#include "Device/HardWare/AmmeterScale/ammeterscale.h"
#include "http/HttpClient/httpclient.h"
#include "http/HttpServer/httpserver.h"
#include "Device/ReadUsbPort/readusbport.h"
#include "DeviceState/devicestate.h"
#include "Dialog/fullscreenmodaldlg.h"
#include "Dialog/mymessagedialog.h"
#include "mainwidget.h"
#include "scale/scaleamendcalibration.h"
#include "configuration.h"

namespace Ui {
class MashWelder;
}

class MashWelder : public MainWidget
{
    Q_OBJECT

public:
    static QString _inputVal;
    static bool _isBroken;
    static int m_systemStatus;
    static bool Equipment_ckeck;
    bool l_Equipment_ckeck;
    static bool QC_Scan_ckeck;
    bool l_QC_Scan_ckeck;
    int l_systemStatus;
    typedef struct __ScrollWarnSwitch
    {
        bool warnswitch1;
        bool warnswitch2;
        bool warnswitch3;
        bool warnswitch4;
        bool warnswitch5;
        bool warnswitch6;
        bool warnswitch7;
        bool warnswitch8;
        bool warnswitch9;
    }ScrollWarnSwitch;
    explicit MashWelder(MainWindow *parent = 0);
    ~MashWelder();
    void initUi();
    void initHttpClient();
    virtual void initButtonSlot();
    void initUltraSonicThread();
    void initLEDcycleTime();
    void initTemperatureThread();
    void initEventFilter();
    void postWarnData(QString,QString);
    void postWarnData(QString,QString,QString);

    /**
     * @brief deleteFun
     * delete Pointer when destructor
     */
    void deleteFun();
    void deleteSubThread();
    /**
     * @brief timerEvent 定时器
     * @param event
     */
    void timerEvent(QTimerEvent *event);
    /*!
     * \brief parseRawData
     * 解析收到的焊接规范号相关的值
     */
    void parseRawData();
    bool eventFilter(QObject *watched, QEvent *event);
    void inputStandardNum(int max,int min);
    /*!
     * \brief inputDefectiveNum
     *输入不良品个数
     */
    void inputDefectiveNum();
    /*!
     * \brief setCurrentStandardPara_ByOrder
     *物料确认后，按照订单里的焊接规范号设置当前的焊接规范号
     */
    void setCurrentStandardPara_ByOrder(ProductionVariant & );
    inline void setWarningTimes(){
        m_warningTimes++;
    }
    inline int getWarningTimes(){
        return m_warningTimes;
    }
    virtual void sendCloseInputDialogSignal_ByHeadMan();
    virtual void sendCloseConfirmDialogSignal_ByQcMan();
    void initSpotDelayTime();
    void initQCScanCheck();
    void initOrderRecordShow();
    virtual void saveElectrodeCountToDataBase();
    /*!
         * \brief initScaleSerialSetting
         * 初始化获取电子秤重量的串口
         */
    virtual void initScaleSerialSetting();
    void initAmmeterScaleModbusSlave();
    void setWeldEnable_unChecked();//WeldEnable unchecked and unused

signals:
    void standardPara_refresh(StandardPara*);
    void showScaleWeight(QString);
    void showscale_unitWeight(QString);
    void showScaleCount(QString);
    void showDistance(QString);
    void showTemperature(QString);
    void showAmmeterData(QString);
    void showAmmeterVolData(QString);
    void showLaserDistanceAtSys(QString);
    void deviceStart_click();
    void preLoadValveOpen_click();
    void weldEnable_click();
    void materialConfirmOK();
    void openSettingSignal();
    void deleteWarnningDialog_signals();
    void closeInputDialog_ByHeadMan();//TODO 20180730 //组长权限刷卡关掉输入对话框
    void closeSenconConfirmDialog_ByQcMan();//TODO 20180730 //QC权限刷卡关掉输入对话框
    void sendTestWeight(float);//TODO 20180803 Add for test
    void sendUw(float);
    void sig_btnOk_clicked();


public slots:
    void writeStandardPara();
    void receiveHttpRegisterOK(bool);
    void httpRegister_slots(bool);
    void receiveStation_fromHttp(QString);
    void receiveModbusConnected(QString);
    void receiveModbusDisconnected(QString);
    void postSerialNum_Slots();
    void updateDeviceState_Slots();
    virtual void startFirstProduction_slots();
    void currentUserShow(QString);
    void resetWarning();
    void getCurrentTestVol(QString);
    void showOrder();
    virtual void escFromSettings();
    virtual void slot_hanjieguifanhao(ProductionVariant);


private slots:
    void firstProductionOK_toScanOperator2();
    void firstQcCheck2();
    void receiveStandardData_fromModbus(QString);
    void receiveWarnData_fromModbus(int);
    void deviceCircleSend();
    void preLoadValveOpen_slot();//预压阀开 -- 向线圈写值
    void weldEnable_Slot();
    /*!
     * \brief weldStart_PressCmd
     * 启动按钮按下去发送的指令
     */
    void weldStart_PressCmd();
    /*!
     * \brief weldStart_ReleaseCmd
     * 启动按钮释放的时候发送的指令
     */
    void weldStart_ReleaseCmd();

    /*!
     * \brief weldStart_PressCmd
     * 焊接计数器清零按钮按下去发送的指令
     */
    void weldCountClear_PressCmd();
    /*!
     * \brief weldStart_ReleaseCmd
     * 焊接计数器按钮释放的时候发送的指令
     */
    void weldCountClear_ReleaseCmd();
    /*!
     * \brief weldWarnClearCmd
     * 发送报警清除命令
     */
    void weldWarnClearCmd();
    /**
     * @brief showStandardParaSetting
     *  显示规范号设置界面
     */
    void showStandardParaSetting();

    /*!
      * \brief showContactUsSetting
      * 显示联系我们设置
      */
    void showContactUsSetting();
    /*!
     * \brief showQueryWarnTable
     * 显示报警信息记录
     */
    void showQueryWarnTable();

    /*!
     * \brief showLogonRightsEditSetting
     * 显示权限编辑界面
     */
    void showLogonRightsEditSetting();
    void receiveExitStand_Slot();
    void receiveExitContact_Slot();
    void exitToMainUi();
    void on_btn_sysInfo_clicked();
    void deviceCircleTimeSlot();
    /*!
     * \brief termitTimer
     * 编辑参数时 暂时中断定时器刷新
     */
    void termitTimer();
    /*!
     * \brief receiveStandPapa_ofStandSet
     * Write Standard parameter
     */
    void receiveStandPapa_ofStandSet(int);
    void receiveSQ_ofStandSet(int);
    void receiveSO_ofStandSet(int);
    void receiveS1_ofStandSet(int);
    void receiveW1_ofStandSet(int);
    void receiveCO_ofStandSet(int);
    void receiveW2_ofStandSet(int);
    void receiveHO_ofStandSet(int);
    void receiveOF_ofStandSet(int);
    void receivePrecurr_ofStandSet(int);
    void receiveWeldcurr_ofStandSet(int);
    void receiveWeldPreCounts_ofStandSet(int);
    /*!
     * \brief readScaleCom
     * 读称重串口
     */
    virtual void
    readScaleCom();

    /*!
     * \brief readUltraDistance
     * read ultrasonic distance
     */
    void readUltraDistance(float);
    void readTemperature(float);
    void acceptInputValue(int);
    void acceptInputValue_test(float);//TODO 20180803 add for test
    void acceptDefectiveValue(int);
    void readAmmeterPowerData(float);
    void readAmmeterVolData(float);
    /*!
     * \brief updateCurrentCount
     *向Webservice update当前生产数量；
     */
    void updateCurrentCount();

    /*!
     * \brief updateCurrentCountCirle
     * 定时发送当前生产数量
     */
    void updateCurrentCountCirle();
    /*!
     * \brief initModbusSetting
     * init modbus setting
     */
    void initModbusSetting();
    //void initAmmeterScaleModbusSlave();
    void powerOff();
    void qcCheckNG_AutoShowTips();
    void on_settingBtn_clicked();
    virtual void spotCheckManualExecute();
    virtual void QCCheckManualExecute();
    virtual void spotCheckTimerTips();
    virtual void QCCheckTimerTips();
    virtual void recordScaleData_slots();
    void getCalibrationCount(int,float);

    void LEDFrashCircleTimeSlot();
    void on_pushButton_clicked();

    void on_btn_elecEnable_clicked();

private:
    /**
     * @brief parseStandardPara1
     * @param array
     * 解析规范号数据
     */
    void parseStandardPara(QByteArray array);
    void initFont();
    void posDetailFun( QString sequence);
    void putDetailFun(QString sequence);
    /*!
     * \brief getWarnDisplay_switch
     * 警报列表显示实际文本
     */
    void getWarnDisplay_switch(int warnValue);
    void login();

    /*!
     * \brief insertDataToWarn  向报警记录数据库中插值
     * \param warn_code         报警代码
     * \param warn_content      报警内容
     * \param add_time          记录时间
     */
    void insertDataToWarn(QString warn_code,QString warn_content,QDateTime add_time);
    virtual void enableButton(bool bl);

    QList<QString> m_material;
    void initComboBox();
    void judgingVolAlarms();
    virtual void deleleMashWarningDialog();
    virtual void setOrderButtonShow();
    virtual void setOrderButtonDisabled();
    virtual void removeDefectNumEvent();
    virtual void setOrderButtonHidden();
    virtual void deleteSpotScrollText();
    virtual void setDefectValue();
    virtual void startSpotCheckTimer();
    QString getLocalHostIP();
    QString macAddress();
    virtual void clearWeldingCount();
    virtual void setCurrentOrderStatusShow(ProductionVariant &productInfo);

    Ui::MashWelder *ui;
    ModbusRtuMaster *m_modbusWnd;
    int timeId;
    int onlinetimeId;
    /*----zhanglong*/
    int Alarm_Count;/*Over temperture count*/
    StandardParaSetting *mStandardParaSetting;
    WeldInfoSetting *mWeldInfoSetting;
    ContactUsSet *m_ContactUsSet;
    StandardPara *mStandardPara;
    StandardPara m_standardpara_temp;
    WarnRecordQuery *m_WarnReord;
    LogonRightsEdit *m_LogonRightsEdit;
    QTimer *m_heartBeatTimer;
    QTimer *m_freshLEDTimer;
    QTimer *m_updateCurrentCountTimer;
    QStringList m_warnList;
    QStringList AlarmList;
    /*!
     * \brief m_warnCodeList
     * 报警错误代码列表
     */
    QStringList m_warnCodeList;
    /*!
     * \brief m_warnContentList
     * 报警错误内容列表
     */
    QStringList m_warnContentList;

    int m_warnValueinfo;

    /*!
     * \brief m_scaleCom
     * 称重串口
     */
    UltraSonicThread *m_ultraSonicThread;
    TemperatureThread *m_temperatureThread;
    AmmeterScale     *m_ammeterDate;
    bool m_register_flag;
    bool m_pos_currentCount;
    StandardParaText *m_StandardParaText;
    QString m_receiveData;

    /*!
     * \brief m_qc_checkTimer
     * 物料确认后，开启qccheck delay时间，在一小时内没有qccheck将关闭焊接使能
     */
    /*!
     * \brief m_defectiveNumber
     *  不良品个数
     */
    int m_defectiveNumber;
    bool m_postDeviceDetail_flag;
    /*!
     * \brief m_warningTimes :报警次数
     */
    int m_warningTimes;
    /**
     * @brief m_DefVol
     * 电压差
     */
    float m_DefVol;

    /*!
     * \brief m_weldingTimes_temp :临时存放的焊接次数
     */
    int m_weldingTimes_temp;
    QString m_currentCount_temp;
    bool m_orderFinish_flag;
    QString m_current_taskId;
    int m_current_orderWorkNum;
    float m_current_order_finished_weight;
    int m_current_order_finished_count;
    float m_scale_weight_temp;
    int m_scale_count_calibrated;
    bool m_save_weight_flag;
    ScaleAmendCalibration *m_scaleAmendInstance;
    int m_electrode_totalCout;
    int m_electrode_remainCount;
    int m_electrode_preCount;
    ScrollWarnSwitch m_ScrollWarnSwitch;
    int m_task_timeId;
    int readScaleComDelay_count;
    MyMessageDialog* _msgDlg;

public:
    void inputGoodNum();
    void sendDatasAboutOrder();
    static bool _orderBegin;
    QJsonObject  _objToserver;
    QJsonArray _jsArrToserver;
    QTimer* _timerSendToserver;
public slots:
    void slot_CloseWeldEnable();
    void on_le_weldcount_textChanged(QString);
    void accpetGoodValue(int val);

private:
    QTimer* _weldEnableTimer;
    void sendUpdateTodayTask();
    void sendNotGood();
    void sendOperatorAndMaterial();
    void send7Datas();

    QJsonArray _writeNum_arr;
    QString startTime;
    QString endTime;
    QJsonObject get7datas_fromUI();
    bool _isWorking;
    int _iNum;

protected slots:
    virtual void slot_send7data();
    virtual void slot_clearOrderMsg();

    virtual void slot_sendOperator(QString ,QString,  DeviceState::DeviceStion );
    virtual void slot_sendMaterial(QString ,QString, QString );
    virtual void slot_sendGongZhang(QString, QString );
};

#endif // MASHWELDER_H
