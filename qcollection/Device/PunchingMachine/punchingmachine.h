#ifndef PUNCHINGMACHINE_H
#define PUNCHINGMACHINE_H

#include <QWidget>
#include "Device/mainwidget.h"
#include "http/HttpClient/httpclient.h"
#include "http/HttpServer/httpserver.h"
#include "ProductionOrder/productionordermanagement.h"
#include "HardWare/AccelarateScale/accelaratescale.h"
#include "Device/HardWare/AmmeterScale/ammeterscale.h"
#include "Modbus/ModbusRTU/modbusrtumaster.h"
#include "Device/HardWare/TemperatureDetect/temperaturethread.h"
#include "scale/scaleamendcalibration.h"
#include "../HttpInteractiveData/httpinteractivedata.h"

namespace Ui {
class PunchingMachine;
}

class PunchingMachine : public MainWidget
{
    Q_OBJECT

public:    
    /*----zhanglong*/
    static int m_systemStatus;
    int l_systemStatus;
    float DistenceShort;
    float DistenceLong_max;
    float DistenceLong_min;
    int DistenceCount;
    int DistenceWorkCount1;
    int DistenceWorkCount2;
    float distence;
    static bool Equipment_ckeck;
    static bool l_Equipment_ckeck;
    static bool _orderBegin;
    explicit PunchingMachine(MainWindow *parent = 0);
    ~PunchingMachine();
    void initHttpClient();
    virtual void setOrderButtonShow();
    virtual void setOrderButtonDisabled();
    virtual void setOrderButtonHidden();
    virtual void removeDefectNumEvent();
    virtual void deleteSpotScrollText();
    virtual void startSpotCheckTimer();
    virtual void setDefectValue();
    virtual void setCurrentOrderStatusShow(ProductionVariant &productInfo);
    virtual void sendCloseInputDialogSignal_ByHeadMan();
    virtual void sendCloseConfirmDialogSignal_ByQcMan();
    virtual void enableButton(bool);
    void initSpotDelayTime();
    /*----zhanglong*/
    void initLEDcycleTime();
    void initQCScanCheck();
    /*void initTemperatureThread();*/
    void deleteFunc();
    void timerEvent(QTimerEvent *event);
    virtual void initButtonSlot();
    void initOrderRecordShow();
    void postWarnData(QString,QString);
    /*!
         * \brief initScaleSerialSetting
         * 初始化获取电子秤重量的串口
         */
    virtual void initScaleSerialSetting();
    /*
    // 初始化激光测距仪
    */
    virtual void initLaser_distance_measuring();
    virtual void initLaser_distance_measuringSec();
    virtual void saveElectrodeCountToDataBase();
    static bool QC_Scan_ckeck;
    static bool l_QC_Scan_ckeck;
    bool eventFilter(QObject *watched, QEvent *event);
    void inputDefectiveNum();
    void inputGoodNum();
    int _badNum;

public slots:
    //void receiveHttpClientData(QString, QString);
    void receiveHttpRegisterOK(bool);
    void httpRegister_slots(bool);
    virtual void startFirstProduction_slots();
    virtual void escFromSettings();
    virtual void showOrder();
    void acceptDefectiveValue(int );
    void accpetGoodValue(int );


    /*!
     * \brief readScaleCom
     * 读称重串口
     */
  //  virtual void readScaleCom();
    // 读取激光测距仪数据
    virtual void readLDMCom();
    // 读取激光测距仪数据2
    virtual void readLDMComSec();
    void onTimerAutoSendTimeout(); //定时发送
    void onSendCmd();
    void receiveStation_fromHttp(QString station);
    void on_btn_contact_clicked();
public :
    void sendNotGood();

    void  sendOperatorAndMaterial();
    QJsonObject _objToserver;
    QJsonArray _jsArrToserver;
    QTimer* _timerSendToserver;
private slots:
    void on_settingBtn_clicked();
    virtual void spotCheckManualExecute();
    virtual void spotCheckTimerTips();
    void getLaserDistance(QString);
    virtual void recordScaleData_slots();
    /*----zhanglong*/
    void LEDFrashCircleTimeSlot();
    void Distance_mec_restart();
    void deviceCircleTimeSlot();
    void updateCurrentCountCirle();
    void initAmmeterScaleModbusSlave(); //电能表
    void deviceCircleSend();
    void updateCurrentCount();
    void readAmmeterPowerData(float);
    void readAmmeterVolData(float);
    virtual void QCCheckManualExecute();

    virtual void QCCheckTimerTips();
    void readAmmeterPowerData_1(float);
    void readAmmeterVolData_1(float);
    void readAmmeterPowerData_2(float);
    void readAmmeterVolData_2(float);
    void readAmmeterPowerData_3(float);
    void readAmmeterVolData_3(float);
    void readTemperature(int);
    void readTemperature_1(int);
    void readTemperature_2(int);
    void initModBusSetting();
    void onChkAutoSendToggled(bool checked);
    void getCalibrationCount(int,float);
    /*!
     * \brief readScaleCom
     * 读称重串口
     */
    virtual void readScaleCom();
    void on_btn_SpotCheck_clicked();
    void on_btn_QCCheck_clicked();
    void firstProductionOK_toScanOperator2();
    void firstQcCheck2();
    void updateDeviceState_Slots();
    void on_le_calibratedCount_textChanged(QString str);  //冲压生产


    void on_btn_manual_clicked();

    void on_btn_test_clicked();

signals:
    void showScaleWeight(QString);
    void showscale_unitWeight(QString);
    void openSettingSignal();
    void showTemperature(QString);
    void toggleAutoSend(bool);
    void sendUw(float);
    void closeInputDialog_ByHeadMan();

private:
    void initAccelarateScale();
    StandardPara *mStandardPara;
    StandardPara m_standardpara_temp;
    Ui::PunchingMachine *ui;
    ModbusRtuMaster *m_modbusWnd;
    //TemperatureThread *m_temperatureThread;
    bool m_register_flag;
    ProductionVariant tProctionInfo;
    AccelarateScale *m_accelarateScale;
    bool m_pos_currentCount;
    QTimer *m_heartBeatTimer;
    QTimer *m_updateCurrentCountTimer;
    bool m_postDeviceDetail_flag;
    void posDetailFun(QString sequence);
    void putDetailFun(QString sequence);
    int timeId;
    int tmp;  //激光测距临时变量
    /*----zhanglong*/
    int Alarm_Count;
    int TeAlarm_Count;
    int TeAlarm_Count1;
    int TeAlarm_Count2;
    QTimer *m_freshLEDTimer;
    QTimer *Check_DistenceTimer;
    QString m_currentCount_temp;
    AmmeterScale     *m_ammeterDate;
    QTimer*                 m_timerAutoSend;
    ScaleAmendCalibration *m_scaleAmendInstance;
    int readScaleComDelay_count;
    QStringList AlarmList;

    QString m_current_taskId;
    float m_current_order_finished_weight;
    int m_current_order_finished_count;
    int m_task_timeId;
    int onlinetimeId;
    bool m_orderFinish_flag;
    int m_current_orderWorkNum;
    int _iNum;
    QJsonArray  _workArr;
    QString startTime;
    QString endTime;

    QJsonObject getUIDatas();
    void initFont();
    void initUi();
    void sendUpdateTodayTask();
    void send7Datas();
    void send7Datas_first();

    void showDefectiveNumTips();
protected slots:
    virtual void slot_sendOperator(QString ,QString,  DeviceState::DeviceStion );
    virtual void slot_sendMaterial(QString ,QString, QString );
    virtual void slot_sendGongZhang(QString, QString );
    virtual void slot_clearOrderMsg();

};

#endif // PUNCHINGMACHINE_H
