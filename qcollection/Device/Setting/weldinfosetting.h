#ifndef WELDINFOSETTING_H
#define WELDINFOSETTING_H

#include <QWidget>
#include "common.h"

namespace Ui {
class WeldInfoSetting;
}

class WeldInfoSetting : public QWidget
{
    Q_OBJECT

public:
    explicit WeldInfoSetting(QWidget *parent,QLayout *layout,int width,int height);
    ~WeldInfoSetting();

signals:
    void exitcurr();
    void showScaleWeight(QString);
public slots:
    void receiveScaleWeight(QString);
    void receiveScaleCount(QString);

    void receiveDistance(QString);
    void receiveTemperature(QString);
    void receiveAmmeterData(QString);
    void receiveAmmeterVolData(QString);
    void receiveLaserDistanceFromMain(QString);
private slots:
    void on_btn_back_clicked();

private:
    void initFont();
private:
    Ui::WeldInfoSetting *ui;
};

#endif // WELDINFOSETTING_H
