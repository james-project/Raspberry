#include "standardparasetting.h"
#include "ui_standardparasetting.h"
#include "input/inputdialog.h"
#include <QMessageBox>
#include "configuration.h"



bool StandardParaSetting::_bHeadManPass = false;
bool StandardParaSetting::_bQcPass = false;

StandardParaSetting::StandardParaSetting(QWidget *parent,QLayout *layout,int width,int height,StandardPara *mStandardPara) :
    QWidget(parent),
    ui(new Ui::StandardParaSetting)
{
    Q_UNUSED(layout);
    ui->setupUi(this);
    initFont();
    this->setFixedSize(width,height);
    ui->le_weldStandardNum->setText(mStandardPara->weldStandardNum);
    ui->le_sq->setText(mStandardPara->sq);
    ui->le_so->setText(mStandardPara->so);
    ui->le_s1->setText(mStandardPara->s1);
    ui->le_w1->setText(mStandardPara->w1);
    ui->le_co->setText(mStandardPara->co);
    ui->le_w2->setText(mStandardPara->w2);
    ui->le_ho->setText(mStandardPara->ho);
    ui->le_of->setText(mStandardPara->of);
    ui->le_prevolcurr->setText(mStandardPara->preCurrVol);
    ui->le_weldvolcurr->setText(mStandardPara->weldCurrVol);
    ui->le_preCounts->setText(mStandardPara->weldPreCount);
    connect(ui->le_weldStandardNum,     SIGNAL(editingFinished()),      this,   SLOT(writeStandardParaSet_slots()));    //规范设置界面修改规范号
    connect(ui->le_weldStandardNum,     SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_weldStandardNum,     SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_sq,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardSQSet_slots()));      //规范设置界面修改预压时间
    connect(ui->le_sq,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_sq,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_so,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardSOSet_slots()));      //规范设置界面修改加压时间
    connect(ui->le_so,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_so,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_s1,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardS1Set_slots()));      //规范设置界面修改缓升时间
    connect(ui->le_s1,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_s1,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_w1,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardW1Set_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_w1,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_w1,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_co,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardCOSet_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_co,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_co,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_w2,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardW2Set_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_w2,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_w2,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_ho,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardHOSet_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_ho,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_ho,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_of,                  SIGNAL(editingFinished()),      this,   SLOT(writeStandardOFSet_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_of,                  SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_of,                  SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_prevolcurr,          SIGNAL(editingFinished()),      this,   SLOT(writeStandardPrecurrSet_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_prevolcurr,          SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_prevolcurr,          SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_weldvolcurr,         SIGNAL(editingFinished()),      this,   SLOT(writeStandardWeldcurrSet_slots()));      //规范设置界面修改焊接1时间
    connect(ui->le_weldvolcurr,         SIGNAL(selectionChanged()),     this,   SIGNAL(termitTime_stanSet()));
    connect(ui->le_weldvolcurr,         SIGNAL(textEdited(QString)),    this,   SIGNAL(termitTime_stanSet()));

    ui->le_weldStandardNum->installEventFilter(this);
    ui->le_sq->installEventFilter(this);
    ui->le_so->installEventFilter(this);
    ui->le_s1->installEventFilter(this);
    ui->le_w1->installEventFilter(this);
    ui->le_co->installEventFilter(this);
    ui->le_w2->installEventFilter(this);
    ui->le_ho->installEventFilter(this);
    ui->le_of->installEventFilter(this);
    ui->le_prevolcurr->installEventFilter(this);
    ui->le_weldvolcurr->installEventFilter(this);
    ui->le_preCounts->installEventFilter(this);
}

StandardParaSetting::~StandardParaSetting()
{
    delete ui;
}

bool StandardParaSetting::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        if (watched == ui->le_weldStandardNum)
        {
            inputStandardParaValue(255,1);
        }
        if (watched == ui->le_sq)
        {
            inputSqValue(255,0);
        }
        if (watched == ui->le_so)
        {
            inputSoValue(255,0);
        }
        if (watched == ui->le_s1)
        {
            inputS1Value(255,0);
        }
        if (watched == ui->le_w1)
        {
            inputW1Value(255,0);
        }
        if (watched == ui->le_co)
        {
            inputCOValue(255,0);
        }
        if (watched == ui->le_w2)
        {
            inputW2Value(255,0);
        }
        if (watched == ui->le_ho)
        {
            inputHOValue(255,0);
        }
        if (watched == ui->le_of)
        {
            inputOFValue(255,0);
        }
        if (watched == ui->le_prevolcurr)
        {
            inputPreVolValue(400,0);
        }
        if (watched == ui->le_weldvolcurr)
        {
            inputWeldVolValue(400,0);
        }
        if (watched == ui->le_preCounts)
        {
            inputWeldingPreCounts(60000,0);
        }
    }
    return QWidget::eventFilter(watched,event);
}

void StandardParaSetting::inputStandardParaValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardParaValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputSqValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardSQValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputSoValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardSOValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputS1Value(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardS1Value(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputW1Value(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardW1Value(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputCOValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardCOValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputW2Value(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardW2Value(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputHOValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardHOValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputOFValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardOFValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}



void StandardParaSetting::inputPreVolValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardPrecurrValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputWeldVolValue(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardWeldcurrValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void StandardParaSetting::inputWeldingPreCounts(int max, int min)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,max,min,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardWeldPreCounts(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}


void StandardParaSetting::on_btn_back_clicked()
{
    this->close();
    emit exitcurr();
    _bHeadManPass = false;
    _bQcPass = false;
}

void StandardParaSetting::writeStandardParaSet_slots()
{
    emit StandardParaValue(ui->le_weldStandardNum->text().toInt());
}

void StandardParaSetting::writeStandardSQSet_slots()
{
    emit StandardSQValue(ui->le_sq->text().toInt());
}

void StandardParaSetting::writeStandardSOSet_slots()
{
    emit StandardSOValue(ui->le_so->text().toInt());
}

void StandardParaSetting::writeStandardS1Set_slots()
{
    emit StandardS1Value(ui->le_s1->text().toInt());
}

void StandardParaSetting::writeStandardW1Set_slots()
{
    emit StandardW1Value(ui->le_w1->text().toInt());
}

void StandardParaSetting::writeStandardCOSet_slots()
{
    emit StandardCOValue(ui->le_co->text().toInt());
}

void StandardParaSetting::writeStandardW2Set_slots()
{
    emit StandardW2Value(ui->le_w2->text().toInt());
}

void StandardParaSetting::writeStandardHOSet_slots()
{
    emit StandardHOValue(ui->le_ho->text().toInt());
}

void StandardParaSetting::writeStandardOFSet_slots()
{
    emit StandardOFValue(ui->le_of->text().toInt());
}

void StandardParaSetting::writeStandardPrecurrSet_slots()
{
    emit StandardPrecurrValue(ui->le_prevolcurr->text().toInt());
}

void StandardParaSetting::writeStandardWeldcurrSet_slots()
{
    emit StandardWeldcurrValue(ui->le_weldvolcurr->text().toInt());
}

void StandardParaSetting::receiveStandard_refresh(StandardPara *m_standardPara)
{
    ui->le_weldStandardNum->setText(m_standardPara->weldStandardNum);
    ui->le_sq->setText(m_standardPara->sq);
    ui->le_so->setText(m_standardPara->so);
    ui->le_s1->setText(m_standardPara->s1);
    ui->le_w1->setText(m_standardPara->w1);
    ui->le_co->setText(m_standardPara->co);
    ui->le_w2->setText(m_standardPara->w2);
    ui->le_ho->setText(m_standardPara->ho);
    ui->le_of->setText(m_standardPara->of);
    ui->le_prevolcurr->setText(m_standardPara->preCurrVol);
    ui->le_weldvolcurr->setText(m_standardPara->weldCurrVol);
    ui->le_preCounts->setText(m_standardPara->weldPreCount);
}

void StandardParaSetting::initFont()
{
    {
        QFont f("DejaVu Sans",20);
        ui->lab_title->setFont(f);
    }
    {
        QFont f("DejaVu Sans",18);
        ui->label_2->setFont(f);
        ui->label_5->setFont(f);
        ui->label_9->setFont(f);
        ui->label_10->setFont(f);
        ui->label_11->setFont(f);
        ui->label_12->setFont(f);
        ui->label_13->setFont(f);
        ui->label_14->setFont(f);
        ui->label_15->setFont(f);
        ui->label_16->setFont(f);
        ui->label_17->setFont(f);
        ui->label_22->setFont(f);
    }
    {
        QFont f("DejaVu Sans",18);
        ui->le_co->setFont(f);
        ui->le_ho->setFont(f);
        ui->le_of->setFont(f);
        ui->le_prevolcurr->setFont(f);
        ui->le_s1->setFont(f);
        ui->le_so->setFont(f);
        ui->le_sq->setFont(f);
        ui->le_weldStandardNum->setFont(f);
        ui->le_w1->setFont(f);
        ui->le_w2->setFont(f);
        ui->le_weldvolcurr->setFont(f);
        ui->le_preCounts->setFont(f);
    }
}
