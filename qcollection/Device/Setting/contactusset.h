#ifndef CONTACTUSSET_H
#define CONTACTUSSET_H

#include <QWidget>

namespace Ui {
class ContactUsSet;
}

class ContactUsSet : public QWidget
{
    Q_OBJECT

public:
    explicit ContactUsSet(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~ContactUsSet();
    void initFont();
signals:
    void exitCurr();
public slots:
    void sendExit();

private:
    Ui::ContactUsSet *ui;
};

#endif // CONTACTUSSET_H
