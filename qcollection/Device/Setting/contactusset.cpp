#include "contactusset.h"
#include "ui_contactusset.h"
#define VEISION "V1.0.17"

/*!
    V1.0.1 2018-02-02
    和webservice 服务器联调控制 开关动作
 */

/*!
    V1.0.2 2018-02-05
    增加GPIO SPI测量温度
 */
/*!
    V1.0.3 2018-02-05
    将参数输入改为数字输入框输入
 */

/*!
    V1.0.4 2018-02-06
    优化报警录查询ScrollBar，增加登录界面键盘
 */

/*!
    V1.0.5 2018-02-06
增加电能表耗电量的modbus解析和http发送给web
 */

/*! V1.0.07 2018-04-20
1。截至2018-04-20 优化了HTTP 与 Webservice的发送数据交互；
2。增加了压塑机的部分数据采集和界面；
3。增加电能表功耗的数据解析和展示（之前解析的为电压信号），计划将电压作为设备上电的状态；
4。增加扫码功能－－待完善；
 */

/*! V1.0.08 2018-04-20
1.优化点焊机HTTP POST和Put代码，增加发送模块；
 */

/*!  V1.0.09 2018-04-21
    1.增加设备点检刷卡确认工作模块；
    2.优化modebus代码（删除无用代码）；
 */

/*!  V1.0.10 2018-04-21
    1.优化MOdbus代码：将MashWelder由继承自ModbusWnd的关系改为new ModbusWnd对象;
    2.优化警报信息显示；
 */

/*!  V1.0.11 2018-04-27
    1.增加点焊机单机流程：设备点检、接收派工工单、权限确认、工单确认、物料确认和QC首检等等
 */

/*!  V1.0.12 2018-05-03
    1.增加点焊机单机流程：设备点检、接收派工工单、权限确认、工单确认、物料确认和QC首检等等
 */

/*!  V1.0.13 2018-05-08
 * 1.Add the QMutex  in thread;
 * 2.Add the order finish dialog;
 */


/*!  V1.0.14 2018-07-19
1.前后写了两版点焊流程
2.压塑增加计数清零；
3.压塑增加输入状态之 抽芯缸1和抽芯缸2；
 */

/*!  V1.0.15 2018-07-19
1.将一些延时设定时间加上10被的倍律关系；
 */

/*!  V1.0.16 2018-08-01
1.增加点焊机新流程;
2.点焊机电表地址是02；压塑机电表地址是01，现在已经区分;
 */

/*!  V1.0.17 2018-08-03
1。点焊机扫码枪扫二维码之扫物料的起始字串为 TaskID；
2。物料比对查询数据库由按数据库自增的最小ID查询改为按照当前选中的Task ID查询；
 */



ContactUsSet::ContactUsSet(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ContactUsSet)
{
    ui->setupUi(this);
    initFont();
    this->setFixedSize(width,height);
    ui->label_2->setAlignment(Qt::AlignHCenter);
    ui->label_3->setAlignment(Qt::AlignHCenter);
    ui->lab_copyright->setText("Copyright ©2018 BEIJING PEOPLE‘S ELECTTRIC PLANT CO.,LTD. All rights reserved.");
    QString team = "Powered by JING REN team,";
    ui->lab_version->setText(team+VEISION);
    connect(ui->btn_return,SIGNAL(clicked(bool)),this,SLOT(sendExit()));
}

ContactUsSet::~ContactUsSet()
{
    delete ui;
}

void ContactUsSet::initFont()
{
    QFont f("DejaVu Sans",20);
    ui->label_1->setFont(f);
    ui->label_2->setFont(f);
    ui->label_3->setFont(f);
    ui->btn_return->setFont(f);
}

void ContactUsSet::sendExit()
{
    emit exitCurr();
}
