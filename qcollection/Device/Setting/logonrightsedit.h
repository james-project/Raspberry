#ifndef LOGONRIGHTSEDIT_H
#define LOGONRIGHTSEDIT_H

#include <QWidget>
/*!
 *登陆权限编辑
 */

namespace Ui {
class LogonRightsEdit;
}

class LogonRightsEdit : public QWidget
{
    Q_OBJECT

public:
    explicit LogonRightsEdit(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~LogonRightsEdit();

    void initFont();
signals:
    void exitCurr();
private slots:
    void on_btn_return_clicked();

private:
    Ui::LogonRightsEdit *ui;
};

#endif // LOGONRIGHTSEDIT_H
