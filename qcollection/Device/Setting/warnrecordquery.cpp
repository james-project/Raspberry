#include "warnrecordquery.h"
#include "ui_warnrecordquery.h"
#include "DataBase/databasequery.h"
#include "DataBase/dbtab_warn.h"
#include <QSqlQueryModel>
#include "tdebug.h"
#include <QStandardItemModel>
#include <QScrollBar>

WarnRecordQuery::WarnRecordQuery(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WarnRecordQuery)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);

    initFont();
    ui->label->setAlignment(Qt::AlignHCenter);


    /*
     * 方法1 此方法未找到数据居中的设置方法
     */
    /*
    QSqlQueryModel *model = new QSqlQueryModel(ui->tableView);
    DataBase::getDataBase().open();
    model->setQuery(QString("select * from tab_warn;"),DataBase::getDataBase());

    model->setHeaderData(0,Qt::Horizontal,QObject::tr("序号"));
    model->setHeaderData(1,Qt::Horizontal,QObject::tr("报警代码"));
    model->setHeaderData(2,Qt::Horizontal,QObject::tr("报警内容"));
    model->setHeaderData(3,Qt::Horizontal,QObject::tr("时间"));
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    ui->tableView->setColumnWidth(0,(width-10)/8*1);
    ui->tableView->setColumnWidth(1,(width-10)/8*1);
    ui->tableView->setColumnWidth(2,(width-10)/8*3);
    ui->tableView->setColumnWidth(3,(width-10)/8*3);
    DataBase::getDataBase().close();
    */

    /*
     * 方法2 用QStandardItemModel来做，可以实现数据的居中
     */
    //准备数据模型
    QStandardItemModel *model = new QStandardItemModel();
    model->setHorizontalHeaderItem(0, new QStandardItem(QObject::tr("序号")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("报警代码")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("报警内容")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("时间")));
    //利用setModel()方法将数据模型与QTableView绑定
    ui->tableView->setModel(model);
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableView->setColumnWidth(0,(width-50)/8*1);
    ui->tableView->setColumnWidth(1,(width-50)/8*1);
    ui->tableView->setColumnWidth(2,(width-50)/8*3);
    ui->tableView->setColumnWidth(3,(width-50)/8*3);
    DataBaseQuery query(DataBase::getDataBase());
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();
    QStringList keyList;
    keyList <<DBTab_Warn::_ID
           <<DBTab_Warn::_TAB_WARN_CODE
          <<DBTab_Warn::_TAB_WARN_CONTENT
         <<DBTab_Warn::_ADD_TIME;
    QString option = "";
    query.selectTable(DBTab_Warn::TAB_WARN,&keyList,option);
    QStringList list;
    list.clear();
    QString result = "";
    list = result.split(",");
    list.clear();
    int num = 0;
    if(query.first())
    {
        do
        {
            QSqlRecord rcd = query.record();
            for(int i =0;i<rcd.count();i++)
            {
                if(i==3)
                {
                    result = rcd.value(i).toDateTime().toString("yyyy-MM-dd hh:mm:ss");
                }
                else
                {
                    result = rcd.value(i).toString();
                }
                list.append(result);
            }
            for(int i=0;i<list.count();i++)
            {
                QString temp = list.at(i);
                model->setItem(num, i, new QStandardItem(temp));
                //设置单元格文本居中，数据设置为居中显示
                model->item(num, i)->setTextAlignment(Qt::AlignCenter);
            }
            num++;
            list.clear();
        }while(query.next());
    }
    num = 0;
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

    ui->tableView->verticalScrollBar()->setStyleSheet("QScrollBar::vertical {\n"
                                                      "border:0px solid red;\n"
                                                      "width: 50px;\n"
                                                      "background:white;}\n"
                                                      "QScrollBar::handle:vertical {\n"
                                                      "border: 0px solid gray;\n"
                                                      "border-radius:0px;\n"
                                                      "min-height:0px;\n"
                                                      "margin:0px 0 0px 0;}\n"
                                                      "QScrollBar::add-line:vertical{"//向下箭头样式
                                                      "background:url(:/res/pagedown_p.png) center no-repeat;}"
                                                      "QScrollBar::sub-line:vertical{"//向上箭头样式
                                                      "background:url(:/res/pageup.png) center no-repeat;}");

                                                      //    ui->tableView->verticalScrollBar()->setStyleSheet("QScrollBar:vertical{"        //垂直滑块整体
                                                      //                                                              "background:#FFFFFF;"  //背景色
                                                      //                                                              "padding-top:20px;"    //上预留位置（放置向上箭头）
                                                      //                                                              "padding-bottom:20px;" //下预留位置（放置向下箭头）
                                                      //                                                              "padding-left:3px;"    //左预留位置（美观）
                                                      //                                                              "padding-right:3px;"   //右预留位置（美观）
                                                      //                                                              "border-left:1px solid #d7d7d7;}"//左分割线
                                                      //                                                              "QScrollBar::handle:vertical{"//滑块样式
                                                      //                                                              "background:#dbdbdb;"  //滑块颜色
                                                      //                                                              "border-radius:6px;"   //边角圆润
                                                      //                                                              "min-height:80px;}"    //滑块最小高度
                                                      //                                                              "QScrollBar::handle:vertical:hover{"//鼠标触及滑块样式
                                                      //                                                              "background:#d0d0d0;}" //滑块颜色
                                                      //                                                              "QScrollBar::add-line:vertical{"//向下箭头样式
                                                      //                                                              "background:url(:/images/resource/images/checkout/down.png) center no-repeat;}"
                                                      //                                                              "QScrollBar::sub-line:vertical{"//向上箭头样式
                                                      //                                                              "background:url(:/images/resource/images/checkout/up.png) center no-repeat;}");

}

WarnRecordQuery::~WarnRecordQuery()
{
    delete ui;
}

void WarnRecordQuery::on_btn_return_clicked()
{
    emit exitCurr();
}

void WarnRecordQuery::initFont()
{
    QFont f("DejaVu Sans",20);
    ui->label->setFont(f);
    ui->tableView->setFont(f);
}
