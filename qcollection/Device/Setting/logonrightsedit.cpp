#include "logonrightsedit.h"
#include "ui_logonrightsedit.h"

LogonRightsEdit::LogonRightsEdit(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogonRightsEdit)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    initFont();
}

LogonRightsEdit::~LogonRightsEdit()
{
    delete ui;
}

void LogonRightsEdit::initFont()
{
    QFont f("DejaVu Sans",20);
    ui->btn_return->setFont(f);
}

void LogonRightsEdit::on_btn_return_clicked()
{
    emit exitCurr();
}
