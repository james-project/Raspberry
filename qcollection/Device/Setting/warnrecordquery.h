#ifndef WARNRECORDQUERY_H
#define WARNRECORDQUERY_H

#include <QWidget>

namespace Ui {
class WarnRecordQuery;
}

class WarnRecordQuery : public QWidget
{
    Q_OBJECT

public:
    explicit WarnRecordQuery(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~WarnRecordQuery();
signals:
    void exitCurr();

private slots:
    void on_btn_return_clicked();

private:
    void initFont();
private:
    Ui::WarnRecordQuery *ui;
};

#endif // WARNRECORDQUERY_H
