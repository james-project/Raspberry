#ifndef OPERATOREDITORSET_H
#define OPERATOREDITORSET_H

#include <QWidget>

namespace Ui {
class OperatorEditorSet;
}

class OperatorEditorSet : public QWidget
{
    Q_OBJECT

public:
    explicit OperatorEditorSet(QWidget *parent = 0);
    ~OperatorEditorSet();

private:
    Ui::OperatorEditorSet *ui;
};

#endif // OPERATOREDITORSET_H
