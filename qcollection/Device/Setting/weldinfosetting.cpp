#include "weldinfosetting.h"
#include "ui_weldinfosetting.h"
#include <QDebug>
#include <QSplitter>
#include <QMessageBox>
WeldInfoSetting::WeldInfoSetting(QWidget *parent,QLayout *layout,int width,int height) :
    QWidget(parent),
    ui(new Ui::WeldInfoSetting)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
//    initFont();
    ui->le_ammeter->setText("0");

}

WeldInfoSetting::~WeldInfoSetting()
{
    delete ui;
}

void WeldInfoSetting::receiveScaleWeight(QString str)
{
    ui->le_scale_weight->setText(str.remove("+").remove("kg").trimmed());
}

void WeldInfoSetting::receiveScaleCount(QString str)
{
    ui->le_scaleCount->setText(str.trimmed());
}

void WeldInfoSetting::receiveDistance(QString distance)
{
//    ui->le_distance->setText(distance.remove("cm"));
}

void WeldInfoSetting::receiveTemperature(QString temperature)
{
    ui->le_temperature->setText(temperature.remove("°c"));
}

void WeldInfoSetting::receiveAmmeterData(QString ammeter)
{
    ui->le_ammeter->setText(ammeter.remove("kWh"));
}

void WeldInfoSetting::receiveAmmeterVolData(QString vol)
{
    ui->le_vol->setText(vol.remove("V"));
}

void WeldInfoSetting::receiveLaserDistanceFromMain(QString laserdistance)
{
//    ui->le_laserDistance->setText(laserdistance.remove("m"));
}

void WeldInfoSetting::on_btn_back_clicked()
{
    this->deleteLater();
    emit exitcurr();
}

void WeldInfoSetting::initFont()
{
    {
        QFont f("DejaVu Sans",20);
        ui->lab_title->setFont(f);
    }
    {
        QFont f("DejaVu Sans",18);
        ui->label_2->setFont(f);
        ui->label_5->setFont(f);
        ui->label_9->setFont(f);
        ui->label_10->setFont(f);
        ui->label_11->setFont(f);
        ui->label_12->setFont(f);
        ui->label_13->setFont(f);
        ui->label_14->setFont(f);
        ui->label_15->setFont(f);
        ui->label_22->setFont(f);
        ui->label_23->setFont(f);
        ui->le_ammeter->setFont(f);
        ui->le_temperature->setFont(f);
        ui->lineEdit_3->setFont(f);
        ui->lineEdit_4->setFont(f);
        ui->le_vol->setFont(f);
        ui->le_scaleCount->setFont(f);
        ui->le_scale_weight->setFont(f);
        ui->lineEdit_8->setFont(f);
        ui->lineEdit_9->setFont(f);
        ui->lineEdit_12->setFont(f);
        ui->lineEdit_13->setFont(f);
//        ui->le_distance->setFont(f);
//        ui->lab_distance->setFont(f);
//        ui->lab_laser->setFont(f);
//        ui->le_laserDistance->setFont(f);

        ui->le_ammeter->setReadOnly(true);
        ui->le_temperature->setReadOnly(true);
        ui->lineEdit_3->setReadOnly(true);
        ui->lineEdit_4->setReadOnly(true);
        ui->le_vol->setReadOnly(true);
        ui->le_scaleCount->setReadOnly(true);
        ui->le_scale_weight->setReadOnly(true);
        ui->lineEdit_8->setReadOnly(true);
        ui->lineEdit_9->setReadOnly(true);
        ui->lineEdit_12->setReadOnly(true);
        ui->lineEdit_13->setReadOnly(true);
//        ui->le_distance->setReadOnly(true);

    }
}
