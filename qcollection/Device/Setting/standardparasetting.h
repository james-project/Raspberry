#ifndef STANDARDPARASETTING_H
#define STANDARDPARASETTING_H

#include <QWidget>
#include "common.h"
#include <QTimer>
#include <QLineEdit>
#include "Device/HttpInteractiveData/mashwelderinteractivedata.h"
#include <QJsonObject>

namespace Ui {
class StandardParaSetting;
}


class StandardParaSetting : public QWidget
{
    Q_OBJECT

public:
    explicit StandardParaSetting(QWidget *parent = NULL,QLayout *layout = NULL,int width = 0,int height = 0,StandardPara *mStandardPara=NULL);
    ~StandardParaSetting();
//    static StandardParaSetting *getobj();
    bool eventFilter(QObject *watched, QEvent *event);

    void inputStandardParaValue(int max, int min);
    void inputSqValue(int max, int min);
    void inputSoValue(int max, int min);
    void inputS1Value(int max, int min);
    void inputW1Value(int max, int min);
    void inputCOValue(int max, int min);
    void inputW2Value(int max, int min);
    void inputHOValue(int max, int min);
    void inputOFValue(int max, int min);
    void inputPreVolValue(int max, int min);
    void inputWeldVolValue(int max, int min);
    void inputWeldingPreCounts(int max, int min);
private slots:
    void on_btn_back_clicked();
public slots:
    void writeStandardParaSet_slots();
    void writeStandardSQSet_slots();
    void writeStandardSOSet_slots();
    void writeStandardS1Set_slots();
    void writeStandardW1Set_slots();

    void writeStandardCOSet_slots();
    void writeStandardW2Set_slots();
    void writeStandardHOSet_slots();
    void writeStandardOFSet_slots();
    void writeStandardPrecurrSet_slots();
    void writeStandardWeldcurrSet_slots();
    void receiveStandard_refresh(StandardPara*);
signals:
    void exitcurr();    
    void termitTime_stanSet();
    void StandardParaValue(int);
    void StandardSQValue(int);
    void StandardSOValue(int);
    void StandardS1Value(int);
    void StandardW1Value(int);
    void StandardCOValue(int);
    void StandardW2Value(int);
    void StandardHOValue(int);
    void StandardOFValue(int);
    void StandardPrecurrValue(int);
    void StandardWeldcurrValue(int);
    void StandardWeldPreCounts(int);
    void closeInputDialog_ByHeadMan();//TODO 20180730 //组长权限刷卡关掉输入对话框
    void closeSenconConfirmDialog_ByQcMan();//TODO 20180730 //QC权限刷卡关掉输入对话框

private:
    Ui::StandardParaSetting *ui;
    void initFont();
//    static StandardParaSetting *mInstance;
    QTimer *m_circleTime;
//    StandardPara *m_standardPara;
public:
    static bool _bHeadManPass;
    static bool _bQcPass;
};

#endif // STANDARDPARASETTING_H
