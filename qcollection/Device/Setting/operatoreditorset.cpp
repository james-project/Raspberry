#include "operatoreditorset.h"
#include "ui_operatoreditorset.h"

OperatorEditorSet::OperatorEditorSet(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OperatorEditorSet)
{
    ui->setupUi(this);
}

OperatorEditorSet::~OperatorEditorSet()
{
    delete ui;
}
