#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include "ReadUsbPort/readusbport.h"
#include "ProductionOrder/productionordermanagement.h"
#include "mainwindow.h"
#include "DeviceState/devicestate.h"
#include "Dialog/fullscreenmodaldlg.h"
#include <QTimer>
#include "Dialog/mymessagedialog.h"
#include "http/HttpClient/httpclient.h"
#include "ProductionOrder/productionlistview.h"
#include "http/HttpServer/httpserver.h"
#include "qextserial/qextserialport.h"
#include "deviceidsettings.h"

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(MainWindow *parent = 0);
    ~MainWidget();
    void initSpotCheckEquipmentDialog();
    void initQCCheckDialog();
    virtual void queryCurrentOrder(ProductionVariant & varient,QString currentTaskId);
    DeviceInfo *getDeviceInfoObj();
    HttpClient *getHttpClientObj();
    DeviceState *getDeviceStateObj();
    fullScreenModalDlg *firstProductionCheckDialog;
    void confirmMateral();
    void initDevice();
    void initSome();
    void initSpotCheckDialogParama();
    void initQCCheckDialogParama();
    inline QString getCurrentTaskId() {
        return m_currentTaskId;
    }
    virtual void initButtonSlot() = 0;
    /*!
         * \brief initScaleSerialSetting
         * 初始化获取电子秤重量的串口
         */
    virtual void initScaleSerialSetting() = 0;
    virtual void saveElectrodeCountToDataBase() = 0;
protected:
    int _iRestartFlag;
signals:
    /*!
     * \brief spotCheckPerrmisionOK -- 点检权限通过
     */
    void spotCheckPerrmisionOK();
    void qcCheckPermissionOK();
    void closeFirstCheckDialog();
    void closeFirstProductionCheckDialog();
    void qcCHKPermissionOK();
    void materialConfirm_signals();
    void sig_firstCheck();

    /*!
     * \brief startFirstProduction
     * 开始首件生产
     */
    void hanjieguifanNum(ProductionVariant);
    void startFirstProduction();
    void enableOrderConfirm();
    void materialConfirmOK();
    void showOrderTips();
    void opreatorCheckOK_signals();
    void closeDefectiveTips();
public slots:
    void spotCheckDelay_Tips();//设备点检超时，出现提示对话框
    void showDeviceCheckTips();
    void showQCCheckTips();
    void testConfirmMaterial();
protected:
    /*!
     * \brief getUsbPortName
     * 以USB的固定端口号获取USB的端口名称
     */
    void getUsbPortName();
    virtual void showProductionOrder();
    virtual void deleleMashWarningDialog();
    virtual void clearWeldingCount();
    virtual void clearPressingCount();
    virtual void enableButton(bool) = 0;
    void initHttpServer();
    void saveProductionVariant(QJsonObject &obj,ProductionVariant &productInfo);
    void insertProductionOrderToDatabase(ProductionVariant &productInfo);
    virtual void setOrderButtonShow() = 0;
    virtual void setOrderButtonDisabled()=0;
    virtual void removeDefectNumEvent()=0;
    virtual void setOrderButtonHidden() = 0;
    virtual void deleteSpotScrollText() = 0;
    virtual void startSpotCheckTimer() = 0;
    virtual void setCurrentOrderStatusShow(ProductionVariant &productInfo) = 0;
    virtual void sendCloseInputDialogSignal_ByHeadMan() = 0;
    virtual void sendCloseConfirmDialogSignal_ByQcMan() = 0;
    virtual void showDefectiveNumTips();
    virtual void setDefectValue()=0;

protected slots:
    void receiveHttpUserPermission(QString);
    void receiveStation_fromHttp(QString);
    void orderCompleted_slots();
    void firstProductionOK_toScanOperator();
    void firstProductCheckOK_Tips();
    void firstQcCheck();
    void showOrderCheckOK_Tips();
    void materialConfirm_slots();
    void materialConfirmOK_slots();
    virtual void startFirstProduction_slots() = 0;
    virtual void slot_hanjieguifanhao(ProductionVariant);
    void qcCheckFun();
    void firstQCCheckPass_Tips();
    void showDeviceFirstCheckTips();
    void postOrder();
    /*!
     * \brief receiveHttpClientData
     * 本地作为服务器接收Web 以Http Post方式发来的数据
     */
    void receiveHttpClientData(QByteArray,QByteArray);
    virtual void spotCheckManualExecute() = 0;
    virtual void spotCheckTimerTips() = 0;

    virtual void confirmCurrentOrderInfoByPostWay(ProductionVariant);
    void returnFromProductOrderList();
    void operatorCheckOk_Slots();
    virtual void recordScaleData_slots() = 0;
    virtual void escFromSettings() = 0;
    void defectiveNumTipsCheckOK();
    /*!
     * \brief readScaleCom
     * 读称重串口
     */
    virtual void readScaleCom() = 0;
public:
    HttpClient *m_httpClient;
    DeviceInfo *m_deviceInfo;
protected:
    MainWindow *mParent;
    UsbPortName m_usbPortName;
    ProductionOrderManagement *m_productionOrderManagement;
    DeviceState * m_deviceState;
    /*!
     * \brief m_spotCheckDialog
     * 开机后，开始设备点检;
     */
    fullScreenModalDlg *m_spotCheckDialog;
    fullScreenModalDlg *m_qcCheckDialog;
    fullScreenModalDlg *defectiveNumTipsDialog;
    fullScreenModalDlg *firstQCCheckDialog;
    /*!
     * \brief m_spotChekDelayTimer
     * 开机之后，超过设定时间，点检对话框自动消失；
     */
    QTimer    *m_spotChekDelayTimer;


    QString m_currentStation;
    int order_doing_id;
    bool order_finished_flag;
    bool m_materialBarcodeScanFlag;
    QTimer *m_qcCheck_circleAskTimer;
    QTimer *m_qc_checkTimer;
    fullScreenModalDlg *materialCheckDialog;
    ProductionListView *m_productListView;
    HttpServer *mHttpServer;
    ProductionVariant tProductionInfo;
    QTimer *m_spotDelayTimer;
    QTimer *m_qc_checkDelayTimer;
    QString m_currentProductName;
    QString m_currentWorkNum;
    QString m_currentWorkStartTime;
    QString m_currentTaskId;
    QextSerialPort *m_scaleCom;
    QextSerialPort *m_ldmCom;
    QextSerialPort *m_ldmCom2;
    bool  bAutoCompleted;
public:
    static bool _QCCheckPass;
    static bool _FirstCheckPass;
    static QString getDeviceId();
    static bool _hasModule;
protected slots:
    virtual void  slot_send7data();
    virtual void  slot_clearOrderMsg();

    virtual void  slot_sendOperator(QString, QString,  DeviceState::DeviceStion );
    virtual void  slot_sendMaterial(QString, QString , QString);
    virtual void  slot_sendGongZhang(QString, QString );

signals:
    void sig_orderCompleted();
};

#endif // MAINWIDGET_H
