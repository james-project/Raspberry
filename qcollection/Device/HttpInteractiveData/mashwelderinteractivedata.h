#ifndef MASHWELDERINTERACTIVEDATA_H
#define MASHWELDERINTERACTIVEDATA_H

#include "httpinteractivedata.h"


class MashwelderInteractiveData : public HttpInteractiveData
{
    Q_OBJECT
public:    
    explicit MashwelderInteractiveData(QObject *parent = 0);
    ~MashwelderInteractiveData();
    void initPara();
    static MashwelderInteractiveData *getobj();
    virtual void posDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para);
    virtual void putDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para);
    inline void setStartOrder(int start)
    {
        m_start_order = start;
    }

    inline int getStartOrder()
    {
        return m_start_order;
    }

    inline void setPutOrder(int putOrder)
    {
        m_put_order = putOrder;
    }

    inline int getPutOrder()
    {
        return m_put_order;
    }


signals:

public slots:
private:
    static MashwelderInteractiveData *m_mashwelderInteractiveData;

    /*!
     * \brief m_start_order
     * http post order;
     */
    int m_start_order;
    /*!
     * \brief m_put_order
     * http put order;
     */
    int m_put_order;
};

#endif // MASHWELDERINTERACTIVEDATA_H
