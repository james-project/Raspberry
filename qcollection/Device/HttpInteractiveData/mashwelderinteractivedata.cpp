#include "mashwelderinteractivedata.h"
#include "http/HttpClient/httpclient.h"

MashwelderInteractiveData::MashwelderInteractiveData(QObject *parent) :
    HttpInteractiveData(parent)
{
    initPara();
}

MashwelderInteractiveData::~MashwelderInteractiveData()
{

}

void MashwelderInteractiveData::initPara()
{

}

MashwelderInteractiveData *MashwelderInteractiveData::m_mashwelderInteractiveData = NULL;
MashwelderInteractiveData *MashwelderInteractiveData::getobj()
{
    if(m_mashwelderInteractiveData == NULL)
    {
        m_mashwelderInteractiveData = new MashwelderInteractiveData();
    }
    return m_mashwelderInteractiveData;
}

void MashwelderInteractiveData::posDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para)
{

}

void MashwelderInteractiveData::putDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para)
{

}
