#ifndef HTTPINTERACTIVEDATA_H
#define HTTPINTERACTIVEDATA_H

#include <QObject>
#include "Device/DeviceState/devicestate.h"

typedef struct __StandardPara
{
    QString weldStandardNum;//焊接规范号
    QString sq;
    QString so;
    QString s1;
    QString w1;
    QString co;
    QString w2;
    QString ho;
    QString of;
    QString currWeld1Vol;
    QString currWeld2Vol;
    QString preCurrVol;
    QString weldCurrVol;
    QString weldCount;//焊接计数器
    QString weldPreCount;//焊接预置数
    QString scaleWeight;//落料重量
    QString scaleCount;  //电子秤计数参考
    QString calibrationCount; //自学习计数
    QString scale_unitWeight;//单重
    QString ultraSonicDistance;
    QString temperarture;
    QString ammeterData;
    QString ammeterVolData;
    QString objCount;
    QString defectiveNumber;//不良品数
    QString totalCount_FromScale;//实际生产的累计数量
    QString distance;
    QString temperarture1;
    QString temperarture2;
    QString temperarture3;
    QString voltage1;
    QString voltage2;
    QString voltage3;
    QString voltage4;
    QString ammeterData1;
    QString ammeterData2;
    QString ammeterData3;
    QString ammeterData4;
}StandardPara;

typedef struct __StandardParaText
{
    QString weldStandardNum_text;//焊接规范号
    QString sq_text;
    QString so_text;
    QString s1_text;
    QString w1_text;
    QString co_text;
    QString w2_text;
    QString ho_text;
    QString of_text;
    QString currWeld1Vol_text;
    QString currWeld2Vol_text;
    QString preCurrVol_text;
    QString weldCurrVol_text;
    QString weldCount_text;//焊接计数器
    QString weldPreCount_text;//焊接预置数
    QString scaleWeight_text;//落料重量
    QString scaleCount_text;
    QString ultraSonicDistance_text;
    QString temperarture_text;
    QString ammeterData_text;
    QString ammeterVolData_text;
    QString objCount_text;
}StandardParaText;



typedef enum __WgtChanged
{
    PLASTIC_MAIN_WGT,
    DELAYTIME_SETTING1_WGT,
    DELAYTIME_SETTING2_WGT,
    INPUT_STATUS_WGT,
    MAIN_MENU_WGT,
    OUTPUT_STATUS_WGT,
    PRESSESS_INFO_WGT,
    PROCESS_SELECTION_WGT
}WgtChanged;


typedef struct __DelayTimeSet1Para //存储延时设定1的参数
{
    QString LowPressureCount;//低压排气次数
    QString HighPressureCount;//高压排气次数
    QString LowProtectTime;//低压保压延时时间
    QString PressureReliefDelayTime;//泄压延时时间
    QString LowPressureDelayTime;//低压排气延时时间
    QString HighProtectDelayTime;//高压保压延时时间
}DelayTimeSet1Para;

typedef struct __DelayTimeSet2Para //存储延时设定1的参数
{
    QString PressureReliefDelayTime2;//泄压延时2时间
    QString HighPaiQiDelayTime;//高压排气延时
    QString ChengxingBaoyaDelayTime;//成型保压延时
    QString DingchuDelayTime;//顶出延时时间
    QString PressureReliefDelayTime;//泄压延时时间
    QString HandshakeSignal;//握手信号
}DelayTimeSet2Para;


class HttpInteractiveData : public QObject
{
    Q_OBJECT
public:
    explicit HttpInteractiveData(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void posDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para) = 0;
    virtual void putDetailFun(DeviceInfo *device_info,StandardParaText *standardpara_text,StandardPara *standard_para) = 0;
};

#endif // HTTPINTERACTIVEDATA_H
