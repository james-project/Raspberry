#include "devicestate.h"


DeviceState *DeviceState::m_deviceInstance = NULL;
DeviceState::DeviceState(QObject *parent) :
    QObject(parent),
    m_machineState(MACHINE_STOP),
    m_deviceStationInfo(FIRST_CHECK_STATION),
    m_orderState(QString("")),
    m_qc_check_flag(QC_CHECKED_NG)
{
    initPara();
}

void DeviceState::initPara()
{
//    m_machineState = MACHINE_STOP;
//    m_deviceStationInfo = FIRST_CHECK_STATION;
}

DeviceState *DeviceState::getObj()
{
   if(m_deviceInstance==NULL)
       m_deviceInstance = new DeviceState();
   return m_deviceInstance;
}

void DeviceState::getMachineStateString(QString &stateString)
{
    int state = getMachineState();
    switch (state) {
    case MACHINE_STOP:
        stateString = "stop";
        break;
    case MACHINE_IDLE:
        stateString = "idle";
        break;
    case MACHINE_WORKING:
        stateString = "processing";
        break;
    case MACHINE_PAUSE:
        stateString = "pause";
        break;
    case MACHINE_CANCEL:
        stateString = "cancel";
        break;
    case MACHINE_MAINTENANCE:
        stateString = "maintenance";
        break;
    case MACHINE_ORDER_COMPLETED:
        stateString = "completed";
        break;
    default:
        stateString = "stop";
        break;
    }
}

void DeviceState::getDeviceStationString(QString &stateString)
{
    int state = getDeviceStation();
    switch (state) {
    case FIRST_CHECK_STATION:
        stateString = "Firstcheck";
        break;
    case QC_CHECK_STATION:
        stateString = "QCcheck";
        break;
    case OPERATOR_WORK_STATION:
        stateString = "Operatorwork";
        break;
    case MAINTAIN_STATION:
        stateString = "Maintain";
        break;
    default:
        stateString = "Operatorwork";
        break;
    }
}
