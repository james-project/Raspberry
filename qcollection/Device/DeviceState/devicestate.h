#ifndef DEVICESTATE_H
#define DEVICESTATE_H
#include <QObject>
#define QC_CHECKED_PASS 1
#define QC_CHECKED_NG   0

typedef struct __DeviceInfo //和Webservice 服务器通讯的设备信息
{
    QString deviceType;
    QString deviceId;
    QString property;
    QString value;
    QString sequence;
}DeviceInfo;



class DeviceState : public QObject
{
    Q_OBJECT
public:
    enum MachineState
    {
        MACHINE_STOP,           //未启动
        MACHINE_IDLE,           //空闲
        MACHINE_WORKING,        //生产中
        MACHINE_PAUSE,          //暂停
        MACHINE_CANCEL,         //取消
        MACHINE_MAINTENANCE,     //维护中
        MACHINE_ORDER_COMPLETED //设备接收的订单完成生产
    };

    enum DeviceStion
    {
        FIRST_CHECK_STATION,        //HeadMan
        QC_CHECK_STATION,           //
        OPERATOR_WORK_STATION,      //生产中
        MAINTAIN_STATION,           //
    };


    explicit DeviceState(QObject *parent = 0);
    void initPara();
    static DeviceState* getObj();

    inline void setMachineState(int status)
    {
        m_machineState = status;
    }

    inline int getMachineState()
    {
        return m_machineState;
    }
    void getMachineStateString(QString &stateString);

    /*-----------------------------------------*/
    inline void setDeviceStation(int station)
    {
        m_deviceStationInfo = station;
    }
    inline int getDeviceStation()
    {
        return m_deviceStationInfo;
    }
    void getDeviceStationString(QString &stationString);
    /*-----------------------------------------*/

    inline void setOrderState(QString orderState)
    {
        m_orderState = orderState;
    }
    inline QString getOrderState()
    {
        return m_orderState;
    }

    inline void setQcCheckStatus(bool qc_status){
        m_qc_check_flag = qc_status;
    }

    inline bool getQcCheckStatus(){
        return m_qc_check_flag;
    }

signals:

public slots:

private:
    static DeviceState *m_deviceInstance;
    int m_machineState;   
    int m_deviceStationInfo;
    QString m_orderState;
    bool m_qc_check_flag;
};

#endif // DEVICESTATE_H
