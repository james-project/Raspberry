#ifndef INPUTSTATUS_H
#define INPUTSTATUS_H

#include <QWidget>
#include <QPainter>

namespace Ui {
class InputStatus;
}

class InputStatus : public QWidget
{
    Q_OBJECT

public:
    explicit InputStatus(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~InputStatus();
    void paintEvent(QPaintEvent *event);

private slots:
    void on_btn_return_clicked();
    void receiveInputStatusInfo(QString);

signals:
    void exitCurr();

private:
    Ui::InputStatus *ui;
};

#endif // INPUTSTATUS_H
