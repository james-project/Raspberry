#ifndef PROCESSSELECTION_H
#define PROCESSSELECTION_H

#include <QWidget>

namespace Ui {
class ProcessSelection;
}

class ProcessSelection : public QWidget
{
    Q_OBJECT

public:
    explicit ProcessSelection(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~ProcessSelection();

signals:
    void exitCurr();
    /*!
     * \brief lowPressStatusEnable_signals : 写有低压排气
     */
    void lowPressStatusEnable_signals();

    /*!
     * \brief lowPressStatusDisable_signals : 写无低压排气
     */
    void lowPressStatusDisable_signals();

    /*!
     * \brief afterCompactionEnable_signals : 写有补压
     */
    void afterCompactionEnable_signals();

    /*!
     * \brief afterCompactionDisable_signals : 写无补压
     */
    void afterCompactionDisable_signals();

    void chouXinGangMode_signals(bool);// true : auto ; false : mannual
    void motorWorkMode_signals(bool);
    void GaoYaPaiQiMode_signals(bool);

private slots:
    void on_btn_return_clicked();
    void receiveProcessInfoStatus(QString);
    /*!
     * \brief writeLowPressureStatus : 写低压排气模式
     */
    void writeLowPressureStatus();

    /*!
     * \brief writeAftercompactionMode : 写补压模式
     */
    void writeAftercompactionMode();


//    void writeChouXinGangMode(bool);

private:
    Ui::ProcessSelection *ui;
};

#endif // PROCESSSELECTION_H
