#ifndef PLASTICPRESSES_H
#define PLASTICPRESSES_H

#include <QWidget>
#include <QKeyEvent>
#include "Device/PlasticPresses/mainmenu.h"
#include "Device/Setting/contactusset.h"
#include "Device/Setting/logonrightsedit.h"
#include "Device/PlasticPresses/plasticpressessinfo.h"
#include "Device/HardWare/AmmeterScale/ammeterscale.h"
#include "Device/HardWare/TemperatureDetect/temperaturethread.h"
#include "Device/HardWare/Gpio/ultrasonicthread.h"
#include "http/HttpClient/httpclient.h"
#include "http/HttpServer/httpserver.h"
#include <QTimer>
#include <QPainter>
#include "MitsubishiPLC/mitsubishiwnd.h"
#include "common.h"
#include "ProductionOrder/productionordermanagement.h"
#include "Socket/tcpsocket.h"
#include "ReadUsbPort/readusbport.h"
#include "Device/mainwidget.h"
#include "Device/HttpInteractiveData/httpinteractivedata.h"
#include <linux/kernel.h>
#include <linux/unistd.h>
#include <sys/sysinfo.h>


namespace Ui {
class PlasticPresses;
}

class PlasticPresses : public MainWidget
{
    Q_OBJECT

public:
    /*----zhanglong*/
    static int m_systemStatus;
    int l_systemStatus;
    static bool Equipment_ckeck;
    static bool l_Equipment_ckeck;
    static bool QC_Scan_ckeck;
    static bool l_QC_Scan_ckeck;
    static bool _orderBegin;
    QJsonObject _objToserver;
    QJsonArray _jsArrToserver;
    QTimer* _timerSendToserver;

    typedef struct __CurrentWorkStatus
    {
        QString start;//焊接规范号
        QString currSetValue;
        QString currRealvalue;
        QString end;
        QString workStatus;
        QString objCount;
        QString dumpEnergy;//耗电量
        QString temperature;
        QString temperature_OfModule2;
    }CurrentWorkStatus;
    CurrentWorkStatus mCurrentWorkStatus;
    explicit PlasticPresses(MainWindow *parent = 0);
    ~PlasticPresses();
    /*----zhanglong*/
    void initLEDcycleTime();
    void getCurrentWgtIndex();
    void initUi();
    void initFont();
    void initText();
    virtual void initButtonSlot();
    void initAmmeterScaleModbusSlave();
    void initQCScanCheck();
    void setFlash(QWidget *ed);
    void showEditorText();
    void hideEditorText();
    void timerEvent(QTimerEvent *event);
    void showFlashLabel();
    void sendOperatorAndMaterial();

    QString text();
    void initHttpClient();
    void initHttpServer();
    void initSocketTest();
    void initOrderRecordShow();
    virtual void setOrderButtonShow();
    virtual void setOrderButtonDisabled();
    virtual void removeDefectNumEvent();
    virtual void setOrderButtonHidden();
    virtual void deleteSpotScrollText();
    virtual void startSpotCheckTimer();
    virtual void setDefectValue();
    virtual void setCurrentOrderStatusShow(ProductionVariant &productInfo);
    virtual void sendCloseInputDialogSignal_ByHeadMan();
    virtual void sendCloseConfirmDialogSignal_ByQcMan();
    virtual void enableButton(bool);
    void initSpotDelayTime();
    void postWarnData(QString,QString);
    /*!
         * \brief initScaleSerialSetting
         * 初始化获取电子秤重量的串口
         */
    virtual void initScaleSerialSetting();
    virtual void saveElectrodeCountToDataBase();
    QString m_current_taskId;
    bool eventFilter(QObject *watched, QEvent *event);
    void inputDefectiveNum();
    int _badNum;
    virtual void showDefectiveNumTips();

public slots:
   virtual void slot_clearOrderMsg();

private slots:
    void deviceCircleSend();
    void showMainMenu();
    void showContactUsSetting();
    void showLogonRightsEditSetting();
    void receiveExitContact_Slot();
    void receiveExitMainMenu_Slot();
    void deviceCircleTimeSlot();
    virtual void QCCheckTimerTips();
    void showSysInfo();
    virtual void QCCheckManualExecute();
    /*----zhanglong*/
    void LEDFrashCircleTimeSlot();
    void readAmmeterPowerData(float);
    void readTemperature(int);
    void readTemperature_OfModule2(int);
    void updateReceiSubWgtIndex(WgtChanged);
    void receiveLowPressEnable_OfMainwgt();
    void receiveLowPressDisable_OfMainwgt();
    void receiveAftercompactionEnable_OfMainwgt();
    void receiveAftercompactionDisable_OfMainwgt();
    void receiveLowPressureCount_OfMainwgt(QString,QString);
    void receiveHighPressureCount_OfMainwgt(QString,QString);
    void receiveLowProtectDelaytime_OfMainwgt(QString,QString);
    void receiveReliefPressureDelaytime_OfMainwgt(QString,QString);
    void receiveLowPressurePaiqiDelaytime_OfMainwgt(QString,QString);
    void receiveHighPressureBaoyaDelaytime_OfMainwgt(QString,QString);
    void receiveReliefPressureDelaytime2_OfMainwgt2(QString,QString);
    void receiveHighPressurePaiqiDelaytime_OfMainwgt2(QString,QString);
    void receiveMoldingBaoyaDelaytime_OfMainwgt2(QString,QString);
    void receiveTopoutDelaytime_OfMainwgt2(QString,QString);
    void receiveReliefPressureDelaytime_OfMainwgt2(QString,QString);
    void powerOff();
    void receiveipStatus_slots(bool);
    void writeDataToTcpServer();
    void updateCurrentCount();
    void updateCurrentCountCirle();
    virtual void startFirstProduction_slots();
    virtual void spotCheckManualExecute();
    virtual void spotCheckTimerTips();
    virtual void recordScaleData_slots();
    virtual void escFromSettings();
    /*!
     * \brief readScaleCom
     * 读称重串口
     */
    virtual void readScaleCom();
    void updateDeviceState_Slots();
    void acceptDefectiveValue(int);

private slots:
    void parseWorkStatus(QString);
    void parseObjCount(QString);
    /*!
     * \brief parseProcessSelectionStatus
     *解析工艺选择参数的状态
     */
    void parseProcessSelectionStatus(QString);

    /*!
     * \brief parseInputStatus
     * 解析输入状态指示灯的状态
     */
    void parseInputStatus(QString);
    /*!
     * \brief parseOutputStatus
     * parse the Output status
     */
    void parseOutputStatus(QString);
    void parseDelay1Data1(QString);
    void parseDelay1Data2(QString);
    void parseDelay1Data3(QString);
    void parseDelay1Data4(QString);
    void parseDelay1Data5(QString);
    void parseDelay1Data6(QString);
    void parseDelay2Data1(QString);
    void parseDelay2Data2(QString);
    void parseDelay2Data3(QString);
    void parseDelay2Data4(QString);
    void parseDelay2Data5(QString);
    /*!
     * \brief receiveHttpClientData
     * 本地作为服务器接收Web 以Http Post方式发来的数据
     */
    //void receiveHttpClientData(QByteArray,QByteArray);
    void receiveplcConnnectedStatus(bool);
    void httpRegister_slots(bool);
    void receiveHttpRegisterOK(bool);
    void receiveProductionVariant_Main(ProductionVariant);
    void on_settingBtn_clicked();
    void on_btn_clearCount_clicked();
    void on_le_objCount_textChanged( QString arg1);

    void on_pushButton_clicked();

signals:
    void sig_autoCompleted(int);
    void sig_exitInput();
    void sig_btnOk_clicked();
    void inputValue(int);

    void showAmmeterData(QString);
    void showTemperatureData(QString);
    void showTemperature_OfModule2(QString);
    void sendCurrentWgtIndex(WgtChanged);
    /*!
     * \brief sendProcessInfo
     *发送工艺选择参数的信息到工艺选择界面显示
     */
    void sendProcessInfo(QString);
    /*!
     * \brief sendInputStatusInfo
     *发送输入状态的指示灯信息给子界面显示
     */
    void sendInputStatusInfo(QString);
    void sendOutputStatusInfo(QString);
    void sendDelay1Info(DelayTimeSet1Para);
    void sendDelay2Info(DelayTimeSet2Para);
    void writeLowPressFlag(bool);
    void writeAftercompactionFlag(bool);
    void writeChouXinGangFlag(bool);
    void writeMotorModeFlag(bool);
    void writeGaoYaPaiQiFlag(bool);
    void writeLowPressureCount(QString value,QString bytesCount);
    void writeHighPressureCount(QString value,QString bytesCount);
    void writeLowProtectDelaytime(QString value,QString bytesCount);
    void writeReliefPressureDelaytime(QString value,QString bytesCount);
    void writeLowPressurePaiqiDelaytime(QString value,QString bytesCount);
    void writeHighPressureBaoyaDelaytime(QString value,QString bytesCount);
    void writeReliefPressureDelaytime2_2(QString value,QString bytesCount);
    void writeHighPressurePaiqiDelaytime_2(QString value,QString bytesCount);
    void writeMoldingBaoyaDelaytime_2(QString value,QString bytesCount);
    void writeTopoutDelaytime_2(QString value,QString bytesCount);
    void writeReliefPressureDelaytime_2(QString value,QString bytesCount);
    void sendIpStatus_OfMainWgt(bool);
    void plcConnnectedStatus_signals(bool);
    void sendProductionVariant(ProductionVariant);
    void openSettingSignal();
    void writeClearCountCmd();
    void sendScaleWeight(QString);
    void sendScaleCount(QString);
    void closeInputDialog_ByHeadMan();
    void closeSenconConfirmDialog_ByQcMan();

private:
    void sendNotGood();
    void posDetailFun(QString sequence);
    void putDetailFun(QString sequence);
    void initFlash();
    void initMitsubishiPlcFun();
    virtual void clearPressingCount();
    void deleteFun();


    QTimer* timerMemory;
    QTimer* timerRestart;
    Ui::PlasticPresses *ui;
    MainMenu *m_mainmenu;
    ContactUsSet *m_ContactUsSet;
    LogonRightsEdit *m_LogonRightsEdit;
    PlasticPressessInfo *m_PlasticPressessInfo;
    AmmeterScale     *m_ammeterDate;
    /*----zhanglong*/
    QStringList AlarmList;
    int Alarm_Count;
    int TeAlarm_Count;
    QTimer *m_freshLEDTimer;
    QTimer *m_circleTimer;
    QWidget *mEd;
    int timeId;
    /*!
     * \brief heartId
     *心跳机制，定时向Webservice发送在线状态
     */
    int heartId;

    /*!
     * \brief stateId
     *状态机制，定时向Webservice个发送当前机器的状态等等
     */
    int timeDelayId;
    bool mFlasbEnable;
    QString mCurrStr;
    QColor mTextColor;
    QColor mBackColor;
    int mFlash;
    MitsubishiWnd * m_mitsubishi;

    WgtChanged mWgtChanged;
    DelayTimeSet1Para m_DelayTimeSet1Para;
    DelayTimeSet2Para m_DelayTimeSet2Para;
    bool m_plcStatus;
    /*!
     * \brief m_register_flag : defult is false that is not regist to the WebService;
     * than the value is true mean that the raspberry is connected to the WebService and that is registed to the WebService;
     */
    bool m_register_flag;
    TcpSocket *m_tcpSocket;
    ProductionVariant tProctionInfo;
    QString m_currentCount_temp;
    QString m_current_count_temp;
    QString m_currentWorkStatus_temp;
    QString m_currentSetValue_temp;
    QString m_currentRealValue_temp;
    QString m_dumpEnergy_temp;
    QTimer *m_currentCountUpdate_timer;
    bool m_post_once_flag;
    QJsonObject _DatasObj;
    QJsonObject getDatas();
    QJsonArray _workArr_pla;
    int m_task_timeId;
    bool m_orderFinish_flag;
    MyMessageDialog* _msgDlg;

    void sendUpdateTodayTask();
    void send7Datas();
public slots:
    void receiveStation_fromHttp(QString station);
    void on_btn_SpotCheck_clicked();
    void on_btn_QCCheck_clicked();
    void firstProductionOK_toScanOperator2();
    void firstQcCheck2();
    void slot_HaoDian(QString);
    void slot_Module_1_temp(QString);
    void slot_Module_2_temp(QString);
    void slot_chengZhong_weight(QString);
    void slot_chengZhong_count(QString);
    void slot_Delay1(DelayTimeSet1Para);
    void slot_Delay2(DelayTimeSet2Para);

private:
    int _iNum;
    QString startTime;
    QString endTime;
protected slots:
    virtual void slot_sendOperator(QString ,QString,  DeviceState::DeviceStion );
    virtual void slot_sendMaterial(QString ,QString, QString );
    virtual void slot_sendGongZhang(QString, QString );
};


#endif // PLASTICPRESSES_H

