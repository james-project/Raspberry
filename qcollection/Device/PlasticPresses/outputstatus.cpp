#include "outputstatus.h"
#include "ui_outputstatus.h"
#include <QDebug>

OutputStatus::OutputStatus(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OutputStatus)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
}

OutputStatus::~OutputStatus()
{
    delete ui;
}

void OutputStatus::on_btn_return_clicked()
{
    emit exitCurr();
}

void OutputStatus::receiveOutputStatusInfo(QString info)//
{
    qDebug() << "\n\n ~~~~OutputStatus::receiveOutputStatusInfo(QString): " << info<<info.at(0)<<info.at(1) ;
    if(info.length()>=16)
    {
        info = info.right(16);
        if((info.at(0) == '0')||(info.at(0) == ' '))
        {
            ui->btn_16->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(0) == '1')
        {
            ui->btn_16->setStyleSheet("border-image: url(:/res/red.png);");
        }
        if((info.at(1) == '0')||(info.at(1) == ' '))
        {
            ui->btn_15->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(1) == '1')
        {
            ui->btn_15->setStyleSheet("border-image: url(:/res/red.png);");
        }
        if((info.at(2) == '0')||(info.at(2) == ' '))
        {
            ui->btn_14->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(2) == '1')
        {
            ui->btn_14->setStyleSheet("border-image: url(:/res/red.png);");
        }
        if((info.at(3) == '0')||(info.at(3) == ' '))
        {
            ui->btn_13->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(3) == '1')
        {
            ui->btn_13->setStyleSheet("border-image: url(:/res/red.png);");
        }
        if((info.at(4) == '0')||(info.at(4) == ' '))
        {
            ui->btn_12->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(4) == '1')
        {
            ui->btn_12->setStyleSheet("border-image: url(:/res/red.png);");
        }
        if((info.at(5) == '0')||(info.at(5) == ' '))
        {
            ui->btn_11->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(5) == '1')
        {
            ui->btn_11->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(6) == '0')||(info.at(6) == ' '))
        {
            ui->btn_10->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(6) == '1')
        {
            ui->btn_10->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(7) == '0')||(info.at(7) == ' '))
        {
            ui->btn_9->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(7) == '1')
        {
            ui->btn_9->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(8) == '0')||(info.at(8) == ' '))
        {
            ui->btn_8->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(8) == '1')
        {
            ui->btn_8->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(9) == '0')||(info.at(9) == ' '))
        {
            ui->btn_7->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(9) == '1')
        {
            ui->btn_7->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(10) == '0')||(info.at(10) == ' '))
        {
            ui->btn_6->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(10) == '1')
        {
            ui->btn_6->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(11) == '0')||(info.at(11) == ' '))
        {
            ui->btn_5->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(11) == '1')
        {
            ui->btn_5->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(12) == '0')||(info.at(12) == ' '))
        {
            ui->btn_4->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(12) == '1')
        {
            ui->btn_4->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(13) == '0')||(info.at(13) == ' '))
        {
            ui->btn_3->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(13) == '1')
        {
            ui->btn_3->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(14) == '0')||(info.at(14) == ' '))
        {
            ui->btn_2->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(14) == '1')
        {
            ui->btn_2->setStyleSheet("border-image: url(:/res/red.png);");
        }

        if((info.at(15) == '0')||(info.at(15) == ' '))
        {
            ui->btn_1->setStyleSheet("border-image: url(:/res/green.png);");
        }
        else if(info.at(15) == '1')
        {
            ui->btn_1->setStyleSheet("border-image: url(:/res/red.png);");
        }
    }

}
