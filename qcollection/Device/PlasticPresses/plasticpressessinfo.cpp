#include "plasticpressessinfo.h"
#include "ui_plasticpressessinfo.h"
#include <QDebug>

PlasticPressessInfo::PlasticPressessInfo(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlasticPressessInfo)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    ui->label_2->setVisible(false);
    ui->lineEdit_7->setVisible(false);
    ui->lineEdit_8->setVisible(false);
}

PlasticPressessInfo::~PlasticPressessInfo()
{
    delete ui;
}

void PlasticPressessInfo::on_btn_back_clicked()
{
    emit exitcurr();
}

void PlasticPressessInfo::receiveAmmeterData(QString ammeter)
{
    ui->le_ammeter->setText(ammeter.remove("kWh"));
}

void PlasticPressessInfo::receiveTemperatureData(QString temp)
{
    //le_temperature
    ui->le_temperature->setText(temp.remove("C"));
}

void PlasticPressessInfo::receiveTemperature_OfModule2(QString temp)
{
    ui->le_temperature2->setText(temp.remove("C"));
}

void PlasticPressessInfo::receiveScaleWeight_FromPlastic(QString weight)
{
    ui->le_scaleWeight->setText(weight);
}

void PlasticPressessInfo::receiveScaleCount_FromPlastic(QString count)
{
    ui->le_scaleCount->setText(count.trimmed());
}

void PlasticPressessInfo::showIpCommunicataStatus(bool status)
{
    if(status)
    {
        ui->le_communicatStatus->setText("正常");
        ui->le_communicatStatus->setStyleSheet("color: rgb(78, 154, 6);background-color: rgb(255, 255, 255);");
    }
    else
    {
        ui->le_communicatStatus->setText("中断");
        ui->le_communicatStatus->setStyleSheet("color: rgb(255, 0, 0);background-color: rgb(255, 255, 255);");
    }
}

void PlasticPressessInfo::showPlcCommunicataStatus(bool status)
{
    if(status)
    {
        ui->le_communicatStatusPLC->setText("正常");
        ui->le_communicatStatusPLC->setStyleSheet("color: rgb(78, 154, 6);background-color: rgb(255, 255, 255);");
    }
    else
    {
        ui->le_communicatStatusPLC->setText("中断");
        ui->le_communicatStatusPLC->setStyleSheet("color: rgb(255, 0, 0);background-color: rgb(255, 255, 255);");
    }
}
