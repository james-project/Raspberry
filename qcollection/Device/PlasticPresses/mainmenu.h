#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include "Device/PlasticPresses/processselection.h"
#include "Device/PlasticPresses/delaytimesetting1.h"
#include "Device/PlasticPresses/delaytimesetting2.h"
#include "Device/PlasticPresses/inputstatus.h"
#include "Device/PlasticPresses/outputstatus.h"
#include "common.h"


namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~MainMenu();
    void initButtonSlot();
    static bool _bPassed;

signals:
    void exitCurr();
    void sendCurrentSubWgtIndex(WgtChanged);
    void sendProcessInfo(QString);
    void sendInputStatusInfo_fromPlastic(QString);
    void sendOutputStatusInfo(QString);
    void sendDelay1Info(DelayTimeSet1Para);
    void sendDelay2Info(DelayTimeSet2Para);
    void receiveLowPressEnable_OfMainMenu();
    void receiveLowPressDisable_OfMainMenu();

    void receiveAftercompactionEnable_OfMainMenu();
    void receiveAftercompactionDisable_OfMainMenu();
    void receiveChouXinGangMode_FromMenu(bool);
    void receiveMotorMode_OfMenu(bool);
    void receiveGaoYaPaiQiMode_OfMenu(bool);


    void receiveLowPressureCount_OfMainMenu(QString,QString);
    void receiveHighPressureCount_OfMainMenu(QString,QString);
    void receiveLowProtectDelaytime_OfMainMenu(QString,QString);
    void receiveReliefPressureDelaytime_OfMainMenu(QString,QString);
    void receiveLowPreesurePaiqiDelaytime_OfMainMenu(QString,QString);
    void receiveHighPreesureBaoyaDelaytime_OfMainMenu(QString,QString);

    void receiveReliefPressureDelaytime2_OfMainMenu2(QString,QString);
    void receiveHighPressurePaiqiDelaytime_OfMainMenu2(QString,QString);
    void receiveMoldingBaoyaDelaytime_OfMainMenu2(QString,QString);
    void receiveReliefPressureDelaytime_OfMainMenu2(QString,QString);
    void receiveTopoutDelaytime_OfMainMenu2(QString,QString);
    void closeInputDialog_ByHeadMan();
    void closeSenconConfirmDialog_ByQcMan();

private slots:
    void on_btn_return_clicked();
    void showProcessSelection();
    void showDelaySet1();
    void showDelaySet2();
    void showInputStatus();
    void showOutputStatus();
    void receiveExitSubMenu_Slot();
    /*!
     * \brief receiveProcessInfo
     *显示从主界面传来的工艺选择的信息
     */
    void receiveProcessInfo(QString);
    void receiveInputStatusInfo(QString);
    void receiveOutputStatusInfo(QString);
    void receiveDelay1Info(DelayTimeSet1Para);
    void receiveDelay2Info(DelayTimeSet2Para);
    void setValue_slot();

private:
    void getCurrentSubWgtIndex();

private:
    Ui::MainMenu *ui;
    ProcessSelection *m_process;
    DelaytimeSetting1 *m_delayset1;
    DelayTimeSetting2 *m_delayset2;
    InputStatus *m_input;
    OutputStatus *m_output;
};

#endif // MAINMENU_H
