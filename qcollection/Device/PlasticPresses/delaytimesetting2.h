#ifndef DELAYTIMESETTING2_H
#define DELAYTIMESETTING2_H

#include <QWidget>
#include "Device/HttpInteractiveData/httpinteractivedata.h"


namespace Ui {
class DelayTimeSetting2;
}

class DelayTimeSetting2 : public QWidget
{
    Q_OBJECT

public:
    explicit DelayTimeSetting2(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~DelayTimeSetting2();
    void initEventFilter();
    bool eventFilter(QObject *watched, QEvent *event);
    void inputDialogModual(QString showString,int max,int min, QString inputMode);

    /*!
     * \brief convertToSpecialString :将整形值转换为固定长度所QSting 如 8 ，长度为4 －－－> "0008",再掉换顺序 －－－－>"0800"
     * \param destString
     * \param sourceValue
     * \param fixLength
     * \param ratio
     */
    void convertToSpecialString(QString &destString,int sourceValue, int fixLength,int ratio);

signals:
    void exitCurr();
    void reliefPressureDelaytime2_signals(QString value,QString bytesCount);
    void highPreesurePaiqiDelaytime_signals(QString value,QString bytesCount);
    void moldingBaoyaDelaytime_signals(QString value,QString bytesCount);
    void topoutDelaytime_signals(QString value,QString bytesCount);
    void reliefPressureDelaytime_signals(QString value,QString bytesCount);

private slots:
    void on_btn_return_clicked();
    void receiveDelay2info(DelayTimeSet2Para);
    void acceptReliefPressureDelaytime2(float);
    void acceptHighPressurePaiqiDelaytime(float);
    void acceptMoldingBaoyaDelaytime(float);
    void acceptTopoutDelaytime(float);
    void acceptReliefPressureDelaytime(float);

private:
    Ui::DelayTimeSetting2 *ui;
    void shuaKa();
   public:
    static bool _headManPass;

};

#endif // DELAYTIMESETTING2_H
