#ifndef PLASTICPRESSESSINFO_H
#define PLASTICPRESSESSINFO_H

#include <QWidget>

namespace Ui {
class PlasticPressessInfo;
}

class PlasticPressessInfo : public QWidget
{
    Q_OBJECT

public:
    explicit PlasticPressessInfo(QLayout *layout,int width,int height,QWidget *parent=0);
    ~PlasticPressessInfo();

signals:
    void exitcurr();

private slots:
    void on_btn_back_clicked();
    void receiveAmmeterData(QString);
    void receiveTemperatureData(QString);
    void receiveTemperature_OfModule2(QString);
    void receiveScaleWeight_FromPlastic(QString);
    void receiveScaleCount_FromPlastic(QString);
    void showIpCommunicataStatus(bool);
    void showPlcCommunicataStatus(bool);

private:
    Ui::PlasticPressessInfo *ui;
};

#endif // PLASTICPRESSESSINFO_H
