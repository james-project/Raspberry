#ifndef OUTPUTSTATUS_H
#define OUTPUTSTATUS_H

#include <QWidget>

namespace Ui {
class OutputStatus;
}

class OutputStatus : public QWidget
{
    Q_OBJECT

public:
    explicit OutputStatus(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~OutputStatus();

private slots:
    void on_btn_return_clicked();
    void receiveOutputStatusInfo(QString);

signals:
    void exitCurr();

private:
    Ui::OutputStatus *ui;
};

#endif // OUTPUTSTATUS_H
