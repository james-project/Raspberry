#include "processselection.h"
#include "ui_processselection.h"
#include <QDebug>
#include "./mainmenu.h"

ProcessSelection::ProcessSelection(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProcessSelection)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    connect(ui->btn_1,SIGNAL(clicked(bool)),this,SLOT(writeLowPressureStatus()));//写低压排气模式
    connect(ui->btn_3,SIGNAL(clicked(bool)),this,SLOT(writeAftercompactionMode()));//写补压模式
    connect(ui->btn_5,SIGNAL(clicked(bool)),this,SIGNAL(chouXinGangMode_signals(bool)));//写抽芯缸模式
    connect(ui->btn_2,SIGNAL(clicked(bool)),this,SIGNAL(motorWorkMode_signals(bool)));//写 Motor work 模式
    connect(ui->btn_4,SIGNAL(clicked(bool)),this,SIGNAL(GaoYaPaiQiMode_signals(bool)));//写 高压排气 模式
}

ProcessSelection::~ProcessSelection()
{
    delete ui;
}

void ProcessSelection::on_btn_return_clicked()
{
    MainMenu::_bPassed = false;
    emit exitCurr();
}

void ProcessSelection::receiveProcessInfoStatus(QString info)
{
    qDebug() << "\n\n ~~~~ProcessSelection::receiveProcessInfoStatus(QString info) :::      info  - 4 -  6 : " << info<< info.at(4)<<info.at(6) ;//
    if(info.length()>=9)
    {
        info = info.right(9);
        if(info.at(0) == '0' || info.at(0) == ' ')
        {
            ui->btn_2->setChecked(false);
        }
        else if(info.at(0) == '1')
        {
            ui->btn_2->setChecked(true);
        }
        if(info.at(6) == '0' || info.at(6) == ' ')
        {
            ui->btn_1->setChecked(false);
        }
        else if(info.at(6) == '1')
        {
            ui->btn_1->setChecked(true);
        }

        if(info.at(2) == '0' || info.at(2) == ' ')
        {
            ui->btn_3->setChecked(false);
        }
        else if(info.at(2) == '1')
        {
            ui->btn_3->setChecked(true);
        }

        if(info.at(4) == '0' || info.at(4) == ' ')
        {
            ui->btn_4->setChecked(false);
        }
        else if(info.at(4) == '1')
        {
            ui->btn_4->setChecked(true);
        }

        if(info.at(8) == '0' || info.at(8) == ' ')
        {
            ui->btn_5->setChecked(false);
        }
        else if(info.at(8) == '1')
        {
            ui->btn_5->setChecked(true);
        }
    }
}

void ProcessSelection::writeLowPressureStatus()
{
    if(ui->btn_1->isChecked())
    {
        emit lowPressStatusEnable_signals();//写有低压排气
    }
    else
    {
        emit lowPressStatusDisable_signals();//写无低压排气
    }

}

void ProcessSelection::writeAftercompactionMode()
{
    if(ui->btn_3->isChecked())
    {
        emit afterCompactionEnable_signals();
    }
    else
    {
        emit afterCompactionDisable_signals();
    }
}

//void ProcessSelection::writeChouXinGangMode(bool)
//{
//    if(ui->btn_5->isChecked())
//    {
//        chouXinGangMode_signals(true);
//    }
//    else
//    {
//        chouXinGangMode_signals(false);
//    }
//}
