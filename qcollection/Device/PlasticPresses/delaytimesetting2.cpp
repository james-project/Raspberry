#include "delaytimesetting2.h"
#include "ui_delaytimesetting2.h"
#include "input/inputdialog.h"
#include <QDebug>
#include "./delaytimesetting1.h"
#include "./mainmenu.h"

DelayTimeSetting2::DelayTimeSetting2(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DelayTimeSetting2)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    initEventFilter();
}

DelayTimeSetting2::~DelayTimeSetting2()
{
    delete ui;
}

void DelayTimeSetting2::initEventFilter()
{
    ui->le_1_write->installEventFilter(this);
    ui->le_2_write->installEventFilter(this);
    ui->le_3_write->installEventFilter(this);
    ui->le_4_write->installEventFilter(this);
    ui->le_5_write->installEventFilter(this);
}

bool DelayTimeSetting2::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(watched == ui->le_1_write)
        {
            inputDialogModual("泄压延时2",200,0, "1");
        }
        else if(watched == ui->le_2_write)
        {
            inputDialogModual("高压排气延时",200,0, "2");
        }
        else if(watched == ui->le_3_write)
        {
            inputDialogModual("成型保压延时",5000,0, "3");
        }
        else if(watched == ui->le_4_write)
        {
            inputDialogModual("顶出延时",200,0, "4");
        }
        else if(watched == ui->le_5_write)
        {
            inputDialogModual("泄压延时",200,0, "5");
        }
    }
    return QWidget::eventFilter(watched,event);
}

void DelayTimeSetting2::inputDialogModual(QString showString, int max, int min, QString inputMode)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,showString,true,1,max,min,false); //TODO 20180719 Change to float mode that is true
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    qDebug()<< "+++++++++++++++++++++++++++  INNNNN ==000";
    if(inputMode.compare("1") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptReliefPressureDelaytime2(float)));
    }
    else if(inputMode.compare("2") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptHighPressurePaiqiDelaytime(float)));
    }
    else if(inputMode.compare("3") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptMoldingBaoyaDelaytime(float)));
    }
    else if(inputMode.compare("4") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptTopoutDelaytime(float)));
    }
    else if(inputMode.compare("5") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptReliefPressureDelaytime(float)));
    }
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void DelayTimeSetting2::convertToSpecialString(QString &destString, int sourceValue, int fixLength, int ratio)
{
    QString tempString = "";
    if(fixLength == 8)
    {
        tempString.sprintf("%08x", (int)sourceValue*ratio);
        QString leftStr = tempString.left(4);
        QString rightStr = tempString.right(4);
        leftStr = leftStr.right(2).append(leftStr.left(2));
        rightStr = rightStr.right(2).append(rightStr.left(2));
        destString = rightStr.append(leftStr);
    }
    else if(fixLength == 4)
    {
        tempString.sprintf("%04x", (int)sourceValue*ratio);
        QString leftStr = tempString.left(2);
        QString rightStr = tempString.right(2);
        destString = (rightStr.append(leftStr)).toUpper();
    }

}

void DelayTimeSetting2::on_btn_return_clicked()
{
    MainMenu::_bPassed = false;
    emit exitCurr();
}

void DelayTimeSetting2::receiveDelay2info(DelayTimeSet2Para info)
{
    ui->le_1->setText(info.PressureReliefDelayTime2);//泄压延时2时间
    ui->le_2->setText(info.HighPaiQiDelayTime);//高压排气延时
    ui->le_3->setText(info.ChengxingBaoyaDelayTime);//成型保压延时
    ui->le_4->setText(info.DingchuDelayTime);//顶出延时时间
    ui->le_5->setText(info.PressureReliefDelayTime);//泄压延时
}

void DelayTimeSetting2::acceptReliefPressureDelaytime2(float value_float)
{
    ui->le_1_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit reliefPressureDelaytime2_signals(destSting,"02");
}

void DelayTimeSetting2::acceptHighPressurePaiqiDelaytime(float value_float)
{
    ui->le_2_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit highPreesurePaiqiDelaytime_signals(destSting,"02");
}

void DelayTimeSetting2::acceptMoldingBaoyaDelaytime(float value_float)
{
    int value_int = (int)(value_float);
    ui->le_3_write->setText(QString::number(value_int));
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);

    qDebug() << Q_FUNC_INFO << destSting;
    emit moldingBaoyaDelaytime_signals(destSting,"02");
}

void DelayTimeSetting2::acceptTopoutDelaytime(float value_float)
{
    ui->le_4_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit topoutDelaytime_signals(destSting,"02");
}

void DelayTimeSetting2::acceptReliefPressureDelaytime(float value_float)
{
    ui->le_5_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit reliefPressureDelaytime_signals(destSting,"02");
}

void DelayTimeSetting2::shuaKa()
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,5000,0,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardSQValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}
