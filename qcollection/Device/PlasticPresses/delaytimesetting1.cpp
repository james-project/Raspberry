#include "delaytimesetting1.h"
#include "ui_delaytimesetting1.h"
#include <QDebug>
#include "./delaytimesetting2.h"
#include "./mainmenu.h"

DelaytimeSetting1::DelaytimeSetting1(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DelaytimeSetting1)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    initEventFilter();
}

DelaytimeSetting1::~DelaytimeSetting1()
{
    delete ui;
}

void DelaytimeSetting1::initEventFilter()
{
    ui->le_1_write->installEventFilter(this);
    ui->le_2_write->installEventFilter(this);
    ui->le_3_write->installEventFilter(this);
    ui->le_4_write->installEventFilter(this);
    ui->le_5_write->installEventFilter(this);
    ui->le_6_write->installEventFilter(this);
}

bool DelaytimeSetting1::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(watched == ui->le_1_write)
        {
            inputDialogModual("低压排气次数",200,0, "1");
        }
        else if(watched == ui->le_2_write)
        {
            inputDialogModual("高压排气次数",200,0, "2");
        }
        else if(watched == ui->le_3_write)
        {
            inputDialogModual("低压保压延时时间",200,0, "3");
        }
        else if(watched == ui->le_4_write)
        {
            inputDialogModual("泄压延时时间",200,0, "4");
        }
        else if(watched == ui->le_5_write)
        {
            inputDialogModual("低压排气延时",200,0, "5");
        }
        else if(watched == ui->le_6_write)
        {
            inputDialogModual("高压保压延时",200,0, "6");
        }
    }
    return QWidget::eventFilter(watched,event);
}

void DelaytimeSetting1::inputDialogModual(QString showString,int max, int min, QString inputMode)
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,showString,true,1,max,min,false); //TODO 20180718 Change float mode is true
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    if(inputMode.compare("1") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptLowPressureCountValue(float)));
    }
    else if(inputMode.compare("2") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptHighPressureCountValue(float)));
    }
    else if(inputMode.compare("3") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptLowProtectedDelaytime(float)));
    }
    else if(inputMode.compare("4") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptReliefPressureDelaytime(float)));
    }
    else if(inputMode.compare("5") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptLowPressurePaiqiDelaytime(float)));
    }
    else if(inputMode.compare("6") == 0)
    {
        connect(inputdialog,SIGNAL(inputValue(float)),this,SLOT(acceptHighPressureBaoyaDelaytime(float)));
    }
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}

void DelaytimeSetting1::convertToSpecialString(QString &destString, int sourceValue, int fixLength, int ratio)
{
    QString tempString = "";
    if(fixLength == 8)
    {
        tempString.sprintf("%08x", (unsigned char)sourceValue*ratio);
        QString leftStr = tempString.left(4);
        QString rightStr = tempString.right(4);
        leftStr = leftStr.right(2).append(leftStr.left(2));
        rightStr = rightStr.right(2).append(rightStr.left(2));
        destString = rightStr.append(leftStr);
    }
    else if(fixLength == 4)
    {
        tempString.sprintf("%04x", (unsigned char)sourceValue*ratio);
        QString leftStr = tempString.left(2);
        QString rightStr = tempString.right(2);
        destString = (rightStr.append(leftStr)).toUpper();
    }

}

void DelaytimeSetting1::on_btn_return_clicked()
{
    MainMenu::_bPassed = false;
    emit exitCurr();
}

void DelaytimeSetting1::receiveDelay1info(DelayTimeSet1Para info )
{
    ui->le_1->setText(info.LowPressureCount);
    ui->le_2->setText(info.HighPressureCount);
    ui->le_3->setText(info.LowProtectTime);
    ui->le_4->setText(info.PressureReliefDelayTime);
    ui->le_5->setText(info.LowPressureDelayTime);
    ui->le_6->setText(info.HighProtectDelayTime);

}

void DelaytimeSetting1::acceptLowPressureCountValue(float value_float)
{

    qDebug() <<  "\r\n\r\n"<<Q_FUNC_INFO << " float value: " << value_float << ( int ) (value_float);
    int value_int = (int)value_float;
    ui->le_1_write->setText(QString::number(value_int));
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    qDebug() << Q_FUNC_INFO << " Di Ya Pai Qi :" << destSting;
    emit lowPressureCout_signals(destSting,"02");
}

void DelaytimeSetting1::acceptHighPressureCountValue(float value_float)
{
    int value_int = (int)value_float;
    ui->le_2_write->setText(QString::number(value_int));
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit highPressureCout_signals(destSting,"02");
}

void DelaytimeSetting1::acceptLowProtectedDelaytime(float value_float)
{
    ui->le_3_write->setText(QString::number(value_float));
    int value_int  = (int)(value_float*10); //TODO 20180719 Add
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit lowProtectDelaytime_signals(destSting,"02");
}

void DelaytimeSetting1::acceptReliefPressureDelaytime(float value_float)
{
    ui->le_4_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit reliefPressureDelaytime_signals(destSting,"02");
}

void DelaytimeSetting1::acceptLowPressurePaiqiDelaytime(float value_float)
{
    ui->le_5_write->setText(QString::number(value_float));
    int value_int =  (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit lowPreesurePaiqiDelaytime_signals(destSting,"02");
}

void DelaytimeSetting1::acceptHighPressureBaoyaDelaytime(float value_float)
{
    ui->le_6_write->setText(QString::number(value_float));
    int value_int = (int)(value_float*10);
    QString destSting = "";
    convertToSpecialString(destSting,value_int,4,1);
    emit HighPressureBaoyaDelaytime_signals(destSting,"02");
}

void DelaytimeSetting1::shuaKa()
{
    QWidget *back = new QWidget(this);
    back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
    back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
    back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
    back->setWindowModality(Qt::WindowModal);
    back->show();
    inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,5000,0,true);
    inputdialog->setWindowModality(Qt::WindowModal);
    inputdialog->show();
    //====================TOODO 20180730 add
    inputdialog->setFullScreenShow();//TODO 20180729
    //TODO 20180730 Add need test
    connect(this,SIGNAL(closeInputDialog_ByHeadMan()),inputdialog,SIGNAL(closeFullDialog_ByHeadMan()));
    connect(this,SIGNAL(closeSenconConfirmDialog_ByQcMan()),inputdialog,SIGNAL(closeSecondConfirmDialog_ByQcMan()));
    //====================TODO 20180730 add
    connect(inputdialog,SIGNAL(inputValue(int)),this,SIGNAL(StandardSQValue(int)));
    connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
    connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
}
