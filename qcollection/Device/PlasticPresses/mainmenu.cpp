#include "mainmenu.h"
#include "ui_mainmenu.h"
#include "../../lock_dlg.h"
#include "Device/DeviceState/devicestate.h"

bool MainMenu:: _bPassed = false; //未通过
MainMenu::MainMenu(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
    ui->stackedWidget->setCurrentWidget(ui->page_default);
    initButtonSlot();

    m_process = new ProcessSelection(ui->frame_process->layout(),ui->frame_process->width(),ui->frame_process->height(),ui->frame_process);
    m_delayset1 = new DelaytimeSetting1(ui->frame_delay1->layout(),ui->frame_delay1->width(),ui->frame_delay1->height(),ui->frame_delay1);
    m_delayset2 = new DelayTimeSetting2(ui->frame_delay2->layout(),ui->frame_delay2->width(),ui->frame_delay2->height(),ui->frame_delay2);
    m_input = new InputStatus(ui->frame_input->layout(),ui->frame_input->width(),ui->frame_input->height(),ui->frame_input);
    m_output = new OutputStatus(ui->frame_output->layout(),ui->frame_output->width(),ui->frame_output->height(),ui->frame_output);

}

MainMenu::~MainMenu()
{
//    if(m_delayset1)delete m_delayset1;
//    if(m_delayset2)delete m_delayset2;
    delete ui;
}

//标志标志
void MainMenu::initButtonSlot()
{
    connect(ui->btn_process,   SIGNAL(clicked(bool)),           this,   SLOT(showProcessSelection()));
    connect(ui->btn_delay1,    SIGNAL(clicked(bool)),           this,   SLOT(showDelaySet1()));
    connect(ui->btn_delay2,    SIGNAL(clicked(bool)),           this,   SLOT(showDelaySet2()));
    connect(ui->btn_inputstatus,    SIGNAL(clicked(bool)),      this,   SLOT(showInputStatus()));
    connect(ui->btn_outputstatus,    SIGNAL(clicked(bool)),     this,   SLOT(showOutputStatus()));
}

void MainMenu::on_btn_return_clicked()
{
    emit exitCurr();
}


//目标1
void MainMenu::showProcessSelection()
{
    ui->stackedWidget->setCurrentWidget(ui->page_process);
    getCurrentSubWgtIndex();

    m_process->setFixedSize( ui->frame_process->width(), ui->frame_process->height()); //....
    connect(m_process,SIGNAL(exitCurr()),this,SLOT(receiveExitSubMenu_Slot()));
    connect(this,SIGNAL(sendProcessInfo(QString)),m_process,SLOT(receiveProcessInfoStatus(QString)));
    //低压排气 - 发
    connect(m_process,SIGNAL(lowPressStatusEnable_signals()),this,SIGNAL(receiveLowPressEnable_OfMainMenu()));
    connect(m_process,SIGNAL(lowPressStatusDisable_signals()),this,SIGNAL(receiveLowPressDisable_OfMainMenu()));

    //补压 - 发
    connect(m_process,SIGNAL(afterCompactionEnable_signals()),this,SIGNAL(receiveAftercompactionEnable_OfMainMenu()));
    connect(m_process,SIGNAL(afterCompactionDisable_signals()),this,SIGNAL(receiveAftercompactionDisable_OfMainMenu()));

    //抽芯缸 - 不发
    connect(m_process,SIGNAL(chouXinGangMode_signals(bool)),this,SIGNAL(receiveChouXinGangMode_FromMenu(bool)));
    //电机 - 不发
    connect(m_process,SIGNAL(motorWorkMode_signals(bool)),this,SIGNAL(receiveMotorMode_OfMenu(bool)));
    //高压排气 - 不发
    connect(m_process,SIGNAL(GaoYaPaiQiMode_signals(bool)),this,SIGNAL(receiveGaoYaPaiQiMode_OfMenu(bool)));
    m_process->show();
    if( _bPassed == false)
    {
        Lock_Dlg dlg("请组长刷卡确认!", this);
        DeviceState::getObj()->setDeviceStation(DeviceState::FIRST_CHECK_STATION);
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),&dlg,SLOT( reject()));
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),this,SLOT( setValue_slot()));
        dlg.exec();
    }
}

void MainMenu::showDelaySet1()
{
    ui->stackedWidget->setCurrentWidget(ui->page_delay1);
    getCurrentSubWgtIndex();
    m_delayset1->setFixedSize( ui->frame_delay1->width(), ui->frame_delay1->height() );
    connect(m_delayset1,SIGNAL(exitCurr()),this,SLOT(receiveExitSubMenu_Slot()));
    //connect(m_delayset1,SIGNAL(exitCurr()),m_delayset1,SLOT(deleteLater()));
    //数据2
    connect(this,SIGNAL(sendDelay1Info(DelayTimeSet1Para)),m_delayset1,SLOT(receiveDelay1info(DelayTimeSet1Para)));
    connect(m_delayset1,SIGNAL(lowPressureCout_signals(QString,QString)),this,SIGNAL(receiveLowPressureCount_OfMainMenu(QString,QString)));
    connect(m_delayset1,SIGNAL(highPressureCout_signals(QString,QString)),this,SIGNAL(receiveHighPressureCount_OfMainMenu(QString,QString)));
    connect(m_delayset1,SIGNAL(lowProtectDelaytime_signals(QString,QString)),this,SIGNAL(receiveLowProtectDelaytime_OfMainMenu(QString,QString)));
    connect(m_delayset1,SIGNAL(reliefPressureDelaytime_signals(QString,QString)),this,SIGNAL(receiveReliefPressureDelaytime_OfMainMenu(QString,QString)));
    connect(m_delayset1,SIGNAL(lowPreesurePaiqiDelaytime_signals(QString,QString)),this,SIGNAL(receiveLowPreesurePaiqiDelaytime_OfMainMenu(QString,QString)));
    connect(m_delayset1,SIGNAL(HighPressureBaoyaDelaytime_signals(QString,QString)),this,SIGNAL(receiveHighPreesureBaoyaDelaytime_OfMainMenu(QString,QString)));
    m_delayset1->show();


    if( _bPassed == false)
    {
        Lock_Dlg dlg("请组长刷卡确认!", this);
        DeviceState::getObj()->setDeviceStation(DeviceState::FIRST_CHECK_STATION);
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),&dlg,SLOT( reject()));
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),this,SLOT( setValue_slot()));
        dlg.exec();
    }
}

void MainMenu::showDelaySet2()
{
    ui->stackedWidget->setCurrentWidget(ui->page_delay2);
    getCurrentSubWgtIndex();
    m_delayset2->setFixedSize( ui->frame_delay2->width(), ui->frame_delay2->height());
    connect(m_delayset2,SIGNAL(exitCurr()),this,SLOT(receiveExitSubMenu_Slot()));
    //数据3
    connect(this,SIGNAL(sendDelay2Info(DelayTimeSet2Para)),m_delayset2,SLOT(receiveDelay2info(DelayTimeSet2Para)));
    connect(m_delayset2,SIGNAL(reliefPressureDelaytime2_signals(QString,QString)),this,SIGNAL(receiveReliefPressureDelaytime2_OfMainMenu2(QString,QString)));//todo
    connect(m_delayset2,SIGNAL(highPreesurePaiqiDelaytime_signals(QString,QString)),this,SIGNAL(receiveHighPressurePaiqiDelaytime_OfMainMenu2(QString,QString)));//todo
    connect(m_delayset2,SIGNAL(moldingBaoyaDelaytime_signals(QString,QString)),this,SIGNAL(receiveMoldingBaoyaDelaytime_OfMainMenu2(QString,QString)));//todo
    connect(m_delayset2,SIGNAL(topoutDelaytime_signals(QString,QString)),this,SIGNAL(receiveTopoutDelaytime_OfMainMenu2(QString,QString)));//todo
    connect(m_delayset2,SIGNAL(reliefPressureDelaytime_signals(QString,QString)),this,SIGNAL(receiveReliefPressureDelaytime_OfMainMenu2(QString,QString)));//todo
    m_delayset2->show();

    if( _bPassed == false)
    {
        Lock_Dlg dlg("请组长刷卡确认!", this);
        DeviceState::getObj()->setDeviceStation(DeviceState::FIRST_CHECK_STATION);
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),&dlg,SLOT( reject()));
        connect(this,SIGNAL(closeInputDialog_ByHeadMan()),this,SLOT( setValue_slot()));
        dlg.exec();
    }
}

void MainMenu::showInputStatus()
{
    ui->stackedWidget->setCurrentWidget(ui->page_inputstatus);
    getCurrentSubWgtIndex();
    m_input->setFixedSize( ui->frame_input->width(), ui->frame_input->height() );
    connect(m_input,SIGNAL(exitCurr()),this,SLOT(receiveExitSubMenu_Slot()));    
    connect(this,SIGNAL(sendInputStatusInfo_fromPlastic(QString)),m_input,SLOT(receiveInputStatusInfo(QString)));//todo
    m_input->show();
}

void MainMenu::showOutputStatus()
{
    ui->stackedWidget->setCurrentWidget(ui->page_outputstatus);
    getCurrentSubWgtIndex();
    //m_output = new OutputStatus(ui->frame_output->layout(),ui->frame_output->width(),ui->frame_output->height(),ui->frame_output);
    m_output->setFixedSize( ui->frame_output->width(), ui->frame_output->height());
    connect(m_output,SIGNAL(exitCurr()),this,SLOT(receiveExitSubMenu_Slot()));
    connect(this,SIGNAL(sendOutputStatusInfo(QString)),m_output,SLOT(receiveOutputStatusInfo(QString)));//todo
    m_output->show();
}

void MainMenu::receiveExitSubMenu_Slot()
{
    ui->stackedWidget->setCurrentWidget(ui->page_default);
    getCurrentSubWgtIndex();
}

void MainMenu::receiveProcessInfo(QString processInfo)
{
    emit sendProcessInfo(processInfo);
}

void MainMenu::receiveInputStatusInfo(QString inputStatusinfo)
{
    emit sendInputStatusInfo_fromPlastic(inputStatusinfo);
}

void MainMenu::receiveOutputStatusInfo(QString outputStatusinfo)
{
    emit sendOutputStatusInfo(outputStatusinfo);
}

void MainMenu::receiveDelay1Info(DelayTimeSet1Para delay1info)
{
    emit sendDelay1Info(delay1info);
}

void MainMenu::receiveDelay2Info(DelayTimeSet2Para delay2info)
{
    emit sendDelay2Info(delay2info);
}

void MainMenu::setValue_slot()
{
    _bPassed = true; //通过
}

void MainMenu::getCurrentSubWgtIndex()
{
    if(ui->stackedWidget->currentWidget() == ui->page_default)
    {
        emit sendCurrentSubWgtIndex(MAIN_MENU_WGT);
    }
    else if(ui->stackedWidget->currentWidget() == ui->page_process)
    {
        emit sendCurrentSubWgtIndex(PROCESS_SELECTION_WGT);
    }
    else if(ui->stackedWidget->currentWidget() == ui->page_inputstatus)
    {
        emit sendCurrentSubWgtIndex(INPUT_STATUS_WGT);
    }
    else if(ui->stackedWidget->currentWidget() == ui->page_outputstatus)
    {
        emit sendCurrentSubWgtIndex(OUTPUT_STATUS_WGT);
    }
    else if(ui->stackedWidget->currentWidget() == ui->page_delay1)
    {
        emit sendCurrentSubWgtIndex(DELAYTIME_SETTING1_WGT);
    }
    else if(ui->stackedWidget->currentWidget() == ui->page_delay2)
    {
        emit sendCurrentSubWgtIndex(DELAYTIME_SETTING2_WGT);
    }

}


