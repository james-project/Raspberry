#ifndef DELAYTIMESETTING1_H
#define DELAYTIMESETTING1_H

#include <QWidget>
#include "input/inputdialog.h"
#include "Device/HttpInteractiveData/httpinteractivedata.h"

namespace Ui {
class DelaytimeSetting1;
}

class DelaytimeSetting1 : public QWidget
{
    Q_OBJECT

public:
    explicit DelaytimeSetting1(QLayout *layout,int width,int height,QWidget *parent = 0);
    ~DelaytimeSetting1();
    void initEventFilter();
    bool eventFilter(QObject *watched, QEvent *event);
    void inputDialogModual(QString showString,int max,int min, QString inputMode);


    /*!
     * \brief convertToSpecialString :将整形值转换为固定长度所QSting 如 8 ，长度为4 －－－> "0008",再掉换顺序 －－－－>"0800"
     * \param destString
     * \param sourceValue
     * \param fixLength
     * \param ratio
     */
    void convertToSpecialString(QString &destString, int sourceValue, int fixLength,int ratio);

signals:
    void exitCurr();
    void lowPressureCout_signals(QString value,QString bytesCount);
    void highPressureCout_signals(QString value,QString bytesCount);
    void lowProtectDelaytime_signals(QString value,QString bytesCount);
    void reliefPressureDelaytime_signals(QString value,QString bytesCount);
    void lowPreesurePaiqiDelaytime_signals(QString value,QString bytesCount);
    void HighPressureBaoyaDelaytime_signals(QString value,QString bytesCount);

private slots:
    void on_btn_return_clicked();
    void receiveDelay1info(DelayTimeSet1Para);
    void acceptLowPressureCountValue(float);
    void acceptHighPressureCountValue(float);
    void acceptLowProtectedDelaytime(float);
    void acceptReliefPressureDelaytime(float);
    void acceptLowPressurePaiqiDelaytime(float);
    void acceptHighPressureBaoyaDelaytime(float);

private:
    Ui::DelaytimeSetting1 *ui;
    void shuaKa();
};

#endif // DELAYTIMESETTING1_H
