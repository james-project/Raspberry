#include "inputstatus.h"
#include "ui_inputstatus.h"
#include "Setting/BasicSetting/basicsettingwidget.h"
#include <QDebug>

InputStatus::InputStatus(QLayout *layout,int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputStatus)
{
    ui->setupUi(this);
    this->setFixedSize(width,height);
}

InputStatus::~InputStatus()
{
    delete ui;
}

void InputStatus::paintEvent(QPaintEvent *event)
{
    QPainter paint(this);
    paint.setPen(Qt::black);
//    int x1 = ui->btn_1->x();
//    int y1 = ui->btn_1->y()+ui->btn_1->height()/2;
//    int x2 = x1-9;
//    int y2 = y1;
//    qDebug()<<"x1 :"<<x1;
//    qDebug()<<"y1 :"<<y1;
//    qDebug()<<"x2 :"<<x2;
//    qDebug()<<"y2 :"<<y2;

//    paint.drawLine(x2,y2,x1,y1);

    paint.drawLine(0,500,50,500);
    qDebug()<<"this->width() :"<<this->x();
    qDebug()<<"this->height () :"<<this->y();

//    paint.restore();
}

void InputStatus::on_btn_return_clicked()
{
    emit exitCurr();
}

void InputStatus::receiveInputStatusInfo(QString info)
{
    qDebug() << "\n\n ~~~~InputStatus::receiveInputStatusInfo(QString): " << info<<info.at(0)<<info.at(1) ;

/*Plastic setting-----zhanglong*/
    if (BasicSettingWidget::getOprtPlastic() == BasicSettingWidget::Plastic_3)
    {
        if(info.length()>=13)
        {
            info = info.right(13);
            if((info.at(9) == '0')||(info.at(9) == ' '))
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(9) == '1')
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(8) == '0')||(info.at(8) == ' '))
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(8) == '1')
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(12) == '0')||(info.at(12) == ' '))
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(12) == '1')
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(6) == '0')||(info.at(6) == ' '))
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(6) == '1')
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(10) == '0')||(info.at(10) == ' '))
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(10) == '1')
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(7) == '0')||(info.at(7) == ' '))
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(7) == '1')
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(11) == '0')||(info.at(11) == ' '))
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(11) == '1')
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(2) == '0')||(info.at(2) == ' '))
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(2) == '1')
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(3) == '0')||(info.at(3) == ' '))
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(3) == '1')
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(1) == '0')||(info.at(1) == ' '))
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(1) == '1')
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(0) == '0')||(info.at(0) == ' '))
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(0) == '1')
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(5) == '0')||(info.at(5) == ' '))
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(5) == '1')
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(4) == '0')||(info.at(4) == ' '))
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(4) == '1')
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/red.png);");
            }
        }
    }
    else if (BasicSettingWidget::getOprtPlastic() == BasicSettingWidget::Plastic_45)
    {
        if(info.length()>=13)
        {
            info = info.right(13);
            if((info.at(12) == '0')||(info.at(12) == ' '))
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(12) == '1')
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(11) == '0')||(info.at(11) == ' '))
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(11) == '1')
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(10) == '0')||(info.at(10) == ' '))
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(10) == '1')
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(9) == '0')||(info.at(9) == ' '))
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(9) == '1')
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(11) == '0')||(info.at(11) == ' '))
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(11) == '1')
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(7) == '0')||(info.at(7) == ' '))
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(7) == '1')
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(10) == '0')||(info.at(10) == ' '))
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(10) == '1')
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(2) == '0')||(info.at(2) == ' '))
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(2) == '1')
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(3) == '0')||(info.at(3) == ' '))
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(3) == '1')
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(1) == '0')||(info.at(1) == ' '))
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(1) == '1')
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(0) == '0')||(info.at(0) == ' '))
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(0) == '1')
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(8) == '0')||(info.at(8) == ' '))
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(8) == '1')
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(7) == '0')||(info.at(7) == ' '))
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(7) == '1')
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/red.png);");
            }
        }
    }
    else if (BasicSettingWidget::getOprtPlastic() == BasicSettingWidget::Plastic_6)
    {
        if(info.length()>=13)
        {
            info = info.right(13);
            if((info.at(9) == '0')||(info.at(9) == ' '))
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(9) == '1')
            {
                ui->btn_1->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(8) == '0')||(info.at(8) == ' '))
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(8) == '1')
            {
                ui->btn_2->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(12) == '0')||(info.at(12) == ' '))
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(12) == '1')
            {
                ui->btn_3->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(6) == '0')||(info.at(6) == ' '))
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(6) == '1')
            {
                ui->btn_4->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(10) == '0')||(info.at(10) == ' '))
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(10) == '1')
            {
                ui->btn_5->setStyleSheet("border-image: url(:/res/red.png);");
            }
            if((info.at(7) == '0')||(info.at(7) == ' '))
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(7) == '1')
            {
                ui->btn_6->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(11) == '0')||(info.at(11) == ' '))
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(11) == '1')
            {
                ui->btn_7->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(2) == '0')||(info.at(2) == ' '))
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(2) == '1')
            {
                ui->btn_8->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(3) == '0')||(info.at(3) == ' '))
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(3) == '1')
            {
                ui->btn_9->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(1) == '0')||(info.at(1) == ' '))
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(1) == '1')
            {
                ui->btn_10->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(0) == '0')||(info.at(0) == ' '))
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(0) == '1')
            {
                ui->btn_11->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(5) == '0')||(info.at(5) == ' '))
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(5) == '1')
            {
                ui->btn_12->setStyleSheet("border-image: url(:/res/red.png);");
            }

            if((info.at(4) == '0')||(info.at(4) == ' '))
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/green.png);");
            }
            else if(info.at(4) == '1')
            {
                ui->btn_13->setStyleSheet("border-image: url(:/res/red.png);");
            }
        }
    }
}
