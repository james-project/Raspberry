#ifndef TEMPERATURETHREAD_H
#define TEMPERATURETHREAD_H

#include <QThread>
#include <QMutex>

class TemperatureThread : public QThread
{
    Q_OBJECT
public:
    explicit TemperatureThread(QThread *parent = 0);
    void run();
    void readTemperature();

signals:
    void sendTemp(float);
private:
    QMutex m_mutex;
};

#endif // TEMPERATURETHREAD_H
