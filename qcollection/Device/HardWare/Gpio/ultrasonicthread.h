#ifndef ULTRASONICTHREAD_H
#define ULTRASONICTHREAD_H

#include <QThread>
#include <QMutex>

class UltraSonicThread : public QThread
{
    Q_OBJECT
public:
    explicit UltraSonicThread(QThread *parent = 0);
    void run();

    void ultraInit();
    float disMeasure();
signals:
    void sendDistance(float);

public slots:
private:
    QMutex m_mutex;
};

#endif // ULTRASONICTHREAD_H
