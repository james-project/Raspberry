#ifndef ACCELARATESCALE_H
#define ACCELARATESCALE_H

#include "qextserial/qextserialport.h"
#include <QTimer>

#include <QObject>

class AccelarateScale : public QObject
{
    Q_OBJECT
public:
    explicit AccelarateScale(QString port,QString baud,DataBitsType dataBits,ParityType parity,StopBitsType stopBits,QObject *parent = 0);
    ~AccelarateScale();
    static AccelarateScale *getobj(QString port,QString baud,DataBitsType dataBits,ParityType parity,StopBitsType stopBits);
    void initComSetting(QString port,QString baud,DataBitsType dataBits,ParityType parity,StopBitsType stopBits);

signals:
    void laserDistance(QString);

public slots:
    void readScaleCom();
    void autoScaleSlots();
private:
    QextSerialPort *myCom;
    static AccelarateScale *m_instance;
    QTimer *m_scaleTimer;
};

#endif // ACCELARATESCALE_H
