#ifndef AMMETERSCALE_H
#define AMMETERSCALE_H
#include <QObject>
#include <QThread>
#include <QMutex>
#include "Modbus/ModbusRTU/modbusrtumaster.h"
#include "Modbus/ModbusRTU/serialport.h"
#include <QTimer>

class AmmeterScale : public QObject
{
    Q_OBJECT
public:

   /*add modbus flag*/
    enum {
        TXRX_NULL,
        RX_VOL_CMD,
        RX_KVA_CMD,
        RX_VOL_CMD_1,
        RX_KVA_CMD_1,
        RX_VOL_CMD_2,
        RX_KVA_CMD_2,
        RX_VOL_CMD_3,
        RX_KVA_CMD_3,
        RX_MODULE1_TEMPREATER,
        RX_MODULE2_TEMPREATER,

        TX_VOL_CMD,
        TX_KVA_CMD,
        TX_VOL_CMD_1,
        TX_KVA_CMD_1,
        TX_VOL_CMD_2,
        TX_KVA_CMD_2,
        TX_VOL_CMD_3,
        TX_KVA_CMD_3,
        TX_MODULE1_TEMPERATER,
        TX_MODULE2_TEMPERATER
    };

    struct ComPortSetting {

        QString cmbComPort;
        QString cmbBaudrate;
        QString cmbDataBits;
        QString cmbParity;
        QString cmbStopBits;
    };
    ComPortSetting mComPortSetting;
    explicit AmmeterScale(QString port,QString baud,QString dataBits,QString parity,QString stopBits,QObject *parent = 0);
    ~AmmeterScale();

    static bool check_ModbusRTU_CRC16(QByteArray buf, quint16 &result);
    static quint16 ModbusRTU_CRC16 (const char *buf, quint16 wLength);
    static quint8 LSBUS_sum(QByteArray buf);
    static QByteArray toHexString(QByteArray buf);
    void initModbusSetting(QString port,QString baud,QString dataBits,QString parity,QString stopBits);
    void createConnection();
    int onParseData(QStringList &parsedData, QByteArray data);
    void readAmmeterInfo(QString mSlaveAddr,QString mFunctionCode,QString mStartAddr,QString mNumOfRegister,QString mCommMode,QString mbyteCount);//read ammeter

    void writeCmd(QString slaveAddr, QString functionCode,QString startAddr,QString numOfRegister,QString commMode,QString byteCount,QString inputDataString,bool intputFlag);//write LED

    QByteArray makeRTUFrame(QByteArray slaveAddr, QByteArray functionCode, QByteArray startAddr,
                                       QByteArray numOfRegister, QByteArray byteCount, const QByteArray writeData);
    QByteArray makeLSBUSFrame(QByteArray slaveAddr, QByteArray functionCode, QByteArray startAddr,
                                       QByteArray numOfRegister, QByteArray byteCount, const QByteArray writeData);
signals:
    void sendAmmeterPowerData(float);
    void sendAmmeterVolData(float);
    void sendTemperature(int);
    /*Add signal----zhanglong*/
    void sendAmmeterPowerData_1(float);
    void sendAmmeterVolData_1(float);
    void sendTemperature_1(int);
    void sendAmmeterPowerData_2(float);
    void sendAmmeterVolData_2(float);
    void sendAmmeterPowerData_3(float);
    void sendAmmeterVolData_3(float);
    void sendTemperature_2(int);
    void sendModule2Temperature(int);
    void sendRequestList(QStringList);
    void sgConnected(QString str);
    void sgDisconnected(QString str);
    void toggleAutoSend(bool);

public slots:
    void onRecvedData(QByteArray data);
    void onSendedData(QByteArray data);
    void onRecvedErrorData(QByteArray data);
    void onChkAutoSendToggled(bool checked);

    quint32 onCmbBaudrateTextChanged(QString str);
    quint32 onCmbDataBitsTextChanged(QString str);
    quint32 onCmbParityTextChanged(QString str);
    quint32 onCmbStopBitsTextChanged(QString str);

    void onPortConnected();
    void onPortDisconnected();
    void onReceiveWriteList(QStringList);
    void onTimerAutoSendTimeout();
    void onReadyEntered();

private slots:
    void onTryConnectPort();


private:
    SerialPort*             m_port;
    QTimer*                 m_timerAutoSend;
    QThread*                m_threadForSerial;
    int                     m_RxQuene;
    int                     m_TxQuene;
    QMutex                  m_mutex;
};

#endif // AMMETERSCALE_H
