#ifndef GLOBALVAR_H
#define GLOBALVAR_H

#include <QObject>

class GlobalVar : public QObject
{
    Q_OBJECT
public:
    static GlobalVar *getobj();
    static int machineWorkPatten;
    inline void setUserPermission(int permission) {
        m_permission = permission;
    }

    inline int getPermission() {
        return m_permission;
    }

signals:

public slots:
    static void updateGlobalVar();
private:
    explicit GlobalVar(QObject *parent = 0);
    static GlobalVar *m_globalVar;
    int m_permission;
};

#endif // GLOBALVAR_H
