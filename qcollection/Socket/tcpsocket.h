#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QObject>
#include <QTcpSocket>

class TcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit TcpSocket(QObject *parent = 0);
    static TcpSocket *getobj();
    void writeData();

signals:
    void onConnected();
public slots:
    void receiveSocketServerData();
    void onDisconnected();
    void onconnectedFun();


private:
    QTcpSocket *m_tcpSocket;
    static TcpSocket *m_socketInstance;
};

#endif // TCPSOCKET_H
