#include "tcpsocket.h"
#include <QDebug>

TcpSocket::TcpSocket(QObject *parent) : QObject(parent)
{
    m_tcpSocket = new QTcpSocket();
    m_tcpSocket->connectToHost("192.168.168.101",1000);
//    m_tcpSocket->connectToHost("192.168.0.10",31500);
    QObject::connect(m_tcpSocket,SIGNAL(readyRead()),this,SLOT(receiveSocketServerData()));
    QObject::connect(m_tcpSocket,SIGNAL(connected()),this,SIGNAL(onConnected()));
    QObject::connect(m_tcpSocket,SIGNAL(connected()),this,SLOT(onconnectedFun()));
    QObject::connect(m_tcpSocket,SIGNAL(disconnected()),this,SLOT(onDisconnected()));
}

TcpSocket *TcpSocket::m_socketInstance = NULL;
TcpSocket *TcpSocket::getobj()
{
    if(m_socketInstance == NULL)
    {
        m_socketInstance = new TcpSocket();
    }
    return m_socketInstance;

}

void TcpSocket::writeData()
{
    m_tcpSocket->write("The data is from Raspberry of Tcp Socket");
}

void TcpSocket::receiveSocketServerData()
{
    QByteArray dataArray = m_tcpSocket->readAll();
    qDebug() << "\n\n\n\n\n+++++++++++ dataArray: " << dataArray;
    if(!dataArray.isEmpty())
        m_tcpSocket->write("111122222");
}

void TcpSocket::onDisconnected()
{
    qDebug() << "\n\n\n\n\n+++++++++++  onDisconnectedonDisconnectedonDisconnectedonDisconnected : ";
}

void TcpSocket::onconnectedFun()
{
    qDebug() << "\n\n\n\n\n+++++++++++  onconnectedFunonconnectedFun onconnectedFun : ";

}
