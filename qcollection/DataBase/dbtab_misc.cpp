#include "dbtab_misc.h"
#include "databasequery.h"
#include <QDebug>
#include "config.h"

const  QString DBTab_Misc:: TAB_MISC ="tab_misc";
const  QString DBTab_Misc:: _TAB_MISC_NUM ="misc_num";
const  QString DBTab_Misc:: _TAB_MISC_NAME ="misc_name";
const  QString DBTab_Misc:: _TAB_MISC_VALUE ="misc_value";
const  QString DBTab_Misc:: _TAB_MISC_DESCRIB = "misc_describ";

void DBTab_Misc::createTab(QSqlDatabase &db)
{   
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_MISC);
    QString str = "CREATE TABLE "   +   TAB_MISC +" (\n" +
            _ID + " integer primary key "AUTO_INCREMENT",\n" +
            _SHOP_ID + " varchar(20) DEFAULT NULL,--  shop_info.fid\n" +
            _BRANCH_ID + " varchar(20) DEFAULT NULL,--  门店branch_info.fid\n" +
            _POS_NO + " varchar(20) DEFAULT NULL,--  pos机号\n" +
            _TAB_MISC_NUM        +   " int(16) unique DEFAULT 0,--  编号\n"      +
            _TAB_MISC_NAME       +   " varchar(255) NOT NULL, --  名称\n" +
            _TAB_MISC_VALUE      +   " varchar(255) DEFAULT NULL, --  value\n" +
            _TAB_MISC_DESCRIB    +   " varchar(255) DEFAULT NULL, --  描述\n"+
            _ADD_TIME + " datetime DEFAULT NULL,--  新增时间\n" +
            _ADD_USER + " varchar(500) DEFAULT NULL,--  新增用户\n" +
            _UPDATE_TIME + " datetime DEFAULT NULL,--  更新时间\n" +
            _UPDATE_USER + " varchar(500) DEFAULT NULL,--  更新用户\n" +
            _VER                + " int(16) DEFAULT 0" +
            ")";
    query.execQString(str);
    initTab(db);
    db.commit();
    db.close();

}

const DBTab_Misc::MiscST DBTab_Misc::tabMiscInit[]=
{
    {DBTab_Misc::VERSION  ,"version1"  ,"000001"   ,""},
    {-1,"","",""}
};

void DBTab_Misc::initTab(QSqlDatabase &db)
{
    DataBaseQuery query(db);
    QString str="insert into "+TAB_MISC+"("+
            _TAB_MISC_NUM+", "       +
            _TAB_MISC_NAME+", "      +
            _TAB_MISC_VALUE+", "     +
            _TAB_MISC_DESCRIB+" "    +
            ")";
    const DBTab_Misc::MiscST *ptr;
    ptr=tabMiscInit;
    while(ptr->num!=-1)
    {
        str+=" VALUES(";
        str+=   QString::number(ptr->num)+","+
                "\""+QString(ptr->name)+"\","+
                "\""+ptr->value+"\","+
                "\""+ptr->describ+"\"";
        str+="),";
        ptr++;
    }
    str.remove(str.size()-1,1);//删除最后一个逗号
    qDebug()<<str;
    query.execQString(str);
}

QString DBTab_Misc::getValue(int num, QSqlDatabase &db)
{
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    QString str;
    str="select "+DBTab_Misc::_TAB_MISC_VALUE+ " from "+DBTab_Misc::TAB_MISC+
            " where "+DBTab_Misc::_TAB_MISC_NUM+" = "+QString::number(num);
    query.execQString(str);
    if(query.next())
    {
        QString returnString = query.record().value(0).toString();
        db.commit();
        db.close();
        return returnString;
    }
    else
    {
        db.commit();
        db.close();
        return "";
    }
}

QString DBTab_Misc::getValue(QString name, QSqlDatabase &db)
{
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    QString str;
    str="select "+DBTab_Misc::_TAB_MISC_VALUE+ " from "+DBTab_Misc::TAB_MISC+
            " where "+DBTab_Misc::_TAB_MISC_NAME+" = '"+name +"'";
    query.execQString(str);
    if(query.next())
    {
        /*return*/QString returnString =  query.record().value(0).toString();
        db.commit();
        db.close();
        return returnString;
    }
    else
    {
        db.commit();
        db.close();
        return "";
    }
}

bool DBTab_Misc::setValue(int num, QString value, QSqlDatabase &db)
{
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    QString str;
    str="select "+DBTab_Misc::_TAB_MISC_VALUE+ " from "+DBTab_Misc::TAB_MISC+
            " where "+DBTab_Misc::_TAB_MISC_NUM+" = " + QString::number(num);
    query.execQString(str);
    if(query.next())
    {
        str="update "+DBTab_Misc::TAB_MISC+
                " set "+DBTab_Misc::_TAB_MISC_VALUE+" = \""+value+"\""
                " where "+DBTab_Misc::_TAB_MISC_NUM+" = "+QString::number(num);

        if(query.exec(str))
        {
            db.commit();
            db.close();
            return true;
        }
        else
        {
            db.commit();
            db.close();
            return false;
        }
    }
    else
    {
        QMap<QString,QVariant> map;
        map.insert(DBTab_Misc::_TAB_MISC_NUM,num);
        map.insert(DBTab_Misc::_TAB_MISC_VALUE,value);
        map.insert(DBTab_Misc::_TAB_MISC_NAME,"");
        query.insert(DBTab_Misc::TAB_MISC,map);
        db.commit();
        db.close();
    }
    return true;

}

