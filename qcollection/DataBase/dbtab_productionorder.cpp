#include "dbtab_productionorder.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"
#include <QFile>
#include <QTextStream>

const  QString DBTab_ProductionOrder::TAB_ORDER = "tab_order";
const  QString DBTab_ProductionOrder::_TAB_ORDER_TASK_ID = "task_id";
const  QString DBTab_ProductionOrder::_TAB_ORDER_PRODUCT_NAME = "product_name";
const  QString DBTab_ProductionOrder::_TAB_ORDER_PRODUCT_CODE = "product_code";
const  QString DBTab_ProductionOrder::_TAB_ORDER_MTYPE3 = "mtyp3";
const  QString DBTab_ProductionOrder::_TAB_ORDER_DEVICE_TYPE = "device_type";
const  QString DBTab_ProductionOrder::_TAB_ORDER_ORDER_CODE = "order_code";
const  QString DBTab_ProductionOrder::_TAB_ORDER_WORK_NUM = "work_num";
const  QString DBTab_ProductionOrder::_TAB_ORDER_WORK_TIME = "work_time";
const  QString DBTab_ProductionOrder::_TAB_ORDER_MODULE_ID = "module_id";
const  QString DBTab_ProductionOrder::_TAB_ORDER_DEVICE_ID = "device_id";
const  QString DBTab_ProductionOrder::_TAB_ORDER_MBOM = "mbom";
const  QString DBTab_ProductionOrder::_TAB_ORDER_FINISHED_FLAG = "finish_flag";
const  QString DBTab_ProductionOrder::_TAB_ORDER_PROCESS_ID = "ProcessId";
const  QString DBTab_ProductionOrder::_TAB_ORDER_TOOLINGRATIO = "ToolingRatio";


void DBTab_ProductionOrder::createTab(QSqlDatabase &db)
{
    qDebug("createTab=User=");
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_ORDER);
    QString str = "CREATE TABLE "   +   TAB_ORDER   +               " (\n " +
            _ID + " integer primary key "AUTO_INCREMENT",               \n " +
            _TAB_ORDER_TASK_ID + " varchar(255) NOT NULL,           \n " +
            _TAB_ORDER_PRODUCT_NAME + " varchar(255) DEFAULT NULL,       \n " +
            _TAB_ORDER_PRODUCT_CODE + " varchar(255) DEFAULT NULL,       \n " +
            _TAB_ORDER_MTYPE3 + " varchar(255) DEFAULT NULL,              \n " +
            _TAB_ORDER_DEVICE_TYPE + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_ORDER_CODE + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_WORK_NUM + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_WORK_TIME + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_MODULE_ID + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_DEVICE_ID + " varchar(255) DEFAULT NULL,    \n " +
            _TAB_ORDER_MBOM + " TEXT,    \n " +            
            _TAB_ORDER_FINISHED_FLAG + " tinyint(1) DEFAULT 0,   \n " +
            _ADD_TIME + " datetime DEFAULT NULL,                  \n " +
            _TAB_ORDER_PROCESS_ID + " varchar(255) DEFAULT NULL,           \n " +
            _TAB_ORDER_TOOLINGRATIO + " varchar(255) DEFAULT NULL           \n " +
            ")";
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_ProductionOrder::initTable(QSqlDatabase &db)
{
    int version = DataBase::getDataBaseVersion(db);
    db.open();
    db.transaction();
    if(version < DATABASE_VERSION_180525)
    {

    }
    db.commit();
    db.close();
}
