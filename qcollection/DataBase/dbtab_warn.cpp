#include "dbtab_warn.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"

const  QString DBTab_Warn::TAB_WARN = "tab_warn";
const  QString DBTab_Warn::_TAB_WARN_CODE = "warn_code";
const  QString DBTab_Warn::_TAB_WARN_CONTENT = "warn_content";
void DBTab_Warn::createTab(QSqlDatabase &db)
{
    qDebug("createTab=Warn=");
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_WARN);
    QString str = "CREATE TABLE "   +   TAB_WARN +" (\n " +
            _ID + " integer primary key "AUTO_INCREMENT",\n " +
            _TAB_WARN_CODE + " varchar(255) DEFAULT NULL,       --  报警代码\n " +
            _TAB_WARN_CONTENT + " varchar(255) DEFAULT NULL,    --  报警内容\n " +
            _ADD_TIME + " datetime DEFAULT NULL                  --  添加时间\n " +
            ")";
    qDebug()<<"str==="<<str;
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_Warn::initTable(QSqlDatabase &db)
{

    Q_UNUSED(db);
}
