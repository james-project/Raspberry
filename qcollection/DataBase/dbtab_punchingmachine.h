#ifndef DBTAB_PUNCHINGMACHINE_H
#define DBTAB_PUNCHINGMACHINE_H

/*
 * 冲压机的表和字段
 */
#include "dbtabbase.h"

class DBTab_PunchingMachine: public DBTabBase
{
public:
public:
    const static QString TAB_PUNCHINGMACHINE;

    //冲压机设备名
    const static QString _TAB_PUNCHINGMACHINE_NAME;//冲压机设备名


    // Scale
    const static QString _TAB_PUNCHINGMACHINE_SCALE_WEIGHT;//Scale weight
    const static QString _TAB_PUNCHINGMACHINE_SCALE_COUNT;//Scale count
    const static QString _TAB_PUNCHINGMACHINE_QUALIFIED_RATE;//Qualified rate
    const static QString _TAB_PUNCHINGMACHINE_TASK_ID;//Task Id

    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);


};

#endif // DBTAB_PUNCHINGMACHINE_H
