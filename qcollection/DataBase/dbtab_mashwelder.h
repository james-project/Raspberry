#ifndef DBTAB_MASHWELDER_H
#define DBTAB_MASHWELDER_H

/*
 * 点焊机的表和字段
 */
#include "dbtabbase.h"

class DBTab_MashWelder : public DBTabBase
{
public:
    const static QString TAB_MASHWELDER;

    //点焊机设备名
    const static QString _TAB_MASHWELDER_NAME;//点焊机设备名

    //规范号参数
    const static QString _TAB_MASHWELDER_CODE_NUMBER;//焊接规范号（工艺参数）

    //当前焊接参数
    const static QString _TAB_MASHWELDER_SQ;//预压时间
    const static QString _TAB_MASHWELDER_SO;//加压时间
    const static QString _TAB_MASHWELDER_S1;//缓升时间
    const static QString _TAB_MASHWELDER_W1;//预热时间
    const static QString _TAB_MASHWELDER_CO;//冷却时间
    const static QString _TAB_MASHWELDER_W2;//焊接时间
    const static QString _TAB_MASHWELDER_HO;//维持时间
    const static QString _TAB_MASHWELDER_OF;//停止时间

    //当前焊接电压/电流参数
    const static QString _TAB_MASHWELDER_CURRENT_W1;//W1焊接电流设置值
    const static QString _TAB_MASHWELDER_CURRENT_W2;//W2焊接电流设置值

    //系统参数
    const static QString _TAB_MASHWELDER_STARTUP_MODE;//启动方式
    const static QString _TAB_MASHWELDER_CONST_CONTROL;//恒流恒压控制方式
    const static QString _TAB_MASHWELDER_SAMPLE;//初/次级采样
    const static QString _TAB_MASHWELDER_CURRENT_UPPER;//电流上限报警设置
    const static QString _TAB_MASHWELDER_CURRENT_LOWER;//电流下限报警设置
    const static QString _TAB_MASHWELDER_HEAT_PROTECTION;//热保护开关方式
    const static QString _TAB_MASHWELDER_END_OUTPUT;//结束输出
    const static QString _TAB_MASHWELDER_CONST_MODE;//恒流/恒热量方式
    const static QString _TAB_MASHWELDER_FILTER_ORDER;//滤波阶数
    const static QString _TAB_MASHWELDER_FILTER_RANGE;//滤波范围
    const static QString _TAB_MASHWELDER_ERROR_MODE;//错误提示方式
    const static QString _TAB_MASHWELDER_CODE_SELECTION;//规范选择
    const static QString _TAB_MASHWELDER_END_SIGNAL_TIME;//结束信号时长
    const static QString _TAB_MASHWELDER_MINI_PASS_ANGLE;//最小导通角

    //焊接状态
    const static QString _TAB_MASHWELDER_W1_FEEDBACK_CURRENT;//W1焊接电流反馈值
    const static QString _TAB_MASHWELDER_W2_FEEDBACK_CURRENT;//W2焊接电流反馈值
    const static QString _TAB_MASHWELDER_WELDING_COUNTER;//焊接计数器
    const static QString _TAB_MASHWELDER_WEIDING_PRENUMBER;//焊接预制数
    const static QString _TAB_MASHWELDER_THYRISTOR_PASS_TIME;//可控硅导通时间
    const static QString _TAB_MASHWELDER_WELDING_POINT_NUMBER;//焊点序号

    //按钮开关
    const static QString _TAB_MASHWELDER_WELDING_START_SWITCH;//焊接启动开关
    const static QString _TAB_MASHWELDER_PREPRESSURE_VALVE_SWITCH;//预压阀启动开关
    const static QString _TAB_MASHWELDER_WELDING_ENABLE_SWITCH;//焊接使能开关
    const static QString _TAB_MASHWELDER_COUNTER_ZERO_SWITCH;//计数器清零开关
    const static QString _TAB_MASHWELDER_CLEAR_ALARM_SWITCH;//清除报警开关

    //报警信息
    const static QString _TAB_MASHWELDER_THYRISITOR_SHORT_CIRCUIT;//可控硅直通
    const static QString _TAB_MASHWELDER_THYRISITOR_CIRCUIT_ERROR;//可控硅不导通
    const static QString _TAB_MASHWELDER_OVER_HEATED;//过热
    const static QString _TAB_MASHWELDER_OVER_CURRENT;//过电流
    const static QString _TAB_MASHWELDER_UNDER_CURRENT;//欠电流
    const static QString _TAB_MASHWELDER_TRANSFORMER_RANGE_OVER;//互感器量程过大
    const static QString _TAB_MASHWELDER_MEMORY_DATA_ERROR;//存储数据出错
    const static QString _TAB_MASHWELDER_NO_SYNCH_SIGNAL;//没有同步信号
    const static QString _TAB_MASHWELDER_PASSTIME_LONG;//导通时间超过10ms

    // Scale
    const static QString _TAB_MASHWELDER_SCALE_WEIGHT;//Scale weight
    const static QString _TAB_MASHWELDER_SCALE_COUNT;//Scale count
    const static QString _TAB_MASHWELDER_QUALIFIED_RATE;//Qualified rate
    const static QString _TAB_MASHWELDER_TASK_ID;//Task Id

    //Electrode    
    const static QString _TAB_MASHWELDER_ELECTRODE_PRE_COUNT;//Electrode pre count
    const static QString _TAB_MASHWELDER_ELECTRODE_CURRENT_COUNT;//Electrode current count
    const static QString _TAB_MASHWELDER_ELECTRODE_TOTAL_COUNT;//Electrode Total count    
    /*!
     * \brief _TAB_MASHWELDER_ELECTRODE_SAVE_FLAG
     * 统计标志，为0时不统计，当累计的次数到达预制数时将此标志置1；
     */
    const static QString _TAB_MASHWELDER_ELECTRODE_SAVE_FLAG;//Electrode save flag --(init value is 0 )











    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);
};

#endif // DBTAB_MASHWELDER_H
