#include "dbtab_punchingmachine.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"

const  QString DBTab_PunchingMachine:: TAB_PUNCHINGMACHINE ="tab_punchingmachine";   //tab name
const  QString DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_NAME = "punchingmachine_name";//设备名


//Scale
const  QString DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_SCALE_WEIGHT = "punchingmachine_scale_weight";//Scale weight
const  QString DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_SCALE_COUNT = "punchingmachine_sacle_count";//Scale count
const  QString DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_QUALIFIED_RATE = "punchingmachine_qualified_rate";//Qualified rate
const  QString DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_TASK_ID = "punchingmachine_task_id";//Task Id


void DBTab_PunchingMachine::createTab(QSqlDatabase &db)
{
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_PUNCHINGMACHINE);
    QString str = "CREATE TABLE "   +   TAB_PUNCHINGMACHINE +" (\n" +
            _ID + " integer primary key "AUTO_INCREMENT",\n" +
            _TAB_PUNCHINGMACHINE_NAME                        + " varchar(255) DEFAULT NULL, -- chongyaji设备名\n" +

            _TAB_PUNCHINGMACHINE_SCALE_WEIGHT                + " double DEFAULT 0.0,  --  Scale weight\n" +
            _TAB_PUNCHINGMACHINE_SCALE_COUNT                 + " int(16) DEFAULT 0,  --  Scale count\n" +
            _TAB_PUNCHINGMACHINE_QUALIFIED_RATE              + " double DEFAULT 0.0,  --  qulified rate\n" +
            _TAB_PUNCHINGMACHINE_TASK_ID                     + " varchar(255) DEFAULT NULL,  --  task id\n" +
            _ADD_TIME                                        + " datetime DEFAULT NULL,                  \n " +
            ")";
    qDebug()<<"str==="<<str;
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_PunchingMachine::initTable(QSqlDatabase &db)
{
    int version = DataBase::getDataBaseVersion(db);
    db.open();
    db.transaction();
    if(version < DATABASE_VERSION_180730)
    {
        DataBaseQuery query(db);
        query.addColumn(DBTab_PunchingMachine::TAB_PUNCHINGMACHINE,DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_SCALE_WEIGHT," double DEFAULT 0.0 ");
        query.addColumn(DBTab_PunchingMachine::TAB_PUNCHINGMACHINE,DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_SCALE_COUNT," int(16) DEFAULT 0 ");
        query.addColumn(DBTab_PunchingMachine::TAB_PUNCHINGMACHINE,DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_QUALIFIED_RATE," double DEFAULT 0.0 ");
        query.addColumn(DBTab_PunchingMachine::TAB_PUNCHINGMACHINE,DBTab_PunchingMachine::_TAB_PUNCHINGMACHINE_TASK_ID," varchar(255) DEFAULT NULL ");
        query.addColumn(DBTab_PunchingMachine::TAB_PUNCHINGMACHINE,_ADD_TIME," datetime DEFAULT NULL ");
    }

    db.commit();
    db.close();
}
