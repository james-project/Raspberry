#ifndef DBTABBASE_H
#define DBTABBASE_H
#include <QString>
#include <QSqlDatabase>
#include "config.h"

#define DATABASE_VERSION_180123 180123
#define DATABASE_VERSION_180525 180525
#define DATABASE_VERSION_180730 180730
#define DATABASE_VERSION_180804 180804
#define DATABASE_VERSION_NOW DATABASE_VERSION_180804

class DBTabBase
{
public:
    static const QString _ID;
    static const QString _SHOP_ID;
    static const QString _BRANCH_ID;
    static const QString _POS_NO;
    static const QString _ADD_TIME;
    static const QString _ADD_USER;
    static const QString _UPDATE_TIME;
    static const QString _UPDATE_USER;
    static const QString _VER;
    static const QString _INDEX_SHOP_ID;
    static const QString _INDEX_BRANCH_ID;
    static const QString _INDEX_POS_NO;
    static qint64 getMaxVersion(const QString &db_table);
};

#endif // DBTABBASE_H
