#include "dbtab_plasticpresses.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"

const  QString DBTab_PlasticPresses:: TAB_PLASTICPRESSES ="tab_plasticpresses";   //tab name
const  QString DBTab_PlasticPresses::_TAB_PLASTICPRESSES_NAME = "plasticpresses_name";  //设备名
const  QString DBTab_PlasticPresses::_TAB_PLASTICPRESSES_TASK_ID = "plasticpresses_task_id";  //Task Id


void DBTab_PlasticPresses::createTab(QSqlDatabase &db)
{
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_PLASTICPRESSES);
    QString str = "CREATE TABLE "   +   TAB_PLASTICPRESSES +" (\n" +
            _ID + " integer primary key "AUTO_INCREMENT",\n" +
            _TAB_PLASTICPRESSES_NAME                        + " varchar(255) DEFAULT NULL, -- chongyaji设备名\n" +
            _TAB_PLASTICPRESSES_TASK_ID                     + " varchar(255) DEFAULT NULL,  --  task id\n" +
            _ADD_TIME                                        + " datetime DEFAULT NULL,                  \n " +
            ")";
    qDebug()<<"str==="<<str;
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_PlasticPresses::initTable(QSqlDatabase &db)
{
    int version = DataBase::getDataBaseVersion(db);
    db.open();
    db.transaction();
    if(version < DATABASE_VERSION_180730)
    {
        DataBaseQuery query(db);
        query.addColumn(DBTab_PlasticPresses::TAB_PLASTICPRESSES,DBTab_PlasticPresses::_TAB_PLASTICPRESSES_TASK_ID," varchar(255) DEFAULT NULL ");
        query.addColumn(DBTab_PlasticPresses::TAB_PLASTICPRESSES,_ADD_TIME," datetime DEFAULT NULL ");
    }

    db.commit();
    db.close();
}
