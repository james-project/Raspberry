#include "dbtab_user.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"

const  QString DBTab_User::TAB_USER             = "tab_user";
const  QString DBTab_User::_TAB_USER_CODE       = "user_code";
const  QString DBTab_User::_TAB_USER_NAME       = "user_name";
const  QString DBTab_User::_TAB_USER_PASSWORD   = "user_password";
const  QString DBTab_User::_TAB_USER_PERMISSION = "user_permission";

void DBTab_User::createTab(QSqlDatabase &db)
{
    qDebug("createTab=User=");
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_USER);
    QString str = "CREATE TABLE "   +   TAB_USER +" (\n " +
            _ID + " integer primary key "AUTO_INCREMENT",\n " +
            _TAB_USER_CODE + " int(20) DEFAULT 0,               --  用户code\n " +
            _TAB_USER_NAME + " varchar(20) DEFAULT NULL,        --  用户名\n " +
            _TAB_USER_PASSWORD + " varchar(20) DEFAULT NULL,    --  密码\n " +
            _TAB_USER_PERMISSION + " int(6) DEFAULT 0           --  权限\n " +
            ")";
    qDebug()<<"str==="<<str;
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_User::initTable(QSqlDatabase &db)
{
        int version=DataBase::getDataBaseVersion(db);
        db.open();
        db.transaction();
        if(version<DATABASE_VERSION_180123)
        {
            DataBaseQuery query(db);
            {
                query.execQString("DELETE FROM tab_product WHERE _id=1");
                QMap<QString,QVariant> map_Save;
                map_Save.insert(DBTab_User::_ID,QVariant(1));
                map_Save.insert(DBTab_User::_TAB_USER_CODE,QVariant(123456));
                map_Save.insert(DBTab_User::_TAB_USER_NAME,QVariant("管理员1"));
                map_Save.insert(DBTab_User::_TAB_USER_PASSWORD,QVariant("999999"));
                map_Save.insert(DBTab_User::_TAB_USER_PERMISSION,QVariant(1));
                query.insert(DBTab_User::TAB_USER,map_Save);
            }
            {
                query.execQString("DELETE FROM tab_product WHERE _id=2");
                QMap<QString,QVariant> map_Save;
                map_Save.insert(DBTab_User::_ID,QVariant(2));
                map_Save.insert(DBTab_User::_TAB_USER_CODE,QVariant(123456));
                map_Save.insert(DBTab_User::_TAB_USER_NAME,QVariant("操作员1"));
                map_Save.insert(DBTab_User::_TAB_USER_PASSWORD,QVariant("123456"));
                map_Save.insert(DBTab_User::_TAB_USER_PERMISSION,QVariant(0));
                query.insert(DBTab_User::TAB_USER,map_Save);
            }
        }
        db.commit();
        db.close();


}
