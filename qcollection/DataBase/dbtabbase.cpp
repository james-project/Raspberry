#include "dbtabbase.h"
#include "databasequery.h"

const QString DBTabBase::_ID = "_id";
const QString DBTabBase::_SHOP_ID = "shop_id";
const QString DBTabBase::_BRANCH_ID = "branch_id";
const QString DBTabBase::_POS_NO = "pos_no";
const QString DBTabBase::_ADD_TIME = "add_time";
const QString DBTabBase::_ADD_USER = "add_user";
const QString DBTabBase::_UPDATE_TIME = "update_time";
const QString DBTabBase::_UPDATE_USER = "update_user";
const QString DBTabBase::_VER = "ver";
const QString DBTabBase::_INDEX_SHOP_ID = "index_shop_id";
const QString DBTabBase::_INDEX_BRANCH_ID = "index_branch_id";
const QString DBTabBase::_INDEX_POS_NO = "index_pos_no";


qint64 DBTabBase::getMaxVersion(const QString &db_table)
{
    DataBaseQuery query;
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();
    int version;
    QString str = "select ifnull(max(" + DBTabBase::_VER +"),-1) from " + db_table;
    query.execQString(str);
    if(query.first()){
        version =  query.value(0).toLongLong();
    }else{

        version= -1;

    }
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

    return version;
}
