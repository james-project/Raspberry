#ifndef DATABASE_H
#define DATABASE_H
#include <QObject>
#include "config.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlIndex>
#include <QDate>
#include <QStringList>

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0,const QString &connect_name="normal");
    void createAllTabs(QSqlDatabase &db);
    QSqlDatabase dataBase();
    static QSqlDatabase& getDataBase();
    static int getDataBaseVersion(QSqlDatabase &db);
    static void setDataBaseVersion(int version,QSqlDatabase &db);
private:
    static DataBase *mDbInstance;
    QSqlDatabase mDb;
    QSqlDatabase mDb_misc;
};

#endif // DATABASE_H
