#ifndef DBTab_Misc_H
#define DBTab_Misc_H
#include "dbtabbase.h"

class DBTab_Misc : public DBTabBase
{
public:
    //-----------Tab Misc
    const static QString TAB_MISC ;

    const static QString _TAB_MISC_NUM;
    const static QString _TAB_MISC_NAME;
    const static QString _TAB_MISC_VALUE;
    const static QString _TAB_MISC_DESCRIB;
    static void createTab(QSqlDatabase &db);
    static void initTab(QSqlDatabase &db);
    enum
    {
        VERSION=1,
        STAND_SALE_UI_ROW_COLUMN,
        ACC_SALE_UI_ROW_COLUMN,
        PREPACK_SALE_UI_ROW_COLUMN,
        SELF_SALE_UI_ROW_COLUMN,
        MARK_NUM_SHOW,
        PLU_SHOW_STYLE,
        POS_SALE_UI_ROW_COLUMN,
    };
    struct MiscST
    {
        int num;
        QString name;
        QString value;
        QString describ;
    };
    static const DBTab_Misc::MiscST tabMiscInit[];
    static QString getValue(int num,QSqlDatabase &db);
    static QString getValue(QString name,QSqlDatabase &db);
    static bool setValue(int num,QString value,QSqlDatabase &db );       
};

#endif // DBTab_Misc_H
