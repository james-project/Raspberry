#ifndef DBTAB_USER_H
#define DBTAB_USER_H

#include "dbtabbase.h"

class DBTab_User : public DBTabBase
{
public:
    const static QString TAB_USER;
    const static QString _TAB_USER_CODE;
    const static QString _TAB_USER_NAME;
    const static QString _TAB_USER_PASSWORD;
    const static QString _TAB_USER_PERMISSION;

    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);

};

#endif // DBTAB_USER_H
