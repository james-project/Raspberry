
#ifndef DBTAB_PRODUCTIONORDER_H
#define DBTAB_PRODUCTIONORDER_H

#include "dbtabbase.h"

class DBTab_ProductionOrder : public DBTabBase
{
public:
    const static QString TAB_ORDER;
    const static QString _TAB_ORDER_TASK_ID;
    const static QString _TAB_ORDER_PRODUCT_NAME;
    const static QString _TAB_ORDER_PRODUCT_CODE;
    const static QString _TAB_ORDER_MTYPE3;
    const static QString _TAB_ORDER_DEVICE_TYPE;
    const static QString _TAB_ORDER_ORDER_CODE;
    const static QString _TAB_ORDER_WORK_NUM;
    const static QString _TAB_ORDER_WORK_TIME;
    const static QString _TAB_ORDER_MODULE_ID;
    const static QString _TAB_ORDER_DEVICE_ID;
    const static QString _TAB_ORDER_MBOM;
    const static QString _TAB_ORDER_FINISHED_FLAG;
    const static QString _TAB_ORDER_PROCESS_ID;
    const static QString _TAB_ORDER_TOOLINGRATIO;


    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);

};

#endif // DBTAB_PRODUCTIONORDER_H
