#ifndef DATABASEQUERY_H
#define DATABASEQUERY_H
#include <QSqlQuery>
#include <QVariant>
#include <QStringList>
#include "database.h"

class DataBaseQuery : public QSqlQuery
{
public:
    explicit DataBaseQuery(QSqlDatabase db=DataBase::getDataBase());
    bool createIndex(const QString &tabName, const QString &fieldName, const QString &indexName="");
    static QString indexName(const QString &name);
    bool insert(QString tabName,const QMap<QString,QVariant> &dataMap);
    bool insertOrUpdate(const QString &tabName,const QString &keyName,QMap<QString,QVariant> &dataMap);
    bool selectTable(QStringList *tabList,const QStringList *keyList=NULL, QString option="");
    bool selectTable(QString tabName,const QStringList *keyList=NULL, QString option="",QString shortItem="",bool shortdesc=0,QString limit="");
    bool selectTableJoin(QString tabName,const QStringList *keyList=NULL,const QStringList *joinList=NULL, QString option="",QString shortItem="",bool shortdesc=0,QString limit="");
    bool selectTableJoin(QString tabName,const QStringList *keyList=NULL,const QStringList *joinList=NULL, QString option="",QString shortItem="",bool isAlphabet=1,bool shortdesc=0,QString limit="");
    bool deleteItem(QString tabName,QString option="");
    bool deleteItem(const QString &tabName,const QString &fieldName,const QString &da);
    bool updateItem(QString tabName,QMap<QString,QVariant> &dataMap,QString option="");
    bool updateItem(QString tabName,QString fieldName,QString now,const QMap<QString,QVariant> &dataMap_new);
    bool errCrashRepairHandle(QString,QString,QString retry=""); //retry为出错前执行的sql语句//暂未确定sql语句是否执行，当前retry参数未用 //20161216
    bool dropDataBaseTableData(QString table);
    bool deleteDataBaseTableData(QString table);
    bool resetDataBaseTableData(QString table);
    bool getMax(QString table,QString fieldName);
    int size(void);
    bool execQString(const QString &str);
    bool execQString_test(const QString &str);
    bool isItemExist(const QString &tabName,const QString &item,const QVariant &content);
    bool removeTable(const QString &table);
    static bool deleteDataBaseTable(QString database,QString table);
    bool addColumn(QString tabName, QString column, QString option);
    static bool checkIfHaveRecord(QString tableName,QString column,QString data);
    bool unionSelectTabs(const QStringList *keyList,QStringList *tabList,QString option="",QString shortItem="",bool shortdesc=false); //MARK20170905
private:
    QSqlDatabase *mDb;
};

#endif // DATABASEQUERY_H
