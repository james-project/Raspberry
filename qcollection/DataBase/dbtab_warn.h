#ifndef DBTAB_WARN_H
#define DBTAB_WARN_H

#include "DataBase/dbtabbase.h"

class DBTab_Warn : public DBTabBase
{
public:
    const static QString TAB_WARN;
    const static QString _TAB_WARN_CODE;
    const static QString _TAB_WARN_CONTENT;

    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);
};

#endif // DBTAB_WARN_H
