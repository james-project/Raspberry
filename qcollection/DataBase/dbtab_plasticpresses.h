#ifndef DBTAB_PLASTICPRESSES_H
#define DBTAB_PLASTICPRESSES_H
#include "dbtabbase.h"


class DBTab_PlasticPresses : public DBTabBase
{
public:
    const static QString TAB_PLASTICPRESSES;
    const static QString _TAB_PLASTICPRESSES_NAME;  //压塑机设备名
    const static QString _TAB_PLASTICPRESSES_TASK_ID;  //Task Id
    static void createTab(QSqlDatabase &db);
    static void initTable(QSqlDatabase &db);
};

#endif // DBTAB_PLASTICPRESSES_H
