#include "dbtab_mashwelder.h"
#include "DataBase/databasequery.h"
#include "tdebug.h"

const  QString DBTab_MashWelder:: TAB_MASHWELDER ="tab_mashwelder";   //tab name

const  QString DBTab_MashWelder::_TAB_MASHWELDER_NAME = "mashwelder_name";//点焊机设备名
//规范号参数
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CODE_NUMBER = "mashwelder_code_num";//焊接规范号（工艺参数）

//当前焊接参数
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_SQ = "mashwelder_sq";//预压时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_SO = "mashwelder_so";//加压时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_S1 = "mashwelder_s1";//缓升时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_W1 = "mashwelder_w1";//预热时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_CO = "mashwelder_co";//冷却时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_W2 = "mashwelder_w2";//焊接时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_HO = "mashwelder_ho";//维持时间
const  QString DBTab_MashWelder:: _TAB_MASHWELDER_OF = "mashwelder_of";//停止时间

//当前焊接电压/电流参数
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CURRENT_W1 = "mashwelder_current_w1";//W1焊接电流设置值
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CURRENT_W2 = "mashwelder_current_w2";//W2焊接电流设置值

//系统参数
const  QString DBTab_MashWelder::_TAB_MASHWELDER_STARTUP_MODE = "mashwelder_startup_mode";//启动方式
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CONST_CONTROL = "mashwelder_const_control";//恒流恒压控制方式
const  QString DBTab_MashWelder::_TAB_MASHWELDER_SAMPLE = "mashwelder_sample";//初/次级采样
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CURRENT_UPPER = "mashwelder_current_upper";//电流上限报警设置
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CURRENT_LOWER = "mashwelder_current_lower";//电流下限报警设置
const  QString DBTab_MashWelder::_TAB_MASHWELDER_HEAT_PROTECTION = "mashwelder_heat_protection";//热保护开关方式
const  QString DBTab_MashWelder::_TAB_MASHWELDER_END_OUTPUT = "mashwelder_output";//结束输出
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CONST_MODE = "mashwelder_const_mode";//恒流/恒热量方式
const  QString DBTab_MashWelder::_TAB_MASHWELDER_FILTER_ORDER = "mashwelder_filter_order";//滤波阶数
const  QString DBTab_MashWelder::_TAB_MASHWELDER_FILTER_RANGE = "mashwelder_filter_range";//滤波范围
const  QString DBTab_MashWelder::_TAB_MASHWELDER_ERROR_MODE = "mashwelder_error_mode";//错误提示方式
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CODE_SELECTION = "mashwelder_code_selection";//规范选择
const  QString DBTab_MashWelder::_TAB_MASHWELDER_END_SIGNAL_TIME = "mashwelder_end_signal_time";//结束信号时长
const  QString DBTab_MashWelder::_TAB_MASHWELDER_MINI_PASS_ANGLE = "mashwelder_mini_pass_angle";//最小导通角

//焊接状态
const  QString DBTab_MashWelder::_TAB_MASHWELDER_W1_FEEDBACK_CURRENT = "mashwelder_w1_feedback_current";//W1焊接电流反馈值
const  QString DBTab_MashWelder::_TAB_MASHWELDER_W2_FEEDBACK_CURRENT = "mashwelder_w2_feedback_current";//W2焊接电流反馈值
const  QString DBTab_MashWelder::_TAB_MASHWELDER_WELDING_COUNTER = "mashwelder_welding_counter";//焊接计数器
const  QString DBTab_MashWelder::_TAB_MASHWELDER_WEIDING_PRENUMBER = "mashwelder_welding_prenumber";//焊接预制数
const  QString DBTab_MashWelder::_TAB_MASHWELDER_THYRISTOR_PASS_TIME = "mashwelder_thyrisitor_passtime";//可控硅导通时间
const  QString DBTab_MashWelder::_TAB_MASHWELDER_WELDING_POINT_NUMBER = "mashwelder_welding_pointnum";//焊点序号

//按钮开关
const  QString DBTab_MashWelder::_TAB_MASHWELDER_WELDING_START_SWITCH = "mashwelder_welding_start_switch";//焊接启动开关
const  QString DBTab_MashWelder::_TAB_MASHWELDER_PREPRESSURE_VALVE_SWITCH = "mashwelder_prepressure_value_switch";//预压阀启动开关
const  QString DBTab_MashWelder::_TAB_MASHWELDER_WELDING_ENABLE_SWITCH = "mashwelder_welding_enable_switch";//焊接使能开关
const  QString DBTab_MashWelder::_TAB_MASHWELDER_COUNTER_ZERO_SWITCH = "mashwelder_counter_zero_switch";//计数器清零开关
const  QString DBTab_MashWelder::_TAB_MASHWELDER_CLEAR_ALARM_SWITCH = "mashwelder_clear_alarm_switch";//清除报警开关

//报警信息
const  QString DBTab_MashWelder::_TAB_MASHWELDER_THYRISITOR_SHORT_CIRCUIT = "mashwelder_thyrisitor_short_circuit";//可控硅直通
const  QString DBTab_MashWelder::_TAB_MASHWELDER_THYRISITOR_CIRCUIT_ERROR = "mashwelder_thyrisitor_circuit_error";//可控硅不导通
const  QString DBTab_MashWelder::_TAB_MASHWELDER_OVER_HEATED = "mashwelder_over_heated";//过热
const  QString DBTab_MashWelder::_TAB_MASHWELDER_OVER_CURRENT = "mashwelder_over_current";//过电流
const  QString DBTab_MashWelder::_TAB_MASHWELDER_UNDER_CURRENT = "mashwelder_under_current";//欠电流
const  QString DBTab_MashWelder::_TAB_MASHWELDER_TRANSFORMER_RANGE_OVER = "mashwelder_transform_range_over";//互感器量程过大
const  QString DBTab_MashWelder::_TAB_MASHWELDER_MEMORY_DATA_ERROR = "mashwelder_memory_data_error";//存储数据出错
const  QString DBTab_MashWelder::_TAB_MASHWELDER_NO_SYNCH_SIGNAL = "mashwelder_no_synch_signal";//没有同步信号
const  QString DBTab_MashWelder::_TAB_MASHWELDER_PASSTIME_LONG = "mashwelder_passtime_long";//导通时间超过10ms

//Scale
const  QString DBTab_MashWelder::_TAB_MASHWELDER_SCALE_WEIGHT = "mashwelder_scale_weight";//Scale weight
const  QString DBTab_MashWelder::_TAB_MASHWELDER_SCALE_COUNT = "mashwelder_sacle_count";//Scale count
const  QString DBTab_MashWelder::_TAB_MASHWELDER_QUALIFIED_RATE = "mashwelder_qualified_rate";//Qualified rate
const  QString DBTab_MashWelder::_TAB_MASHWELDER_TASK_ID = "mashwelder_task_id";//Task Id

//Electrode
const  QString DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_PRE_COUNT = "mashwelder_electrode_pre_count";//Electrode pre count
const  QString DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_CURRENT_COUNT = "mashwelder_electrode_curr_count";//Electrode current count
const  QString DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_TOTAL_COUNT = "mashwelder_electrode_total_count";//Electrode Total count
const  QString DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_SAVE_FLAG = "mashwelder_electrode_save_flag";//Electrode save flag --(init value is 0 )

void DBTab_MashWelder::createTab(QSqlDatabase &db)
{
    qDebug("createTab=TAB_MASHWELDER=");
    DataBaseQuery query(db);
    db.open();
    db.transaction();
    query.exec("drop table " + TAB_MASHWELDER);
    QString str = "CREATE TABLE "   +   TAB_MASHWELDER +" (\n" +
            _ID + " integer primary key "AUTO_INCREMENT",\n" +
            _TAB_MASHWELDER_NAME                        + " varchar(255) DEFAULT NULL, -- 点焊机设备名\n" +
            _TAB_MASHWELDER_SQ                          + " int(6) DEFAULT 0, --  预压时间\n" +
            _TAB_MASHWELDER_SO                          + " int(6) DEFAULT 0, --  加压时间\n" +
            _TAB_MASHWELDER_S1                          + " int(6) DEFAULT 0, --  缓升时间\n" +
            _TAB_MASHWELDER_W1                          + " int(6) DEFAULT 0, --  预热时间\n" +
            _TAB_MASHWELDER_CO                          + " int(6) DEFAULT 0, --  冷却时间\n" +
            _TAB_MASHWELDER_W2                          + " int(6) DEFAULT 0, --  焊接时间\n" +
            _TAB_MASHWELDER_HO                          + " int(6) DEFAULT 0, --  维持时间\n" +
            _TAB_MASHWELDER_OF                          + " int(6) DEFAULT 0, --  停止时间\n" +

            _TAB_MASHWELDER_CURRENT_W1                  + " int(6) DEFAULT 0, --  W1焊接电流设置值\n" +
            _TAB_MASHWELDER_CURRENT_W2                  + " int(6) DEFAULT 0, --  W2焊接电流设置值\n" +
            _TAB_MASHWELDER_STARTUP_MODE                + " int(6) DEFAULT 0, --  启动方式\n" +
            _TAB_MASHWELDER_CONST_CONTROL               + " int(6) DEFAULT 0, --  恒流恒压控制方式\n" +
            _TAB_MASHWELDER_SAMPLE                      + " int(6) DEFAULT 0, --  次级采样\n" +
            _TAB_MASHWELDER_CURRENT_UPPER               + " int(6) DEFAULT 0, --  电流上限报警设置\n" +
            _TAB_MASHWELDER_CURRENT_LOWER               + " int(6) DEFAULT 0, --  电流下限报警设置\n" +
            _TAB_MASHWELDER_HEAT_PROTECTION             + " int(6) DEFAULT 0, --  热保护开关方式\n" +
            _TAB_MASHWELDER_END_OUTPUT                  + " int(6) DEFAULT 0, --  结束输出\n" +
            _TAB_MASHWELDER_CONST_MODE                  + " int(6) DEFAULT 0, --  恒流/恒热量方式\n" +
            _TAB_MASHWELDER_FILTER_ORDER                + " int(6) DEFAULT 0, --  滤波阶数\n" +
            _TAB_MASHWELDER_FILTER_RANGE                + " int(6) DEFAULT 0, --  滤波范围\n" +
            _TAB_MASHWELDER_ERROR_MODE                  + " int(6) DEFAULT 0, --  错误提示方式\n" +
            _TAB_MASHWELDER_CODE_SELECTION              + " int(6) DEFAULT 0, --  规范选择\n" +
            _TAB_MASHWELDER_END_SIGNAL_TIME             + " int(6) DEFAULT 0, --  结束信号时长\n" +
            _TAB_MASHWELDER_MINI_PASS_ANGLE             + " int(6) DEFAULT 0, --  最小导通角\n" +
            _TAB_MASHWELDER_W1_FEEDBACK_CURRENT         + " int(6) DEFAULT 0, --  W1焊接电流反馈值\n" +
            _TAB_MASHWELDER_W2_FEEDBACK_CURRENT         + " int(6) DEFAULT 0, --  W2焊接电流反馈值\n" +
            _TAB_MASHWELDER_WELDING_COUNTER             + " int(6) DEFAULT 0, --  焊接计数器\n" +
            _TAB_MASHWELDER_WEIDING_PRENUMBER           + " int(6) DEFAULT 0, --  焊接预制数\n" +
            _TAB_MASHWELDER_THYRISTOR_PASS_TIME         + " int(6) DEFAULT 0, --  可控硅导通时间\n" +
            _TAB_MASHWELDER_WELDING_POINT_NUMBER        + " int(6) DEFAULT 0, --  焊点序号\n" +
            _TAB_MASHWELDER_WELDING_START_SWITCH        + " int(6) DEFAULT 0, --  焊接启动开关\n" +
            _TAB_MASHWELDER_PREPRESSURE_VALVE_SWITCH    + " int(6) DEFAULT 0, --  预压阀启动开关\n" +
            _TAB_MASHWELDER_WELDING_ENABLE_SWITCH       + " int(6) DEFAULT 0, --  焊接使能开关\n" +
            _TAB_MASHWELDER_COUNTER_ZERO_SWITCH         + " int(6) DEFAULT 0, --  计数器清零开关\n" +
            _TAB_MASHWELDER_CLEAR_ALARM_SWITCH          + " int(6) DEFAULT 0, --  清除报警开关\n" +
            _TAB_MASHWELDER_THYRISITOR_SHORT_CIRCUIT    + " int(6) DEFAULT 0, --  可控硅直通\n" +
            _TAB_MASHWELDER_THYRISITOR_CIRCUIT_ERROR    + " int(6) DEFAULT 0, --  可控硅不导通\n" +
            _TAB_MASHWELDER_OVER_HEATED                 + " int(6) DEFAULT 0, --  过热\n" +
            _TAB_MASHWELDER_OVER_CURRENT                + " int(6) DEFAULT 0, --  过电流\n" +
            _TAB_MASHWELDER_UNDER_CURRENT               + " int(6) DEFAULT 0, --  欠电流\n" +
            _TAB_MASHWELDER_TRANSFORMER_RANGE_OVER      + " int(6) DEFAULT 0, --  互感器量程过大\n" +
            _TAB_MASHWELDER_MEMORY_DATA_ERROR           + " int(6) DEFAULT 0, --  存储数据出错\n" +
            _TAB_MASHWELDER_NO_SYNCH_SIGNAL             + " int(6) DEFAULT 0, --  没有同步信号\n" +
            _TAB_MASHWELDER_PASSTIME_LONG               + " int(6) DEFAULT 0,  --  导通时间超过10ms\n" +

            _TAB_MASHWELDER_SCALE_WEIGHT                + " double DEFAULT 0.0,  --  Scale weight\n" +
            _TAB_MASHWELDER_SCALE_COUNT                 + " int(16) DEFAULT 0,  --  Scale count\n" +
            _TAB_MASHWELDER_QUALIFIED_RATE              + " double DEFAULT 0.0,  --  qulified rate\n" +
            _TAB_MASHWELDER_TASK_ID                     + " varchar(255) DEFAULT NULL,  --  task id\n" +
            _ADD_TIME                                   + " datetime DEFAULT NULL,                  \n " +

            _TAB_MASHWELDER_ELECTRODE_PRE_COUNT         + " int(16) DEFAULT 0,  -- electroder pre count \n "+
            _TAB_MASHWELDER_ELECTRODE_CURRENT_COUNT     + " int(16) DEFAULT 0,  -- electroder curr count \n "+
            _TAB_MASHWELDER_ELECTRODE_TOTAL_COUNT       + " int(16) DEFAULT 0,  -- electroder total count \n "+
            _TAB_MASHWELDER_ELECTRODE_SAVE_FLAG         + " tinyint(1) DEFAULT 0  -- electroder save flag \n "+
            ")";
    qDebug()<<"str==="<<str;
    query.execQString(str);
    db.commit();
    db.close();
}

void DBTab_MashWelder::initTable(QSqlDatabase &db)
{
    int version = DataBase::getDataBaseVersion(db);
    db.open();
    db.transaction();
    if(version < DATABASE_VERSION_180730)
    {
        DataBaseQuery query(db);
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_SCALE_WEIGHT," double DEFAULT 0.0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_SCALE_COUNT," int(16) DEFAULT 0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_QUALIFIED_RATE," double DEFAULT 0.0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_TASK_ID," varchar(255) DEFAULT NULL ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,_ADD_TIME," datetime DEFAULT NULL ");
    }
    if(version < DATABASE_VERSION_180804)
    {
        DataBaseQuery query(db);
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_PRE_COUNT," int(16) DEFAULT 0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_CURRENT_COUNT," int(16) DEFAULT 0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_TOTAL_COUNT," int(16) DEFAULT 0 ");
        query.addColumn(DBTab_MashWelder::TAB_MASHWELDER,DBTab_MashWelder::_TAB_MASHWELDER_ELECTRODE_SAVE_FLAG," tinyint(1) DEFAULT 0 ");
    }
    db.commit();
    db.close();
}
