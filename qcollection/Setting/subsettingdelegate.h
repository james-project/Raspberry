#ifndef SUBSETTINGDELEGATE_H
#define SUBSETTINGDELEGATE_H

#include <QItemDelegate>


#define ITEM_HEIGHT 40+(SCREEN_HIGH -480)/16


class SubSettingDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SubSettingDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
    static bool isDisable(const QModelIndex & index);
signals:
    
public slots:

private:

    
};

#endif // SUBSETTINGDELEGATE_H
