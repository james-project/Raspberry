#ifndef SETTINGLISTMODEL_H
#define SETTINGLISTMODEL_H

#include <QAbstractListModel>
#include <QLayout>
#include <QDebug>

class SettingData
{
public:
    QString m_name;
    QString mIcon;
    QString mSetting;

    SettingData(const QString &str1,const QString &str2=QString(),const QString &setting=QString())//,const QString &str2)
    {
        m_name = str1;
        mIcon = str2;
        mSetting = setting;
    }



    SettingData(char *str1,char *str2=NULL,char *str3=NULL)
    {
        m_name = str1;
        if(str2!=NULL)
        {
            mIcon = str2;
        }
        else
        {
            mIcon = QString();
        }

        if(str3!=NULL)
        {
            mSetting = str3;
        }
        else
        {
            mSetting ="";
        }
    }
};

class SettingListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SettingListModel(QObject *parent = 0);
    SettingListModel(QList<SettingData> &list,QObject *parent=0);
    void changeList(QList<SettingData> *list);
    ~SettingListModel();
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
signals:

public slots:

private:
    QList<SettingData> *mListdata;
};

#endif // SETTINGLISTMODEL_H
