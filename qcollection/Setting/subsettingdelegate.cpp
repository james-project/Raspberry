#include "subsettingdelegate.h"
#include "subsettingmodel.h"
#include <QPainter>
#include <QSettings>
//#include "WgtSetting/wgtsettingwidget.h"
#include <QDebug>
#include "common.h"
#include "other/drawtext.h"

SubSettingDelegate::SubSettingDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

void SubSettingDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();

    QRect rectItem = option.rect;

    //rectItem.adjust(10,0,0,0);

    QRect border = QRect(rectItem.left()+4,rectItem.top()+3,rectItem.width()-8,rectItem.height()-6);

    if(index.data(SubSettingModel::ROLE_ISDISABLE).toBool())
    {
        painter->setPen(Qt::gray);
        painter->drawRoundedRect(border,6.0,6.0);
    }
    else
    {
        if(option.state & QStyle::State_Selected)
        {
           // QLinearGradient linearGrad(QPointF(rectItem.left(), rectItem.top()), QPointF(rectItem.left(), rectItem.bottom()));
           // linearGrad.setColorAt(0, QColor(57,135,180));
           // linearGrad.setColorAt(1, QColor(74,172,255));
           // QBrush brush(linearGrad);
            painter->setBrush(option.palette.highlight());
            painter->setPen(Qt::black);
            painter->drawRoundedRect(border,6.0,6.0);
            painter->setPen(Qt::white);
        }
        else
        {
            painter->setPen(Qt::black);
            painter->drawRoundedRect(border,6.0,6.0);
        }
    }

    QFont f("DejaVu Sans",18);
    painter->setFont(f);
    QString icon = index.data(Qt::DecorationRole).toString();  //图片
    QImage image = QImage(icon);
    int offset = (rectItem.height()-image.size().height())/2;
    painter->drawImage(rectItem.left()+10,rectItem.top()+offset,image);

    QString name = index.data(Qt::DisplayRole).toString();     //名字
    QRect nameRect = rectItem;
    nameRect.setLeft(nameRect.left()+image.width()+13);

   // painter->setFont(QFont(painter->font().family(),14));
    painter->drawText(nameRect,Qt::AlignVCenter,name);

    //qDebug("xx:%s",setting.toLatin1().data());
    QString icon2 = index.data(ROLE_ICON).toString();  //图片

    QImage  tag = QImage(icon2);
    offset = (rectItem.height()-tag.size().height())/2;

    painter->drawImage(rectItem.right()-20,rectItem.top()+offset,tag);

    QString setting = index.data(ROLE_SETTING).toString().left(25);
    QRect setRect = nameRect;
    setRect.setLeft(rectItem.right()-30-(painter->fontMetrics().width(setting)));
    painter->setPen(QColor(80,80,80));
    painter->drawText(setRect,Qt::AlignVCenter,setting);

    painter->restore();

}

QSize SubSettingDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    Q_UNUSED(index);
    Q_UNUSED(option);
    return QSize(0,ITEM_HEIGHT);
}
