#include "subsettingwidget.h"
#include "ui_subsettingwidget.h"
#include "subsubsettingdelegate.h"
//#include "WgtSetting/wgtsettingwidget.h"
#include "common.h"
//#include "SysSetting/syssettingwidget.h"
//#include "Dialog/passworddialog.h"
//#include "dataview/usermodel.h"
#include "login/logindialog.h"
#include "subsettingmodel.h"
#include <QPoint>
#include <QDesktopWidget>
//#include <SysSetting/setpasswordwidget.h>
#include <mymessagedialog.h>

#include <QDebug>
#include "setting.h"
//#include "dataview/usermodel.h" //MARK0330
#include "mainwindow.h" //MARK0330

QString SubSettingWidget::mSettingStr = "";

const QString SubSettingWidget::SUBSETTING_MODE = "subsetting_mode";//MARK0330
const QString SubSettingWidget::RECORDPW = "recordpw";//MARK0330

bool HavePressCalKey = false;

SubSettingWidget::SubSettingWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SubSettingWidget)
{
    ui->setupUi(this);
    mListview = ui->listView;
    mBackBtn = ui->backBtn;
    mSubListview = ui->listView_sub;
    mLayout = ui->verticalLayout;
    mTitle = ui->title;
    SubSettingDelegate *delegate = new SubSettingDelegate(mListview);
    mListview->setItemDelegate(delegate);
    SubSubSettingDelegate *delegateSub = new SubSubSettingDelegate(mSubListview);
    mSubListview->setItemDelegate(delegateSub);
    OnSelectSubItem = 0;
    connect(mListview,SIGNAL(clicked(QModelIndex)),this,SLOT(selectItem(QModelIndex)));
    connect(mSubListview,SIGNAL(clicked(QModelIndex)),this,SLOT(selectSubItem(QModelIndex)));
    connect(mBackBtn,SIGNAL(clicked()),this,SLOT(resetSubSettingWigget()));
    mSubWidget = 0;
    mPWDlg = NULL;
//    mListview->setFocusPolicy(Qt::StrongFocus);
}

SubSettingWidget::~SubSettingWidget()
{
    delete ui;
}

void SubSettingWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    int key = keyEvent->key();
    switch(key)
    {
        case Qt::Key_Enter:
        case Qt::Key_Return:          
            if(!mListview->isHidden())
            {
                selectItem(mListview->currentIndex());
            }
            else
            {
                selectSubItem(mSubListview->currentIndex());
            }
            break;
        default:
               keyEvent->ignore();
          break;
    }
}

void SubSettingWidget::resetSubSettingWigget()
{
    mBackBtn->hide();
    mSubListview->hide();
    if(mSubWidget!=0)
    {
        mSubWidget->hide();
        mLayout->removeWidget(mSubWidget);
        delete(mSubWidget);
        mSubWidget = 0;
    }
    mListview->show();
    SetTitlText(mSettingName);
}

void SubSettingWidget::passwordRight(const QString &pw)
{
//    QString passwd = SetPasswordWidget::getTechnicianPasswd();
//    if(pw.compare(passwd)==0)
//    {
//        mPwRight = true;
//    }
//    else if(pw==BACKDOOR_PASSWD)
//    {
//        mPwRight = true;
//    }
//    else if(pw==SUPPER_PASSWD
//            ||pw=="۰۰۰۰۰۰") //波斯语000000
//    {
//        QSettings set(DIR_SETTINGS,QSettings::IniFormat);
//        set.beginGroup(SysSettingWidget::GROUP_NAME);
//        QString str =set.value(SysSettingWidget::PASSWORD_SETTING_NAME).toString();
//        set.endGroup();
//        if(str.isEmpty())
//        {
//            mPwRight = true;
//        }
//        else
//        {
//            mPWDlg->clearPassword();
//            MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
//        }
//    }
//    else
//    {
//        mPWDlg->clearPassword();
//        MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
//    }
//    if(mPwRight)
//        mPWDlg->accept();
    mPwRight = true;

}

void SubSettingWidget::closePasswordDlg()
{
    mPWDlg = 0;
    if(mPwRight==true)
    {
        bool (*call)(QListView *l);
        QModelIndex index = mListview->currentIndex();
        call = (bool (*)(QListView *l))(index.data(SubSettingModel::ROLE_CALLBACK).toInt());
        if(call!=NULL)
        {
            if(call(mSubListview))
            {
                mBackBtn->show();
                mListview->hide();

                SetTitlText(index.data().toString());
            }
            else
            {
                mListview->update(index);
            }
        }
    }
}

void SubSettingWidget::setVisible(bool visible)
{
    if(visible==false)
    {
        emit hideSignal();
    }
    QWidget::setVisible(visible);
}

void SubSettingWidget::selectItem(const QModelIndex& index)
{
    qDebug() << "~~~~~SubSettingWidget::selectItem return";
    if(index.data(SubSettingModel::ROLE_ISDISABLE).toBool())
        return;


    qDebug() << "~~~~~SubSettingWidget::selectItem cpmein";
    if(mPwRight==false&&/*LoginDialog::permission()<UserModel::ADMIN&&*/index.data(SubSettingModel::ROLE_NEEDPW).toBool())
    {
        if(mPWDlg==NULL)
        {
            //mIndex = &index;
            qDebug()<<mIndex<<"selectItem"<<index.row();
            mPWDlg = new PassWordDialog(this);
            mPWDlg->setFixedSize(SCREEN_WIDTH*7/24,SCREEN_HIGH*7/24);
            mPWDlg->move(mapFromGlobal(QPoint(SCREEN_WIDTH*17/48,SCREEN_HIGH*10/48-40)));
            mPWDlg->show();

            connect(mPWDlg,SIGNAL(passwdInputedFinish(QString)),this,SLOT(passwordRight(QString)));
            connect(mPWDlg,SIGNAL(destroyed()),this,SLOT(closePasswordDlg()));
            connect(this,SIGNAL(hideSignal()),mPWDlg,SLOT(clickCancelBtn()));
        }
        return;
    }
    else
    {
        if(mPWDlg!=NULL)
        {
            delete mPWDlg;
            mPWDlg = NULL;
        }
    }

    bool (*call)(QListView *l);
    call = (bool (*)(QListView *l))(index.data(SubSettingModel::ROLE_CALLBACK).toInt());
    if(call!=NULL)
    {
        if(call(mSubListview))
        {
            mBackBtn->show();
            mListview->hide();

            SetTitlText(index.data().toString());
        }
        else
        {
            mListview->update(index);
        }
    }
}

void SubSettingWidget::setSettingName(QString str)
{
    mSettingName = str;
}

void SubSettingWidget::selectSubItem(const QModelIndex& index)
{
    QSettings mSet(DIR_SETTINGS,QSettings::IniFormat);
    mSet.beginGroup(mGroupStr);
    int sel =index.row();
    if(sel!=mSet.value(mSettingStr,0)){
        mSet.setValue(mSettingStr,sel);
    }
    mSet.endGroup();

    if(OnSelectSubItem!=0)
    {
        OnSelectSubItem(index);
        OnSelectSubItem = 0;
    }
    resetSubSettingWigget();
}

/*Add Plastic ----zhanglong*/
void SubSettingWidget::selectSubItem1(const QModelIndex& index)
{
    QSettings mSet(DIR_SETTINGS,QSettings::IniFormat);
    mSet.beginGroup(mPlasticStr);
    int sel =index.row();
    if(sel!=mSet.value(mSettingStr,0)){
        mSet.setValue(mSettingStr,sel);
    }
    mSet.endGroup();

    if(OnSelectSubItem!=0)
    {
        OnSelectSubItem(index);
        OnSelectSubItem = 0;
    }
    resetSubSettingWigget();
}

/*
void SubSettingWidget::setOnSelectSubItem(void (*onSelect)(const QModelIndex& index))
{
    OnSelectSubItem = onSelect;
}
*/

void SubSettingWidget::setSubListView(QListView *lv,QList<SettingData> & menus,QString groupName)
{
    QSettings mSet(DIR_SETTINGS,QSettings::IniFormat);
    mSet.beginGroup(groupName);
    int sel = mSet.value(mSettingStr,0).toInt();
    mSet.endGroup();

//    SettingListModel *model = new SettingListModel(menus,lv);
//    lv->setModel(model);
    SettingListModel *model = new SettingListModel(menus,lv);
    lv->setModel(model);


    QModelIndex index = model->index(sel);
    lv->setCurrentIndex(index);
    lv->show();

   //qDebug()<<mGroupStr<<mSettingStr;
}

void SubSettingWidget::SetTitlText(const QString &str)
{
    mTitle->setText(str);
}

