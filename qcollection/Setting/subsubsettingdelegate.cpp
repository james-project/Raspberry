#include "subsubsettingdelegate.h"
#include <QPainter>
#include <QDebug>
#include "other/drawtext.h"
#include "subsettingmodel.h"

SubSubSettingDelegate::SubSubSettingDelegate(QObject *parent) :
    QItemDelegate(parent)
{

}

void SubSubSettingDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();
    QFont f("DejaVu Sans",18);
    painter->setFont(f);

    QImage  tag;
    if (option.state & QStyle::State_Selected)
    {
        //painter->fillRect(option.rect, option.palette.highlight());
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setPen(Qt::white);
        QString str = index.data(Qt::DecorationRole).toString();
        if(str!=QString())
        {
            tag = QImage(str);
        }
        else
        {
            tag = QImage(":/res/setting/select_icon2.png");
        }
    }
    else
    {
         painter->setPen(Qt::black);
         QString str = index.data(Qt::DecorationRole).toString();
         if(str!=QString())
         {
             tag = QImage(str);
         }
         else
         {
             tag = QImage(":/res/setting/select_icon3.png");
         }
    }

    QRect rectItem = option.rect;
    if(index.data(SubSettingModel::ROLE_ISDISABLE).toBool())
    {
        painter->setPen(Qt::gray);
    }
    else
    {
        if(option.state & QStyle::State_Selected)
        {
            painter->setBrush(option.palette.highlight());
            painter->setPen(Qt::white);
        }
        else
        {
            painter->setPen(Qt::black);
        }
    }

    QString name = index.data(Qt::DisplayRole).toString();
    int offset = (rectItem.height()-tag.size().height())/2;

    QString set = index.data(Qt::EditRole).toString();

    QRect nameRect = rectItem;
    nameRect.setLeft(nameRect.left()+10);

    painter->drawText(nameRect,Qt::AlignVCenter,name);

    int xright = rectItem.right()-30;
    nameRect.setRight(xright);
    nameRect.setLeft(xright-painter->fontMetrics().width(set));
    if(set.length()>0)
    {
        painter->drawText(nameRect,Qt::AlignVCenter,set);
    }
    painter->drawImage(rectItem.right()-20,rectItem.top()+offset,tag);
    painter->setPen(Qt::black);
    painter->drawLine(rectItem.bottomLeft(),rectItem.bottomRight());
    painter->restore();

//    painter->drawImage();
    /*
     *
     *
    offsetY = (ITEM_HEIGHT - tag.height()) / 2;
    QRect tagRect = QRect(opt.rect.topLeft()+QPoint(490,offsetY),tag.size());

    painter->save();

    const QString& icon = index.data(Qt::UserRole).toString();
    QPixmap pixmap = QPixmap(icon);
    int offsetY = (ITEM_HEIGHT - pixmap.height()) / 2;
    QRect iconRect = QRect(opt.rect.topLeft()+QPoint(235,offsetY),pixmap.size());

    QRect tagRect = QRect(opt.rect);
    tagRect.setRight(ITEM_SPACING*2);

    //QFontMetrics fm(painter->font());

    const QString& caption = index.data(Qt::UserRole + 2).toString();
    QRect captionRect = QRect(opt.rect);
    captionRect.setLeft(tagRect.right());

    QLinearGradient linearGrad(QPointF(opt.rect.left(), opt.rect.top()), QPointF(opt.rect.left(), opt.rect.bottom()));
    linearGrad.setColorAt(0, QColor(57,135,180));
    linearGrad.setColorAt(1, QColor(74,172,228));
    QBrush brush(linearGrad);

    opt.palette.setBrush(QPalette::Highlight,brush);

    drawBackground(painter,opt,index);

    if(opt.state & QStyle::State_Selected)
    {
        painter->setPen(Qt::white);
        //opt.state = opt.state ^ QStyle::State_Selected;
    }else
    {
        painter->setPen(Qt::black);
    }

    //painter->fillRect(iconRect,QColor(255,0,0));
    painter->drawPixmap(iconRect,pixmap);
    //painter->fillRect(captionRect,QColor(255,0,0));
    //painter->drawText(captionRect,caption);
    painter->drawText(captionRect,Qt::AlignVCenter,caption);

    painter->setPen(Qt::darkGray);
    painter->drawLine(opt.rect.bottomLeft(),opt.rect.bottomRight());
    */
}

QSize SubSubSettingDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    Q_UNUSED(index);
    Q_UNUSED(option);
    return QSize(0,ITEM_HEIGHT);
}
