#include "subwidget.h"
#include <QLayout>
#include <QDebug>

SubWidget::SubWidget(QWidget *parent, QLayout *layout, QWidget *comeinWidget) :
    QWidget(parent)
{
    mParent = parent;
    if (parent) {
        parent->hide();
    }
    mLayout = layout;
    if (mLayout) {
        mLayout->addWidget(this);
    }
    mComein = comeinWidget;
    mMyLayout = 0;
    setAttribute(Qt::WA_DeleteOnClose);
}

void SubWidget::hideWidget()
{
    this->hide();
    if (mLayout)
        mLayout->removeWidget(this);
    if (mMyLayout && mComein)
        mMyLayout->removeWidget(mComein);

    if (mParent) {
        mParent->show();
        mParent->setFocus();
    }
}

void SubWidget::showWidget()
{
    this->show();
    this->setFocus();
    if (mLayout)
        mLayout->addWidget(this);
    if (mMyLayout && mComein) {
        mMyLayout->addWidget(mComein);
    }
    if (mParent) {
        mParent->hide();
    }
}

void SubWidget::keyPressEvent(QKeyEvent *keyEvent)
{
    switch (keyEvent->key()) {
    case Qt::Key_Return:
        qDebug() << "\n\n\n ~~~~~~~~~~~~~~~~~ SubWidget::keyPressEvent:";
        keyEvent->ignore();
        break;
    default:
        keyEvent->ignore();
        break;
    }
    QWidget::keyPressEvent(keyEvent);
}

void SubWidget::quit()
{
    SubWidget::hideWidget();
    this->deleteLater();
    emit quitSignal();
}

void SubWidget::layoutComein()
{
    if (mMyLayout && mComein) {
        mMyLayout->addWidget(mComein);
        mComein->show();
    }
}
