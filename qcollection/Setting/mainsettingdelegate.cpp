#include "mainsettingdelegate.h"
#include <QPainter>
#include "other/drawtext.h"
#include <QDebug>

MainSettingDelegate::MainSettingDelegate(QObject *parent)
    :QItemDelegate(parent)
{
    mHeight = HEIGHT(50,70);
}

void MainSettingDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();

    QFont f("DejaVu Sans",18);
    painter->setFont(f);

    if (option.state & QStyle::State_Selected)
    {
        QRect rect = option.rect;
        rect.setLeft(rect.left()-1);
        painter->fillRect(rect, QColor(255,255,255));
        painter->setPen(Qt::black);
    }    
    //painter->setPen(Qt::black);
    QRect rectItem = option.rect;

    QString name = index.data(Qt::DisplayRole).toString();
    QImage  tag = QImage(":/res/setting/arrows_icon.png");

    int offset = (rectItem.height()-tag.size().height())/2;

    QRect nameRect = rectItem;

    rectItem.setLeft(nameRect.left());

    QImage  tag1 = QImage(index.data(Qt::DecorationRole).toString());
    painter->drawImage(rectItem.left()+5,rectItem.top()+offset,tag1);

    nameRect.setLeft(nameRect.left()+40);
    painter->drawImage(rectItem.right()-20,rectItem.top()+offset,tag);
    painter->drawText(nameRect,Qt::AlignVCenter,name);

    painter->setPen(QColor(0x43,0x55,0x68));
    painter->drawLine(rectItem.bottomLeft(),rectItem.bottomRight());
    painter->restore();
}

void MainSettingDelegate::setHeightSize(int height)
{
    mHeight = height;
}

QSize MainSettingDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    return QSize(0,mHeight);
}
