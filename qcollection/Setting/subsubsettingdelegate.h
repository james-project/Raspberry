#ifndef SELECTSETTINGDELEGATE_H
#define SELECTSETTINGDELEGATE_H

#include <QItemDelegate>
#include <QItemDelegate>
#include "common.h"

#define ITEM_HEIGHT 40+(SCREEN_HIGH -480)/16

class SubSubSettingDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SubSubSettingDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
    
signals:
    
public slots:
    
};

#endif // SELECTSETTINGDELEGATE_H
