#ifndef SUBSETTINGWIDGET_H
#define SUBSETTINGWIDGET_H

#include <QWidget>
#include <QModelIndex>
#include <QVBoxLayout>
#include <QListView>
#include <QPushButton>
#include <QLabel>
#include "Dialog/passworddialog.h"
#include "subsettingdelegate.h"
#include "subsettingmodel.h"
#include "settinglistmodel.h"
#include <QSettings>

//#include "subsubEnableDelegate.h"


namespace Ui {
class SubSettingWidget;
}


class SubSettingWidget : public QWidget
{
    Q_OBJECT


public:
    explicit SubSettingWidget(QWidget *parent = 0);
    ~SubSettingWidget();
    void setSettingName(QString str);
    void SetBackButton(QPushButton *btn);
    void SetLeftWidget(QWidget *widget);
    void SetTitlText(const QString &str);

    //static TSettings *mSet;


public slots:
    virtual void selectItem(const QModelIndex &index);
    virtual void setVisible(bool visible);

    virtual void selectSubItem(const QModelIndex &index);
    virtual void selectSubItem1(const QModelIndex &index);
    static void setSubListView(QListView *lv,QList<SettingData> & menus,QString groupName);
    void resetSubSettingWigget(void);
    void passwordRight(const QString & passwd);
    void closePasswordDlg(void);
signals:
    void hideSignal();


    //void setOnSelectSubItem( void(*onSelect)(const QModelIndex& ));

protected:
    QString mGroupStr;
    QString mPlasticStr;
    static QString mSettingStr;

    QVBoxLayout *mLayout;
    QListView *mListview;
    QListView *mSubListview;
    QWidget *mSubWidget ;

    QPushButton *mBackBtn;
    QLabel *mTitle;


    void (*OnSelectSubItem)(const QModelIndex& index);

    int mTimeId;

private:
    void keyPressEvent(QKeyEvent *keyEvent);

    bool isDisable(const QModelIndex &index);
    Ui::SubSettingWidget *ui;
    QString mSettingName;
    bool mPwRight;
    PassWordDialog *mPWDlg;
    int mIndex;

    static const QString SUBSETTING_MODE;//MARK0330
    static const QString RECORDPW;//MARK0330

};

#endif // SUBSETTINGWIDGET_H
