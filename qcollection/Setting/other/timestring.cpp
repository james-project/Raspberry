#include "timestring.h"
//#include "SysSetting/solardate.h"
#include <QApplication>
#include <QLocale>
#include <QDesktopWidget>

QString TimeString::timetoString(QTime time)
{
    return time.toString("hh:mm");
}

QString TimeString::timetoStringHMS(QTime time)
{

    return time.toString("hh:mm:ss");
}

QString TimeString::stringCurrTime()
{
    return timetoString(QTime::currentTime());
}

QTime TimeString::fromString(const QString &str)
{
    return QTime::fromString(str,"hh:mm");
}

