#ifndef TIMESTRING_H
#define TIMESTRING_H
#include <QTime>
class TimeString
{
public:
    static QString stringCurrTime();
    static QString timetoString(QTime time);
    static QString timetoStringHMS(QTime time);
    static QTime fromString(const QString &str);
};

#endif // TIMESTRING_H
