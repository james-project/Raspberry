#ifndef DRAWTEXT_H
#define DRAWTEXT_H
#include <QPainter>
class DrawText
{
public:
    static void drawText(QPainter *painter,const QRect & rectangle, int flags, const QString &text);
};

#endif // DRAWTEXT_H
