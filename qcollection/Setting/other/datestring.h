#ifndef DATESTRING_H
#define DATESTRING_H
#include <QDate>

class DateString
{
public:
    static QString stringCurrDate();
    static QString dateToCurrFormat(QDate d);
    static QString datetoFormat(QDate d,QString fmt="");
    static QString currDateToFormat(QString fmt);
    static void setDateFormat(QString str);
    static QDate fromString(QString str);
private:
    static QString mDateForamt;
    static QString mSp;
};

#endif // DATESTRING_H
