#include "datestring.h"
//#include "SysSetting/syssettingwidget.h"
#include "common.h"
#include <QDebug>
//#include <SysSetting/solardate.h>
#include <QSettings>


QString DateString::mDateForamt = "";
QString DateString::mSp = "";


QString DateString::stringCurrDate()
{
//    if(mDateForamt=="")
//    {
//        QSettings set(DIR_SETTINGS,QSettings::IniFormat);
//        set.beginGroup(SysSettingWidget::GROUP_NAME);

//        mDateForamt = set.value(SysSettingWidget::DATE_FORMAT_SETTING_NAME,"yyyy MM dd").toString();

//        mSp = set.value(SysSettingWidget::DATE_SEPARATOR_SETTING_NAME,"/").toString();
//        set.endGroup();
//        mDateForamt.replace(QString(" "),mSp);
//        //qDebug()<<mDateForamt;
//    }

//    if(QApplication::desktop()->locale()==QLocale::Persian)
//    {
//        return SolarDate::gregorianToSolar(QDate::currentDate()).toString(mSp);
//    }
//    return QDate::currentDate().toString(mDateForamt);
}

QString DateString::datetoFormat(QDate d, QString fmt)
{
//    if(QApplication::desktop()->locale()==QLocale::Persian)
//    {
//        return SolarDate::gregorianToSolar(d).toString(mSp);
//    }
//    else
//    {
//        if(fmt=="")
//        {
//            return d.toString(mDateForamt);
//        }
//        else
//        {
//            return d.toString(fmt);
//        }
//    }
}

QString DateString::currDateToFormat(QString fmt)
{
//    if(QApplication::desktop()->locale()==QLocale::Persian)
//    {
//        return SolarDate::gregorianToSolar(QDate::currentDate()).toString(mSp);
//    }
    return QDate::currentDate().toString(fmt);
}

QString DateString::dateToCurrFormat(QDate d)
{
//    if(QApplication::desktop()->locale()==QLocale::Persian)
//    {
//        return SolarDate::gregorianToSolar(d).toString(mSp);
//    }
    return d.toString(mDateForamt);
}


void DateString::setDateFormat(QString str)
{
   mDateForamt = str;
   mSp = "/";
   if(mDateForamt.contains("-"))
       mSp = "-";
   //qDebug()<<mDateForamt;
}

QDate DateString::fromString(QString str)
{
    return QDate::fromString(str,mDateForamt);
}


