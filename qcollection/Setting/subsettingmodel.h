//#ifndef SUBSETTINGMODEL_H
//#define SUBSETTINGMODEL_H

//#include <QAbstractListModel>
//#include <QListView>

//enum UsrRole
//{
//    ROLE_SETTING=Qt::UserRole,
//    ROLE_INFECTAPPROVAL=Qt::UserRole+1,
//    ROLE_CALLBACK=Qt::UserRole+2,
//    ROLE_ICON = Qt::UserRole+3
//};

//class SubSettingData
//{
//public:
//    QString mName;//菜单名
//    QString mIcon;//菜单图标
//    bool (*mCallBack)(QListView *l);//选择后执行的操作，返回值表示是否要弹出子菜单
//    QString (*mGetSetting)(void);
//    enum InfectionApproval
//    {
//        None = 0,
//        OIML =0x01,
//        NETP = 0x02,
//        CMC = 0x04,
//        CALSW_PRO = 0X08,
//        PW_PRO = 0X20,
//        CHN_NO_CHANGE = 0X40,
//        PERMISSION_ADMIN = 0X80,
//        PERMISSION_TECH = 0X100,
//        PERMISSION =0X280
//    };
//    int mInfectApprov;
//    QString (*mGetIcon)(void);
//    SubSettingData(const QString &str1, const QString &str2 = NULL, QString (*getSetting)(void) = NULL, bool (*call)(QListView*) = NULL, int approval = None, QString (*getIcon)(void) = NULL)
//    {
//        mName = str1;
//        mIcon = str2;
//        mGetSetting = getSetting;
//        mCallBack = call;
//        mInfectApprov = approval;
//        mGetIcon = getIcon;
//    }

//};

//class SubSettingModel : public QAbstractListModel
//{
//    Q_OBJECT
//public:
//    enum UserRole
//    {
//        ROLE_SETTING = Qt::UserRole,
//        ROLE_INFECTAPPROVA,
//        ROLE_CALLBACK,
//        ROLE_ICON,
//        ROLE_ISDISABLE,
//        ROLE_NEEDPW
//    };

//    explicit SubSettingModel(QObject *parent = 0);
//    SubSettingModel(QList<SubSettingData> &list, QObject *parent = 0);
//    ~SubSettingModel();
//    int rowCount(const QModelIndex &parent = QModelIndex()) const;
//    void addItem(SubSettingData set);
//    void removeLast();
//    QVariant data(const QModelIndex &index, int role) const;
//    bool isDisable(int pos) const;
//    static int mCurrRow;
//private:
//    QList<SubSettingData> *dataList;
//};

//#endif // SUBSETTINGMODEL_H


#ifndef SUBSETTINGMODEL_H
#define SUBSETTINGMODEL_H

#include <QAbstractListModel>
#include <QListView>
enum UsrRole
{
    ROLE_SETTING=Qt::UserRole,
    ROLE_INFECTAPPROVAL=Qt::UserRole+1,
    ROLE_CALLBACK=Qt::UserRole+2,
    ROLE_ICON = Qt::UserRole+3
};

class SubSettingData
{
public:
    QString mName; //菜单名
    QString mIcon; //菜单图标
    //QString mSetting; //设置名
    bool (*mCallBack)(QListView *l); //选择后执行的操作,返回值表示是否要弹出子菜单
    QString (*mGetSetting)(void);

    enum  InfectionApproval //此菜单是否受认证影响
    {
        None = 0,//不受影响
        OIML = 0x001,//受OIML 影响
        NTEP = 0x002,
        CMC  = 0x004,
        CALSW_PRO=0x008, //校正开关影响
        PW_PRO =0x010,    //受密码保护
        CHN_NO_CHANGE = 0x020,//中文下不可以修改

        PERMISSION=0x0C0,
        PERMISSION_USER= 0x00,
        PERMISSION_MANAGER=0x40,
        PERMISSION_ADMIN = 0x80,
        SRI_LANKAN  =0x100,

    };

    int  mInfectApprov;
    QString (*mGetIcon)(void);
    //int mDoubleSelection;


    SubSettingData(const QString &str1,
                   const QString &str2=NULL,
                   QString (*getSetting)(void)=NULL,
                   bool (*call)(QListView*)=NULL,
                   int approval=None,
                   QString (*getIcon)(void)=NULL)//,const QString &str2)
    {
        mName = str1;
        mIcon = str2;
        mGetSetting = getSetting;
        mCallBack = call;
        mInfectApprov = approval;
        mGetIcon = getIcon;
    }

    bool operator ==(const SubSettingData &t)
    {
        if(mName==t.mName)
            return true;
        return false;
    }
};

class SubSettingModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum UsrRole
    {
        ROLE_SETTING=Qt::UserRole,
        ROLE_INFECTAPPROVA,
        ROLE_CALLBACK,
        ROLE_ICON,
        ROLE_ISDISABLE,
        ROLE_NEEDPW

    };
    explicit SubSettingModel(QObject *parent = 0);
    SubSettingModel(QList<SubSettingData> &list,QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const;
    void addItem(const SubSettingData &set);
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    void insertItems(int i,QList<SubSettingData> *list);
    void removeItems(QList<SubSettingData> *list);
    void removeItemAt(int i);
    void removeItem(const SubSettingData set);
    void removeLast();
    bool isDisable(int pos) const;

    ~SubSettingModel();
    static int mCurrRow;

signals:

public slots:

private:

   QList<SubSettingData> *dataList;


};

#endif // SUBSETTINGMODEL_H
