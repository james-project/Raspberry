#ifndef MAINSETTINGDELEGATE_H
#define MAINSETTINGDELEGATE_H

#include <QItemDelegate>
#include "common.h"



class MainSettingDelegate : public QItemDelegate
{
public:
    MainSettingDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
    void setHeightSize(int height);

private:
    int mHeight;
    QColor mBackgroundColor;
    QColor mPenColor;

};

#endif // MAINSETTINGDELEGATE_H
