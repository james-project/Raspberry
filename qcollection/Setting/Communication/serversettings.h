#ifndef SERVERSETTINGS_H
#define SERVERSETTINGS_H

#include <QWidget>

namespace Ui {
class ServerSettings;
}

class ServerSettings : public QWidget
{
    Q_OBJECT

public:
    explicit ServerSettings(QWidget *parent = 0);
    ~ServerSettings();

    static QString getServerIP();
    static int getServerPort();
    static QString getServerSettings();
private:
    Ui::ServerSettings *ui;
    static const QString SETTING_SERVER_GROUP;
    static const QString SETTING_SERVER_IP;
    static const QString SETTING_SERVER_PORT;
};

#endif // SERVERSETTINGS_H
