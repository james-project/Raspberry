#include "comsettingwidget.h"
#include "common.h"
#include <QDebug>
#include <QApplication>
#include "serversettings.h"
#include "../../Device/mashwelder.h"
#include "../../deviceidsettings.h"
#include "../../videoip.h"
#include "../../mainwindow.h"
#include "../../temperaturesettings.h"


const QString ComSettingWidget::GROUP_NAME="communication";

ComSettingWidget::ComSettingWidget(QWidget *parent) :
    SubSettingWidget(parent)
{
    QList<SubSettingData> submenus;
    submenus<<SubSettingData(tr("Server Setting"),NULL,getServerSettings, serverSettings);
    submenus<<SubSettingData(tr("Device Id"),NULL, getDeviceIdSettings, deviceIdSettings);
    submenus<<SubSettingData(tr("Video Ip"),NULL, getVideoIpSettings, videoIpSettings);
    submenus<<SubSettingData(tr("G-DIO Internal Temperature"),NULL, getTemperatureSettings, NULL);


    mModel = new SubSettingModel(submenus,this);
    mListview->setModel(mModel);
    QModelIndex index = mModel->index(0);
    mListview->setCurrentIndex(index);
}

QString ComSettingWidget::getServerSettings()
{
    qDebug()<<"ComSettingWidget::getServerSettings-----------";
    return ServerSettings::getServerSettings();
}

QString ComSettingWidget::getDeviceIdSettings()
{
    qDebug()<<"ComSettingWidget::getDeviceIdSettings-----------";
    return DeviceIdSettings::getDeviceId();
}

QString ComSettingWidget::getVideoIpSettings()
{
    return VideoIp::getVideoIp();
}

QString ComSettingWidget::getTemperatureSettings()
{
    TSystem2("cat /sys/class/thermal/thermal_zone0/temp > /home/Work/master/Raspberry/qcollection/Temperature");
    QFile file("/home/Work/master/Raspberry/qcollection/Temperature");
    if( !file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        qDebug() << file.error();
        return QString();
    }

    QTextStream in(&file);
     return QString::number(in.readLine().toDouble()/1000.0 ) + QString(" 度");
}



bool ComSettingWidget::serverSettings(QListView *l)
{
    qDebug()<<"ComSettingWidget::serverSettings-----------";
    ComSettingWidget *sys = (ComSettingWidget *)(l->parentWidget());
    ServerSettings * w = new ServerSettings(sys);
    sys->mSubWidget = w;
    sys->layout()->addWidget(w);
    return true;
}

bool ComSettingWidget::deviceIdSettings(QListView *l)
{
    qDebug()<<"ComSettingWidget::deviceIdSettings-----------";
    ComSettingWidget *sys = (ComSettingWidget *)(l->parentWidget());
    DeviceIdSettings * w = new DeviceIdSettings(sys);
    sys->mSubWidget = w;
    sys->layout()->addWidget(w);
    return true;
}

bool ComSettingWidget::videoIpSettings(QListView *l)
{
    qDebug()<<"ComSettingWidget::deviceIdSettings-----------";
    ComSettingWidget *sys = (ComSettingWidget *)(l->parentWidget());
    VideoIp * w = new VideoIp(sys);
    sys->mSubWidget = w;
    sys->layout()->addWidget(w);
    return true;
}

bool ComSettingWidget::temperatureSettings(QListView *l)
{
    ComSettingWidget *sys = (ComSettingWidget *)(l->parentWidget());
    TemperatureSettings * w = new TemperatureSettings(sys);
    sys->mSubWidget = w;
    sys->layout()->addWidget(w);
    return true;
}



