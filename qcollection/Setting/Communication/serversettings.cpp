#include "serversettings.h"
#include "ui_serversettings.h"
#include "Setting/BasicSetting/basicsettingwidget.h"
#include <QSettings>
#include "common.h"
const QString ServerSettings::SETTING_SERVER_GROUP = "server";
const QString ServerSettings::SETTING_SERVER_IP = "ip";
const QString ServerSettings::SETTING_SERVER_PORT = "port";

ServerSettings::ServerSettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerSettings)
{
    ui->setupUi(this);
    ui->le_ip->setText(getServerIP());
    ui->le_ip->setInputMask("000.000.000.000");
    ui->le_port->setText(QString::number(getServerPort()));
}

QString ServerSettings::getServerIP()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    return set.value(SETTING_SERVER_GROUP+"/"+SETTING_SERVER_IP,"192.168.168.48").toString();
}

int ServerSettings::getServerPort()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    return set.value(SETTING_SERVER_GROUP+"/"+SETTING_SERVER_PORT,80).toInt();
}

QString ServerSettings::getServerSettings()
{
    return getServerIP()+":"+QString::number(getServerPort());
}


ServerSettings::~ServerSettings()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(SETTING_SERVER_GROUP);
    set.setValue(SETTING_SERVER_PORT,ui->le_port->text().toInt());
    set.setValue(SETTING_SERVER_IP,ui->le_ip->text());
    set.endGroup();
    delete ui;
}


