#ifndef COMSETTINGWIDGET_H
#define COMSETTINGWIDGET_H
#include "Setting/subsettingwidget.h"
#include <QListView>
class ComSettingWidget : public SubSettingWidget
{
    Q_OBJECT
public:
    static const QString GROUP_NAME;
public:
    explicit ComSettingWidget(QWidget *parent = 0);
    static bool serverSettings(QListView *l);
    static bool deviceIdSettings(QListView *l);
    static bool temperatureSettings(QListView *l);


    static bool videoIpSettings(QListView *l);
    static QString getServerSettings();
    static QString getDeviceId();
    static QString getDeviceIdSettings();
    static QString getVideoIpSettings();
    static QString getTemperatureSettings();

private:
    SubSettingModel *mModel;
};

#endif // COMSETTINGWIDGET_H
