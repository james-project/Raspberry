#ifndef SETTING_H
#define SETTING_H

#include <QWidget>
#include <QListView>
#include "subwidget.h"
#include "Setting/subsettingmodel.h"

namespace Ui {
class Setting;
}

class Setting : public SubWidget
{
    Q_OBJECT

public:
//    enum SettingItem
//    {
//        SettingItem_Basic = 0,
//        SettingItem_Operating
//    };

    /*explicit*/ Setting(QWidget *parent, QLayout *layout);
    ~Setting();
    void keyPressEvent(QKeyEvent *keyEvent);
private slots:
    void showStackWidget(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Ui::Setting *ui;
};

#endif // SETTING_H
