#ifndef SUBWIDGET_H
#define SUBWIDGET_H

#include <QWidget>
#include <QKeyEvent>

class SubWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SubWidget(QWidget *parent, QLayout *layout, QWidget *comein = 0);
    virtual void hideWidget(void);
    virtual void showWidget(void);
    void keyPressEvent(QKeyEvent *keyEvent);

signals:
    void quitSignal(void);

public slots:
    void quit(void);

protected:
    void layoutComein(void);

    QWidget *mParent;
    QLayout *mLayout;
    QWidget *mComein;
    QLayout *mMyLayout;
};

#endif // SUBWIDGET_H
