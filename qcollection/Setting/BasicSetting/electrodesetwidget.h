#ifndef ELECTRODESETWIDGET_H
#define ELECTRODESETWIDGET_H

#include <QWidget>

namespace Ui {
class ElectrodeSetWidget;
}

class ElectrodeSetWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ElectrodeSetWidget(QWidget *parent = 0);
    ~ElectrodeSetWidget();
    bool eventFilter(QObject *watched, QEvent *event);
    static int getElectrode_preCount();
    static void setPreCount(int preCount);
    static int getElectrode_currentCount();
    static void setElectrode_currentCount(int currCount);
    static void setElectrode_remainCount(int remaincount);
    static int getElectrode_remainCount();
public slots:
    void acceptInputValue_OfElectrodePreCount(int preCount);

private:
    Ui::ElectrodeSetWidget *ui;
    static const QString ELECTRODE_GROUP;
    static const QString ELECTRODE_PRECOUNT;
    static const QString ELECT_CURRENTCOUNT;
    static const QString ELECTRODE_REMAINCOUNT;
};

#endif // ELECTRODESETWIDGET_H
