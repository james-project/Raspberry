#ifndef BASICSETTINGWIDGET_H
#define BASICSETTINGWIDGET_H

#include "Setting/subsettingwidget.h"


class BasicSettingWidget : public SubSettingWidget
{
    Q_OBJECT

public:
    enum OprtMode
    {
        MASHWELDER_MODE = 0,
        PLASTIC_MODE,
        PUNCHING_MODE
    };

    enum Plastic
    {
        Plastic_3 = 0,
        Plastic_45,
        Plastic_6
    };

    explicit BasicSettingWidget(QWidget *parent = 0);
    static const QString GROUP_NAME;
    static const QString Plastic_NAME;
    const static QString WORK_MODE_SETTING;
    const static QString Plastic_SETTING;

    void selectSubItem(const QModelIndex &index);
    //void selectSubItem(const QModelIndex &index);
    static int getOprtMode();
    static int getOprtPlastic();
    static void setOprtMode(OprtMode mode );
    static void setOprtPlastic(OprtMode mode );
    static QString getOprtModeStr();
    static QString getOprtPlasticStr();

    static bool setElectrodeWidgetMode(QListView *l);
    static QString getElectrodeWidgetMode();
private:
    static bool setOprtMode(QListView *lv);
    static bool setOprtPlastic(QListView *lv);
    static QList<SettingData> mWorkModeMenus;
    static QList<SettingData> plasicModeMenus;


private:
};

#endif // BASICSETTINGWIDGET_H


