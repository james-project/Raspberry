#include "electrodesetwidget.h"
#include "ui_electrodesetwidget.h"
#include "common.h"
#include "input/inputdialog.h"
#include <QDebug>
#include <QSettings>

const QString ElectrodeSetWidget::ELECTRODE_GROUP = "electrode_group";
const QString ElectrodeSetWidget::ELECTRODE_PRECOUNT = "electrode_precount";
const QString ElectrodeSetWidget::ELECT_CURRENTCOUNT = "electrode_currentcount";
const QString ElectrodeSetWidget::ELECTRODE_REMAINCOUNT = "electrode_remaincount";

ElectrodeSetWidget::ElectrodeSetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ElectrodeSetWidget)
{
    ui->setupUi(this);
    ui->le_preCount->installEventFilter(this);
    ui->le_currentCount->setReadOnly(true);
    ui->le_preCount->setText(QString::number(getElectrode_preCount()));
    ui->le_currentCount->setText(QString::number(getElectrode_currentCount()));
}

ElectrodeSetWidget::~ElectrodeSetWidget()
{
    delete ui;
}

bool ElectrodeSetWidget::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if (watched == ui->le_preCount) {
            QWidget *back = new QWidget(this);
            back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
            back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);//Windows上设为透明色 需有Qt::FramelessWindowHint
            back->setAttribute(Qt::WA_TranslucentBackground,true);//Windows上设为透明色
            back->setWindowModality(Qt::WindowModal);
            back->show();
            inputDialog *inputdialog = new inputDialog(back,"电极预制数",false,0,1000000,1,true);
            inputdialog->setWindowModality(Qt::WindowModal);
            inputdialog->move(0,0);
            inputdialog->show();
            connect(inputdialog,SIGNAL(inputValue(int)),this,SLOT(acceptInputValue_OfElectrodePreCount(int)));
            connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
            connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
        }
    }
    return QWidget::eventFilter(watched,event);
}

int ElectrodeSetWidget::getElectrode_preCount()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    int precount =  set.value(ELECTRODE_PRECOUNT,0).toInt();
    set.endGroup();
    return precount;
}

int ElectrodeSetWidget::getElectrode_currentCount()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    int currcount =  set.value(ELECT_CURRENTCOUNT,0).toInt();
    set.endGroup();
    return currcount;
}

void ElectrodeSetWidget::setElectrode_currentCount(int currCount)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    set.setValue(ELECT_CURRENTCOUNT,currCount);
    set.endGroup();

}

int ElectrodeSetWidget::getElectrode_remainCount()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    int remaincount =  set.value(ELECTRODE_REMAINCOUNT,0).toInt();
    set.endGroup();
    return remaincount;
}

void ElectrodeSetWidget::setElectrode_remainCount(int remaincount)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    set.setValue(ELECTRODE_REMAINCOUNT,remaincount);
    set.endGroup();

}

void ElectrodeSetWidget::acceptInputValue_OfElectrodePreCount(int preCount)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    set.setValue(ELECTRODE_PRECOUNT,preCount);
    set.setValue(ELECTRODE_REMAINCOUNT,preCount);
    set.setValue(ELECT_CURRENTCOUNT,0);
    ui->le_preCount->setText(QString::number(preCount));
    ui->le_currentCount->setText(QString::number(getElectrode_currentCount()));
    set.endGroup();
}

void ElectrodeSetWidget::setPreCount(int preCount)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(ELECTRODE_GROUP);
    set.setValue(ELECTRODE_PRECOUNT,preCount);
    set.setValue(ELECTRODE_REMAINCOUNT,preCount);
    set.setValue(ELECT_CURRENTCOUNT,0);
    set.endGroup();
}
