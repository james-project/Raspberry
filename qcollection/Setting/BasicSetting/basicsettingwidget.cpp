#include "basicsettingwidget.h"
#include "common.h"
#include <QDebug>
#include "Dialog/mymessagedialog.h"
#include "electrodesetwidget.h"
//#include "SysSetting/syssettingwidget.h"


const QString BasicSettingWidget::GROUP_NAME = "basicsetting";
const QString BasicSettingWidget::Plastic_NAME = "plasticsetting";
const QString BasicSettingWidget::WORK_MODE_SETTING = "work_mode";
const QString BasicSettingWidget::Plastic_SETTING = "Plastic_setting";
QList<SettingData> BasicSettingWidget::mWorkModeMenus;
QList<SettingData> BasicSettingWidget::plasicModeMenus;



BasicSettingWidget::BasicSettingWidget(QWidget *parent) :
    SubSettingWidget(parent)
{
    if(mWorkModeMenus.length()==0)
    {
        mWorkModeMenus<<tr("点焊机")
                   <<tr("压塑机")
                   <<tr("冲压机");
    }
    if(plasicModeMenus.length()==0)
    {
        plasicModeMenus<<tr("压塑机3#")
                   <<tr("压塑机4/5#")
                   <<tr("压塑机6#");
    }

    QList<SubSettingData> submenus;
    submenus
//            <<SubSettingData(tr("User Information"),NULL,getCompanyName,setCompany)
//            <<SubSettingData(tr("Scale ID"),NULL,getScaleName,setScaleName)
              <<SubSettingData(tr("Work Mode"),NULL,getOprtModeStr,setOprtMode)
              <<SubSettingData(tr("Plastic setting"),NULL,getOprtPlasticStr,setOprtPlastic)
              <<SubSettingData(tr("电极次数"),NULL,getElectrodeWidgetMode,setElectrodeWidgetMode)  ;//TODO 20180804
    mGroupStr = GROUP_NAME;
    mPlasticStr = Plastic_NAME;
    SubSettingModel *model = new SubSettingModel(submenus,this);
    mListview->setModel(model);
    QModelIndex index = model->index(0);
    mListview->setCurrentIndex(index);
}

void BasicSettingWidget::selectSubItem(const QModelIndex &index) //MARK1222
{
    QSettings mSet(DIR_SETTINGS,QSettings::IniFormat);

    if(mSettingStr == WORK_MODE_SETTING){
//        mSet.beginGroup(mGroupStr);
        int sel =index.row();
        if(sel!=getOprtMode())
        {
            MyMessageDialog *mDialog = new MyMessageDialog(tr("Do you want to restart Now?"),tr("Warning"));
            if(mDialog->exec()==QDialog::Accepted)
            {
                SubSettingWidget::selectSubItem(index);
//                    SysSettingWidget::restart();
                TSystem2("sudo reboot");
            }
        }
        mSet.endGroup();
    }
    if(mSettingStr == Plastic_SETTING){
//        mSet.beginGroup(mGroupStr);
        int sel =index.row();
        if(sel!=getOprtPlastic())
        {
            SubSettingWidget::selectSubItem1(index);
        }
        mSet.endGroup();
    }
}

int BasicSettingWidget::getOprtMode()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(GROUP_NAME);
    int i = set.value(WORK_MODE_SETTING,BasicSettingWidget::MASHWELDER_MODE).toInt();
    set.endGroup();
    if (i > BasicSettingWidget::PUNCHING_MODE || i < 0) {
        i = BasicSettingWidget::MASHWELDER_MODE;
    }
    return i;
}

/*Add Plastic setting----zhanglong*/
int BasicSettingWidget::getOprtPlastic()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(Plastic_NAME);
    int i = set.value(Plastic_SETTING,BasicSettingWidget::Plastic_3).toInt();
    set.endGroup();
    if (i > BasicSettingWidget::Plastic_6 || i < 0) {
        i = BasicSettingWidget::Plastic_3;
    }
    return i;
}

void BasicSettingWidget::setOprtMode(BasicSettingWidget::OprtMode mode)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(GROUP_NAME);
    set.setValue(WORK_MODE_SETTING,mode);
    set.endGroup();
}

bool  BasicSettingWidget::setOprtMode(QListView *lv)
{
    mSettingStr = WORK_MODE_SETTING;
    setSubListView(lv,mWorkModeMenus,GROUP_NAME);
    return true;
}

/*Add Plastic setting----zhanglong*/
void BasicSettingWidget::setOprtPlastic(BasicSettingWidget::OprtMode mode)
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(Plastic_NAME);
    set.setValue(Plastic_SETTING,mode);
    set.endGroup();
}

bool  BasicSettingWidget::setOprtPlastic(QListView *lv)
{
    mSettingStr = Plastic_SETTING;
    setSubListView(lv,plasicModeMenus,Plastic_NAME);
    return true;
}

QString BasicSettingWidget::getOprtModeStr()
{
    QString name =  mWorkModeMenus.at(getOprtMode()).m_name;
    return name;
}
/*Add Plastic setting----zhanglong*/
QString BasicSettingWidget::getOprtPlasticStr()
{
    QString name =  plasicModeMenus.at(getOprtPlastic()).m_name;
    return name;
}

bool BasicSettingWidget::setElectrodeWidgetMode(QListView *l)
{
    qDebug()<<"ComSettingWidget::serverSettings-----------";
    BasicSettingWidget *sys = (BasicSettingWidget *)(l->parentWidget());
    ElectrodeSetWidget * w = new ElectrodeSetWidget(sys);
    sys->mSubWidget = w;
    sys->layout()->addWidget(w);
    return true;
}

QString BasicSettingWidget::getElectrodeWidgetMode()
{
    int precount = ElectrodeSetWidget::getElectrode_preCount();
    return QString::number(precount);
}

