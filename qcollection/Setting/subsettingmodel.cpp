#include "subsettingmodel.h"
#include <QSettings>
#include "common.h"

SubSettingModel::SubSettingModel(QObject *parent) :
    QAbstractListModel(parent)
{

    dataList = new  QList<SubSettingData> ();
}

SubSettingModel::SubSettingModel(QList<SubSettingData> &list,QObject *parent)
{
    Q_UNUSED(parent);
    dataList = new  QList<SubSettingData>(list);
}

SubSettingModel::~SubSettingModel()
{
    delete dataList;
}

int SubSettingModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return dataList->count();
}

void SubSettingModel::addItem(const SubSettingData &set)
{
    beginResetModel();
    dataList->append(set);
    endResetModel();
}

void SubSettingModel::insertItems(int i,QList<SubSettingData> *list)
{
    beginResetModel();
    for(int pos=0;pos<list->size();pos++)
    {
        dataList->insert(i+pos,list->at(pos));
    }
    endResetModel();
}

void SubSettingModel::removeItems(QList<SubSettingData> *list)
{
    beginResetModel();
    int t = list->size();
    for(int pos=0;pos<t;pos++)
    {
        dataList->removeOne(list->at(pos));
    }
    endResetModel();
}

void SubSettingModel::removeItemAt(int i)
{
    beginResetModel();
    dataList->removeAt(i);
    endResetModel();
}

void SubSettingModel::removeItem(const SubSettingData t)
{
    beginResetModel();
    dataList->removeOne(t);
    endResetModel();
}

void SubSettingModel::removeLast()
{
    beginResetModel();
    dataList->removeLast();
    endResetModel();
}

bool SubSettingModel::isDisable(int pos) const
{
//    int infect =  dataList->at(pos).mInfectApprov;

//    if(infect==0)
//         return false;

//    //QSettings set(DIR_SETTINGS,QSettings::IniFormat);
//    //int approval = set.value(WgtSettingWidget::GROUP_NAME +"/"+WgtSettingWidget::APPROVAL_SETTING_NAME).toInt();

//    //bool approval = TScaleNew::getScale()->approval();
//    Scale::ApprovalType approval = TScaleNew::getScale()->getApproval();

//    if(approval==Scale::APPROVAL_OIML)
//    {
//        if((infect&SubSettingData::OIML)!=0) //NETP 限制不能修改
//            return true;
//    }


//    if(approval==Scale::APPROVAL_NTEP)
//    {

//        if((infect&SubSettingData::NTEP)!=0) //NETP 限制不能修改
//            return true;
//    }
//    if(approval==Scale::APPROVAL_SRI_LANKAN)
//    {
//        qDebug()<<"------------------"<<infect<<(infect&SubSettingData::SRI_LANKAN);

//        if((infect&SubSettingData::SRI_LANKAN)!=0) // 限制不能修改
//            return true;
//    }
//    if(HavePressCalKey==false && approval!=0 && (infect & SubSettingData::CALSW_PRO)) //校正开关保护
//    {
//        qDebug("nocak");
//        return true;
//    }

//    if(infect&SubSettingData::CHN_NO_CHANGE&&language==SysSettingWidget::LANGUAGE_CH_JIANTI)
//    {
//        return true;
//    }

//    int x = (infect&SubSettingData::PERMISSION)>>6;
//    if(LoginDialog::permission()<x)
//        return true;

    return false;
}

int SubSettingModel::mCurrRow = 0;
QVariant SubSettingModel::data(const QModelIndex &index, int role) const
{

    int pos = index.row();
    if(!index.isValid() || pos > dataList->count() )
    {
        return QVariant();
    }

    switch(role)
    {
    case Qt::DisplayRole:
        return dataList->at(pos).mName;
    case Qt::DecorationRole:
        return dataList->at(pos).mIcon;
    case ROLE_SETTING:
         if(dataList->at(pos).mGetSetting!=0)
         {
            mCurrRow = pos;
            return dataList->at(pos).mGetSetting();
         }
         else
            return QVariant();
    case ROLE_INFECTAPPROVA:
        return  dataList->at(pos).mInfectApprov;
    case ROLE_CALLBACK:
        mCurrRow = pos;
        return (int)(dataList->at(pos).mCallBack);
    case ROLE_ICON:

        if(dataList->at(pos).mGetIcon!=0)
            return dataList->at(pos).mGetIcon();
        else
            return ":/res/setting/arrows_icon.png";
    case ROLE_ISDISABLE:
        return isDisable(pos);
        break;
    case ROLE_NEEDPW:
        if((dataList->at(pos).mInfectApprov&SubSettingData::PW_PRO)!=0)
        {
            return true;
        }
        else
        {
            return false;
        }
    default:
        return QVariant();
    }
}



