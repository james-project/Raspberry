#include "setting.h"
#include "ui_setting.h"
#include "mainwindow.h"
#include "settinglistmodel.h"
#include "mainsettingdelegate.h"
#include "subsettingmodel.h"
#include "subsettingdelegate.h"
#include "subsettingwidget.h"
#include "subsubsettingdelegate.h"
#include "common.h"
#include "Dialog/passworddialog.h"

#include <QList>
#include <QHash>
#include <QDebug>


Setting::Setting(QWidget *parent,QLayout *layout):
    SubWidget(parent,layout),
    ui(new Ui::Setting)
{
    ui->setupUi(this);
//    HavePressCalKey = 0;
    QListView *lv = ui->mainSettingList;

    QList<SettingData> menus;
    menus   << SettingData(tr("Basic Information" ),tr(":/res/setting/basic-information.png"))
            << SettingData(tr("Communication")     ,tr(":/res/setting/communication.png"));
    SettingListModel *model = new SettingListModel(menus,this);
    lv->setModel(model);
    MainSettingDelegate *delegate = new MainSettingDelegate(this);
    lv->setItemDelegate(delegate);
    QModelIndex index = model->index(0);
    lv->setCurrentIndex(index);
    lv->setFocus();
    connect(lv,SIGNAL(clicked(QModelIndex)),this,SLOT(showStackWidget(QModelIndex)));
    showStackWidget(index);
}


void Setting::showStackWidget(const QModelIndex &index)
{
    ui->stackedWidget->setCurrentIndex(index.row());
    SubSettingWidget *w = (SubSettingWidget*)(ui->stackedWidget->currentWidget());
    QString str = index.data(Qt::DisplayRole).toString();
    w->setSettingName(str);
    w->resetSubSettingWigget();
}

void Setting::keyPressEvent(QKeyEvent *keyEvent)
{
    int key = keyEvent->key();

    switch(key)
    {
    case Qt::Key_Enter:
    case Qt::Key_Return:
         {
            int item=ui->mainSettingList->currentIndex().row();
            showStackWidget(ui->mainSettingList->currentIndex());
            item=ui->mainSettingList->currentIndex().row();

            keyEvent->accept();
            return;
         }
        break;

      default:
             keyEvent->ignore();
        break;
    }

    SubWidget::keyPressEvent(keyEvent);
}

Setting::~Setting()
{
    delete ui;
}

void Setting::on_pushButton_clicked()
{
    this->quit();
}
