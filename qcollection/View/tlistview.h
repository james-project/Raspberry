#ifndef TLISTVIEW_H
#define TLISTVIEW_H

#include <QListView>

class TListView : public QListView
{
    Q_OBJECT
public:
    explicit TListView(QWidget *parent = 0);
    bool isOnBottom();
    bool isOnTop();
    int pages();
    int currentPage();
    void setBarRange(int min, int max);
    void showEvent(QShowEvent *e);
    virtual QModelIndexList selectedIndexes() const;

signals:
    void longPress(const QModelIndex &index);
    void singleclicked(const QModelIndex &index);
    void douleclicked(const QModelIndex &index);
public slots:
    void pageUp();
    void pageDown();
private slots:
    void timeOutProcess();
private:
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    int mPressPosY;
    int mOffsetY;
    int mScrollValue;
    QScrollBar *mBar;
    bool isDown;
    QTimer *mTimer;
    int pressTime;
    bool mIsDoubleClick;
};

#endif // TLISTVIEW_H
