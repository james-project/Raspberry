#ifndef INPUTDIALOG_H
#define INPUTDIALOG_H

#include <QDialog>
#include "common.h"
#include "Dialog/fullscreenmodaldlg.h"


namespace Ui {
class inputDialog;
}

class inputDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit inputDialog(QWidget *parent = 0,QString title = "Dialog",bool isfloatMode = false,int inputdeci = 0,int inputmax=99,int inputmin=0,bool fullScreenMode=false);// 增加最大值和最小值输入范围
    ~inputDialog();
    void hideSome();
    void setFullScreenShow();
private slots:
     void acceptSampleInput(bool);
     void delayTimer_timeoutSlot();
     void deleteFullDialog_ByHeadMan();
     void deleteSecondFullDialog_ByQcMan();

signals:
     void inputValue(int);
     void inputValue(float);
     void exitInputdialog();
     void userPermission(int permission);
     void closeFullDialog_ByHeadMan();
     void closeSecondConfirmDialog_ByQcMan();
     void sig_hideBack();
private:
    Ui::inputDialog *ui;
    bool floatMode;
    int timeId;
    bool m_receive_permission_flag;
    fullScreenModalDlg *modifyConfirmDialog;
    fullScreenModalDlg *secondConfirmDialog;
    QWidget *mParent;
    QTimer *mDelayTimer;
    int m_permissionConfirmTimes;
};

#endif // INPUTDIALOG_H
