#ifndef NUMBERKEY_H
#define NUMBERKEY_H

#include <QWidget>
#include "mashwelder.h"
#include "standardparasetting.h"


namespace Ui {
class NumberKey;
}

class NumberKey : public QWidget
{
    Q_OBJECT
    
public:
    explicit NumberKey(QWidget *parent = NULL,QWidget *ed = NULL,int desc = 0);
    enum
    {
        KEYOK='\r'
    };
    ~NumberKey();
    void hideOk();
    void hideEsc();
    void hideEscOk();
    void disableDot();

    void setFlashEnable(bool dis);
    void showFlashLabel();
    void initCurrString();
    void setParmeter(QWidget *ed,int maxLen=6,int deci=0,QString s=QString(""));
    void setEditor(QWidget *ed);
    void setMaxLenth(int len);
    void hideEditorText();
    void showEditorText();
    void timerEvent(QTimerEvent *event);
    void setDeci(int deci);
    QString text();
    inline void setClearFlag(bool flag)
    {
        clearFlag = flag;
    }
    //QPushButton* btnBack;

signals:
    void numkeyClicked(QChar key);
    void quitReslut(bool ok=false);

public slots:
    void clickBtn(void);
    void slot_hideBack();
private:
    Ui::NumberKey *ui;
    bool mFristInput;
    QWidget *mEd;
    int mMaxLenth;
    int mDeci;
    int mFlash;
    int timeId;
    bool mFlasbEnable;
    QString mCeStr;
    QString mCurrStr;
    QColor mTextColor;
    QColor mBackColor;
    bool clearFlag;
};

#endif // NUMBERKEY_H
