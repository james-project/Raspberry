#include "inputdialog.h"
#include "ui_inputdialog.h"
#include <QDebug>
#include "Dialog/mymessagedialog.h"
#include "Device/mashwelder.h"
#include "Device/DeviceState/devicestate.h"
#include "../Device/mashwelder.h"
#include "../Setting/BasicSetting/basicsettingwidget.h"
#include "../Device/PlasticPresses/delaytimesetting1.h"
#include "../Device/PlasticPresses/delaytimesetting2.h"

inputDialog::inputDialog(QWidget *parent,QString title,bool isfloatMode,int inputdeci,int inputmax,int inputmin, bool fullScreenMode) :
    QDialog(parent),
    ui(new Ui::inputDialog),
    m_receive_permission_flag(false),
    modifyConfirmDialog(NULL),
    secondConfirmDialog(NULL),
    mParent(parent),
    mDelayTimer(new QTimer(0)),
    m_permissionConfirmTimes(0)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowTitleHint);
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setFixedSize(parent->width()*5/8,parent->height()*32/60);
    this->move(100,-10);//TODO 20180803 add for test
    ui->label_text->hide();
    ui->label->setText(title);
    ui->label_2->setText(title);
    ui->inputKey->setParmeter(ui->label_value,6,inputdeci,"0");
    ui->le_inputmax->setText(QString::number(inputmax));
    ui->le_inputmin->setText(QString::number(inputmin));
    ui->le_inputmax->setReadOnly(true);
    ui->le_inputmin->setReadOnly(true);
    floatMode = isfloatMode;
    if(floatMode)
    {
        ui->label_value->setText(toString(inputvalue_float,inputdeci));
    }
    else
    {
        ui->label_value->setText(QString::number(inputvalue_int));
    }
    ui->inputKey->setEditor(ui->label_value);
    ui->inputKey->setMaxLenth(8);
    disconnect(ui->inputKey,0,0,0);
    connect(ui->inputKey,SIGNAL(quitReslut(bool)),this,SLOT(acceptSampleInput(bool)));
    connect(this,SIGNAL(sig_hideBack()),ui->inputKey,SLOT(slot_hideBack()));

    if( title == "请输入不良品")
    {
        emit sig_hideBack();
    }
    ui->label_value->clear();
}
void inputDialog::acceptSampleInput(bool accept)
{
    if(accept==1)
    {
        MashWelder::_inputVal = ui->label_value->text();
        if(floatMode)
        {
            inputvalue_float = ui->label_value->text().toFloat();
            emit inputValue(inputvalue_float);
        }
        else
        {
            inputvalue_int= ui->label_value->text().toInt();
            if(ui->le_inputmax->isVisible() || ui->le_inputmin->isVisible())
            {
                if(ui->label_value->text().toInt() > ui->le_inputmax->text().toInt() ||\
                        ui->label_value->text().toInt() < ui->le_inputmin->text().toInt()     )
                {
                    QString tips = "输入值超出范围！";
                    MyMessageDialog *dialog = new MyMessageDialog(tips,tr("错误信息"),0,tr("否"),tr("确定"));
                    dialog->hideCancelBtn();
                    if(dialog->exec() == QDialog::Accepted)
                    {
                    }
                }
                else
                {
                    emit inputValue(inputvalue_int);
                }
            }
            else
            {
                emit inputValue(inputvalue_int);
            }
        }
    }
    else
    {
        inputvalue_float = 0;
        inputvalue_int = 0;
    }
    close();
    emit exitInputdialog();
}

void inputDialog::delayTimer_timeoutSlot()
{
    if (modifyConfirmDialog)
    {
        modifyConfirmDialog->close();
        delete modifyConfirmDialog;
        modifyConfirmDialog = NULL;
        emit exitInputdialog();
    }
}
void inputDialog::deleteFullDialog_ByHeadMan()
{
    if (modifyConfirmDialog)
    {
        modifyConfirmDialog->close();
        delete modifyConfirmDialog;
        modifyConfirmDialog = NULL;

        if(  BasicSettingWidget::PLASTIC_MODE == BasicSettingWidget::getOprtMode())
        {
            return;
        }

        if (secondConfirmDialog == NULL)
        {
            secondConfirmDialog = new fullScreenModalDlg(QString("\n请QC刷工卡确认！"),mParent,false);
            secondConfirmDialog->setAttribute(Qt::WA_DeleteOnClose);
            secondConfirmDialog->setFixedSize(mParent->width(),mParent->height());
            secondConfirmDialog->show();
            DeviceState::getObj()->setDeviceStation(DeviceState::QC_CHECK_STATION);
            this->setFocus();
            connect(this,SIGNAL(closeSecondConfirmDialog_ByQcMan()),this,SLOT(deleteSecondFullDialog_ByQcMan()));
        }
    }
}

void inputDialog::deleteSecondFullDialog_ByQcMan()
{
    if (secondConfirmDialog)
    {
        secondConfirmDialog->close();
        delete secondConfirmDialog;
        secondConfirmDialog = NULL;
        /*----zhanglong*/
        MashWelder::m_systemStatus &= 0xbfff;
    }
}

inputDialog::~inputDialog()
{
    qDebug()<<"inputDialog-del";
    delete ui;
}

void inputDialog::hideSome()
{
    ui->label->hide();
    ui->frame_limit->hide();
}

void inputDialog::setFullScreenShow()
{
    if(StandardParaSetting::_bQcPass == true && StandardParaSetting::_bHeadManPass == true )
           return;

    if(  BasicSettingWidget::PLASTIC_MODE == BasicSettingWidget::getOprtMode())
    {
        return;
    }

    if (modifyConfirmDialog == NULL)
    {
        modifyConfirmDialog= new fullScreenModalDlg(QString("\n请组长刷工卡确认！"),mParent,false);
        modifyConfirmDialog->setAttribute(Qt::WA_DeleteOnClose);
        modifyConfirmDialog->setFixedSize(mParent->width(),mParent->height());
        modifyConfirmDialog->show();
        /*----zhanglong*/
        MashWelder::m_systemStatus |= 0x4000;
        this->setFocus();
        DeviceState::getObj()->setDeviceStation(DeviceState::FIRST_CHECK_STATION);
        connect(this,SIGNAL(closeFullDialog_ByHeadMan()),this,SLOT(deleteFullDialog_ByHeadMan()));
        mDelayTimer->start(1000*60*30);
        connect(mDelayTimer,SIGNAL(timeout()),this,SLOT(delayTimer_timeoutSlot()));
    }
}
