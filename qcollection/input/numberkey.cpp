#include "numberkey.h"
#include "ui_numberkey.h"
#include <QLineEdit>
#include <QLabel>
#include "common.h"
#include <QMessageBox>
#include "inputdialog.h"

NumberKey::NumberKey(QWidget *parent,QWidget *ed,int deci) :
    QWidget(parent),
    ui(new Ui::NumberKey)
{
    ui->setupUi(this);
    mFristInput = true;
    mMaxLenth = 9999;
    mFlash = 0;

    mFlasbEnable = true;
    clearFlag = false;

    ui->btn_0->setProperty("key",QChar('0'));
    ui->btn_1->setProperty("key",'1');
    ui->btn_2->setProperty("key",'2');
    ui->btn_3->setProperty("key",'3');
    ui->btn_4->setProperty("key",'4');
    ui->btn_5->setProperty("key",'5');
    ui->btn_6->setProperty("key",'6');
    ui->btn_7->setProperty("key",'7');
    ui->btn_8->setProperty("key",'8');
    ui->btn_9->setProperty("key",'9');
    ui->btn_dot->setProperty("key",'.');
    ui->btn_ce->setProperty("key",'\b');
    ui->btn_ok->setProperty("key",'\r');
    ui->btn_esc->setProperty("key",'\x1b');
    mEd = NULL;
    mBackColor = Qt::white;//mEd->palette().brush(QPalette::Background).color();
    mTextColor = Qt::black;//QColor::black();//mEd->palette().brush(QPalette::Text).color();
    setEditor(ed);
    mDeci = deci;
    //ui->btn_enter->setProperty("key",'\r');


    connect(ui->btn_0,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_1,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_2,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_3,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_4,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_5,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_6,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_7,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_8,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_9,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_dot,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_ce,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_esc,SIGNAL(clicked()),this,SLOT(clickBtn()));
    connect(ui->btn_ok,SIGNAL(clicked()),this,SLOT(clickBtn()));

    timeId = startTimer(200);
}


ulong expTab[]={1,10,100,1000,10000,100000,1000000,10000000,100000000};

QString toString(float d, int deci, int max_n)
{
    QString str;
    while(d>expTab[max_n-deci])
    {
        deci--;
        if(deci<0)
            return "------";
    }
    char fmt[]="%.2f";
    fmt[2] = '0' +deci;
    str.sprintf(fmt,d);
    return str;
}


void NumberKey::setParmeter(QWidget *ed,int maxLen,int deci,QString s)
{
    setEditor(ed);
    mMaxLenth =  maxLen;
    mDeci = deci;
    mCeStr = s;
}

QString NumberKey::text()
{

    if(mEd)
    {
        if(mEd->inherits("QLineEdit"))
        {
            return ((QLineEdit*)mEd)->text();
        }
        else if(mEd->inherits("QLabel"))
        {
            return ((QLabel*)mEd)->text();
        }
    }
    else
    {
        return mCurrStr;
    }
}

void NumberKey::setEditor(QWidget *ed)
{
    if(mEd!=NULL)
       showEditorText();
    if(mEd!=ed)
    {
        mEd = ed;
        mFristInput = true;
        if(mEd!=0)
        {
            mCurrStr = text();
            showEditorText();

        }
    }
    if(mEd!=NULL)
    {
        mBackColor = mEd->palette().color(QPalette::Window);
        mTextColor = mEd->palette().color(QPalette::Text);

    }
}

void NumberKey::hideEditorText()
{

    if(mEd->inherits("QLabel"))
    {
        QPalette pal    = mEd->palette();
        pal.setColor(QPalette::WindowText,mBackColor);
        mEd->setPalette(pal);

    }
    else if(mEd->inherits("QLineEdit"))
    {
        QPalette pal    = mEd->palette();
        pal.setColor(QPalette::Text,mBackColor);
        mEd->setPalette(pal);
    }

}

void NumberKey::showEditorText()
{
    if(mEd->inherits("QLabel"))
    {
        QPalette pal    = mEd->palette();
        pal.setColor(QPalette::WindowText,mTextColor);
        mEd->setPalette(pal);
    }
    else if(mEd->inherits("QLineEdit"))
    {
        QPalette pal    = mEd->palette();
        pal.setColor(QPalette::Text,mTextColor);
        mEd->setPalette(pal);
    }
}


void NumberKey::setMaxLenth(int len)
{
    mMaxLenth =  len;
}

void NumberKey::setDeci(int deci)
{
    mDeci = deci;
}

void NumberKey::hideOk()
{
    ui->btn_ok->hide();
}
void NumberKey::hideEsc()
{
    ui->btn_esc->hide();
}
void NumberKey::hideEscOk()
{
    ui->btn_ok->hide();
    ui->btn_esc->hide();
}

void NumberKey::disableDot()
{
    ui->btn_dot->setText("");
    ui->btn_dot->setDisabled(true);
}



void NumberKey::setFlashEnable(bool flash)
{
    mFlasbEnable = flash;
}

void NumberKey::showFlashLabel()
{
    if(mFlasbEnable)
    {
        if(mEd)
        {

            if(isVisible())
            {
                mFlash++;
                if(mFlash<2)
                {
                    hideEditorText();
                }
                else if(mFlash==2)
                {
                    showEditorText();
                }
                else if(mFlash>3)
                {
                    mFlash = 0;
                }
            }
            else
            {
                showEditorText();
                //mEd = 0;
            }
        }
        else
        {

        }
    }
}

void NumberKey::timerEvent(QTimerEvent *event)
{
    if(event->timerId()==timeId)
        showFlashLabel();
}



NumberKey::~NumberKey()
{
    delete ui;
}

void NumberKey::clickBtn()
{
     QPushButton *button = qobject_cast<QPushButton *>(sender());
     QChar ch;
     if (button)
     {
        ch = button->property("key").toChar();
        numkeyClicked(ch);
     }
     if(mEd)
     {
         if(mEd->inherits("QLineEdit"))
         {
             QLineEdit *le = qobject_cast<QLineEdit *>(mEd);
             if(ch==QChar('\b'))
             {
                 if(mCeStr==QString(""))
                 {
                     if(clearFlag)
                     {
                         le->setText("");
                     }
                     else
                     {
                         le->setText(toString(0,mDeci));
                     }

                 }
                 else
                 {
                     le->setText(mCeStr);
                 }

                 mFristInput = true;
             }
             else if(ch == QChar('\r'))
             {
                 mCurrStr = text();
                 showEditorText();
                 mEd = 0;
                 quitReslut(true);
                 mFristInput = true;
             }
             else if(ch== QChar('\x1b'))
             {
                 showEditorText();
                 mEd = 0;
                 quitReslut(false);
                 mFristInput = true;
             }
             else if(ch==QChar('.'))
             {
                 if(!le->text().contains('.') && mDeci!=0)
                 {
                     if(mFristInput)
                     {

                         mFristInput = 0;
                         le->setText("0.");
                     }
                     else
                     {
                         if(le->text().length()<mMaxLenth+1)
                            le->insert(".");
                     }
                 }
                 else
                 {

                 }
             }
             else
             {                 
                 if(clearFlag)
                 {
                     mFristInput = false;
                 }
                 if(mFristInput)
                 {
                     if(ch!=QChar('0'))
                     {
                        mFristInput = false;
                     }
                     le->setText(ch);
                 }
                 else
                 {
                     QString str= le->text();
                     int l = str.length();
                     if(l<mMaxLenth ||(str.contains(".")&&l<mMaxLenth+1))
                     {
                         int index = str.indexOf('.');

                         if(index==-1 || mDeci ==0 || l-index-1<mDeci)
                                le->insert(ch);
                     }

                 }
              }
         }
         else if(mEd->inherits("QLabel"))
         {
             QLabel *le = qobject_cast<QLabel *>(mEd);
             if(ch==QChar('\b'))
             {
                 if(mCeStr==QString(""))
                 {
                    le->setText(toString(0,mDeci));
                 }
                 else
                 {
                     le->setText(mCeStr);
                 }
                 mFristInput = true;
             }
             else if(ch == QChar('\r'))
             {
                 mCurrStr = text();
                 showEditorText();
                 mEd = NULL;
                 quitReslut(true);
                 mFristInput = true;
             }
             else if(ch== QChar('\x1b'))
             {

                 showEditorText();
                 mEd = NULL;
                 quitReslut(false);
                 mFristInput = true;
             }
             else if(ch==QChar('.'))
             {
                 if(!le->text().contains('.') && mDeci!=0)
                 {
                     if(mFristInput)
                     {
                         mFristInput = 0;
                         le->setText("0.");
                     }
                     else
                     {
                         QString str= le->text();
                         if(str.length()<mMaxLenth+1)
                            le->setText(le->text() + '.');

                     }
                 }
             }
             else
             {
                 if(mFristInput)
                 {
                     if(ch!=QChar('0'))
                     {
                        mFristInput = false;
                     }
                     le->setText(ch);
                 }
                 else
                 {
                     QString str= le->text();
                     int l = str.length();
                     if(l<mMaxLenth || (str.contains(".")&&l<mMaxLenth+1))
                     {
                         int index = str.indexOf('.');
                         if(index==-1 || mDeci ==0 || l-index-1<mDeci)
                                le->setText(str + ch);
                     }
                 }
              }
         }

     }
}

void NumberKey::slot_hideBack()
{
    ui->btn_esc->setVisible(false);
}



