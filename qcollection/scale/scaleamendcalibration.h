#ifndef SCALEAMENDCALIBRATION_H
#define SCALEAMENDCALIBRATION_H

#include <QObject>
#include <QThread>
#define SAMPLE_MAX_SIZE 20


class ScaleAmendCalibration : public QThread
{
    Q_OBJECT
public:
    explicit ScaleAmendCalibration(QThread *parent = 0);
    static ScaleAmendCalibration *getobj();
    void run();
    void filter(float &adoutValue,float &sourceWeight);
    float LowPassfilter(float filterScale, float newWeight);
    float MovingAverageFilter(int SampingNum, float SampingWeight);
    inline void setFilterScale(float filterScale){
        m_filterCoef = filterScale;
    }
    inline float getFilterScale() {
        return m_filterCoef;
    }

    inline void setReRunFlag(bool flag) {
        m_firstWeightFlag = flag;
        m_unitweight = 0.0;
        m_calibrationCount = 0;
        m_calibrationCountPrevious=0;
        m_latestWeight_temp = 0.0;
        m_PreviousWeight_CNTChange=0.0;
        m_PreviousWeight_CNTChange=0.0;
        MovingAverageWeight =0.0;
        m_previousWeight = 0.0;


    }

    inline bool getRRunFlag() {
        return m_firstWeightFlag;
    }

signals:
    void scaleConversionValue(float);
    void sendCalibrationCount(int,float);

public slots:    
    void getScaleSourceValue(float scaleSourceValue, float unitWeight);
    void getScaleUw(float);
private:
    static ScaleAmendCalibration *m_scaleAmendCalibration;
    float m_scaleSourceValue;
    float value_buff[SAMPLE_MAX_SIZE];
    float m_scaleConversionValue;
    float m_unitweight;
    float m_FirstUnitweight;
    int m_calibrationCount;
    int m_calibrationCountPrevious;
    float m_latestWeight_temp;
    float m_PreviousWeight_CNTChange;
    float MovingAverageWeight;
    float m_conversionWeight;//滤波后的值
    float m_deltaWeight;
    float m_filterCoef;
    float m_previousWeight;
    int m_firstWeight_Recalculate_count;
public :
    static  bool m_firstWeightFlag;

};

#endif // SCALEAMENDCALIBRATION_H
