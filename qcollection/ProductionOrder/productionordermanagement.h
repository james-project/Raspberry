#ifndef PRODUCTIONORDERMANAGEMENT_H
#define PRODUCTIONORDERMANAGEMENT_H

#include <QWidget>
#include <QJsonArray>
#include <QVariant>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFrame>
#include <QSpacerItem>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QTableView>
#include <QStandardItemModel>
#include <QStyledItemDelegate>



typedef struct __ProductionVariant
{
    QString TaskId;
    QString ProductName;
    QString ProductCode;
    QString Mtype3;
    QString DeviceType;
    QString OrderCode;
    QString WorkNum;
    QString WorkTime;
    QString ModuleId;
    QString DeviceId;
    QString mbom_src;
    QString OrderStartTime;
    QString ProcessId;
    QString ToolingRatio;
}ProductionVariant;

class ProductionOrderManagement : public QWidget
{
    Q_OBJECT
public:
    explicit ProductionOrderManagement(/*ProductionVariant &productInfo,QLayout *layout,int width,int height,*/QWidget *parent=0);
    void layoutFun();
    void AddMiddleGridItem(QGridLayout *layout ,QWidget *wgt, QLayoutItem * spacerItem, int row, int column, QString showText, QFont font);
signals:
    void exitcurr();
    void postConfirmOrderSignals();

public slots:
    void enableOrderCheck_Slots();
    void testSlot();
private:
    QFrame *lineFrame;
    QLabel *lab_title;
    QLabel *lab_taskId;
    QLabel *lab_productName;
    QLabel *lab_productCode;
    QLabel *lab_mtype3;
    QLabel *lab_deviceType;
    QLabel *lab_orderCode;
    QLabel *lab_workNum;
    QLabel *lab_workTime;
    QLabel *lab_moduleId;
    QLabel *lab_deviceId;
    QLabel *lab_projId;

    QLineEdit *le_taskId;
    QLineEdit *le_productName;
    QLineEdit *le_productCode;
    QLineEdit *le_mtype3;
    QLineEdit *le_deviceType;
    QLineEdit *le_orderCode;
    QLineEdit *le_workNum;
    QLineEdit *le_workTime;
    QLineEdit *le_moduleId;
    QLineEdit *le_deviceId;
    QLineEdit *le_projId;

    QVBoxLayout *mainLayout;
    QWidget *mainWgt;
    QVBoxLayout *topLayout;
    QGridLayout *midLayout;
    QHBoxLayout *bottomLayout;
    QPushButton *btn_return;
    QPushButton *btn_orderConfirm;
    QSpacerItem *hori_spacerLeft;
    QSpacerItem *hori_spacerRight;

    QStandardItemModel *mid_standardItemModel;
    QTableView *mid_tabview;

};

class StyleDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit StyleDelegate(QObject * parent):QStyledItemDelegate(parent) {}
    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const;
};

#endif // PRODUCTIONORDERMANAGEMENT_H
