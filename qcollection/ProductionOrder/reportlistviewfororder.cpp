#include "reportlistviewfororder.h"
#include "ui_reportlistviewfororder.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QMessageBox>

ReportListViewForOrder::ReportListViewForOrder(QList<Item> &items,QString option,int shortIndex,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReportListViewForOrder)
{
    ui->setupUi(this);
    QHBoxLayout *l = ui->titleLayout;
    mItems = items;
    mModel = ProductionOrderListModel::getModel(/*OperateSettingWidget::TEMP_RECORD_CHECK,option*/);
    ui->listView->setModel(mModel);
//    mModel->changeShort(ProductionOrderListModel::_ID); //暂时处理
//    mDelegate = ProductionOrderDelegate::newDelegate(OperateSettingWidget::TEMP_RECORD_CHECK,items,this);
    mDelegate = new ProductionOrderDelegate(items,this);

    qDebug() << "\n\n\n  ~~~~~~~~~~~~~~~~~~~~~~~~` items :" << items.count();

    ui->listView->setItemDelegate(mDelegate);
    ui->listView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    for(int i=0; i<items.count();i++)
    {
        QLabel *label = new QLabel(items.at(i).mName,ui->title);
        label->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Preferred);
        label->setAlignment(Qt::AlignHCenter);
        l->addWidget(label,items.at(i).mMaxLen);

        label->setFont(QFont("DejaVu Sans",18));
        mLabelTbl.append(label);
        label->installEventFilter(this);
        if(shortIndex==i)
        {
//            mModel->changeShort(mItems.at(i).mRole);
//            if(mModel->descShort())
//                label->setText(mItems.at(i).mName+"↓");
//            else
                label->setText(mItems.at(i).mName+"↑");
        }
    }
}

ReportListViewForOrder::~ReportListViewForOrder()
{
    delete ui;
}

void ReportListViewForOrder::setReportDate(QDate date)
{

}

void ReportListViewForOrder::setReportItem(QList<Item> &items)
{
    mDelegate->setItems(items);
    QHBoxLayout *l = ui->titleLayout;

    for(int i=0;i<mLabelTbl.count();i++)
    {
        delete mLabelTbl.at(i);
    }
    mLabelTbl.clear();

    for(int i=0; i<items.count();i++)
    {
        QLabel *label = new QLabel(items.at(i).mName,this);
        l->addWidget(label,items.at(i).mMaxLen);
        mLabelTbl.append(label);
    }
    ui->listView->repaint();
}

TListView *ReportListViewForOrder::listView()
{
    return ui->listView;
}

bool ReportListViewForOrder::eventFilter(QObject *target, QEvent *event)
{
    if(event->type()==QEvent::MouseButtonPress)
    {
        QLabel *lab = (QLabel*)target;

        int i = mLabelTbl.indexOf(lab);

//        if(mModel->changeShort(mItems.at(i).mRole))
//        {
//            for(int j=0;j<mItems.count();j++)
//            {
//               mLabelTbl[j]->setText(mItems.at(j).mName);
//            }
//            if(mModel->descShort())
//                lab->setText(mItems.at(i).mName+"↓");
//            else
//                lab->setText(mItems.at(i).mName+"↑");
//        }
    }
    return QWidget::eventFilter(target,event);
}
