#include "productionordermanagement.h"
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <QSizePolicy>
#include <QHeaderView>
#include <QScrollBar>
#include "DataBase/databasequery.h"
#include "DataBase/dbtab_productionorder.h"


ProductionOrderManagement::ProductionOrderManagement(/*ProductionVariant &productInfo,QLayout *layout,int width,int height,*/QWidget *parent) :
    QWidget(parent)
{

    ProductionVariant productInfo;
    QLayout *layout;
    int width;
    int height;

    lineFrame = new QFrame;
    lineFrame->setFrameShape(QFrame::HLine);
    lineFrame->setFrameShadow(QFrame::Plain);
    lab_title = new QLabel;
    lab_title->setText("生产订单信息");

    mainLayout = new QVBoxLayout;
    mainWgt = new QWidget(this);
    topLayout = new QVBoxLayout;
    midLayout = new QGridLayout;
    bottomLayout = new QHBoxLayout;

    hori_spacerLeft = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Preferred);
    hori_spacerRight = new QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Preferred);

    btn_return = new QPushButton;
    btn_return->setText("返回");
    btn_return->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

    btn_orderConfirm = new QPushButton(tr("订单确认"));
    btn_orderConfirm->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

    QFont f("DejaVu Sans",20);
    lab_title->setFont(f);
    btn_return->setFont(f);
    btn_return->setStyleSheet("background-color: rgb(146, 208, 80);");
    btn_orderConfirm->setFont(f);
    btn_orderConfirm->setStyleSheet("background-color: rgb(146, 208, 80);");

    /*lab_taskId = new QLabel;
    lab_productName = new QLabel;
    lab_productCode = new QLabel;
    lab_mtype3 = new QLabel;
    lab_deviceType = new QLabel;
    lab_orderCode = new QLabel;
    lab_workNum = new QLabel;
    lab_workTime = new QLabel;
    lab_moduleId = new QLabel;
    lab_deviceId = new QLabel;
    lab_projId = new QLabel;

    le_taskId = new QLineEdit;
    le_productName = new QLineEdit;
    le_productCode = new QLineEdit;
    le_mtype3 = new QLineEdit;
    le_deviceType = new QLineEdit;
    le_orderCode = new QLineEdit;
    le_workNum = new QLineEdit;
    le_workTime = new QLineEdit;
    le_moduleId = new QLineEdit;
    le_deviceId = new QLineEdit;
    le_projId = new QLineEdit;

    AddMiddleGridItem(midLayout,NULL, hori_spacerLeft,0, 0, "", f);
    AddMiddleGridItem(midLayout,lab_taskId, NULL,0, 1, "Task ID:", f);
    AddMiddleGridItem(midLayout,le_taskId, NULL,0, 2, "", f);
    AddMiddleGridItem(midLayout,NULL, hori_spacerRight,0, 3, "", f);
    AddMiddleGridItem(midLayout,lab_productName, NULL,1, 1, "Product Name:", f);
    AddMiddleGridItem(midLayout,le_productName, NULL,1, 2, "", f);
    AddMiddleGridItem(midLayout,lab_productCode, NULL,2, 1, "Product Code:", f);
    AddMiddleGridItem(midLayout,le_productCode, NULL,2, 2, "", f);
    AddMiddleGridItem(midLayout,lab_mtype3, NULL,3, 1, "Mtype3", f);
    AddMiddleGridItem(midLayout,le_mtype3, NULL,3, 2, "", f);
    AddMiddleGridItem(midLayout,lab_deviceType, NULL,4, 1, "Device Type:", f);
    AddMiddleGridItem(midLayout,le_deviceType, NULL,4, 2, "", f);
    AddMiddleGridItem(midLayout,lab_orderCode, NULL,5, 1, "Order Code:", f);
    AddMiddleGridItem(midLayout,le_orderCode, NULL,5, 2, "", f);
    AddMiddleGridItem(midLayout,lab_workNum, NULL, 6, 1, "Work Num:", f);
    AddMiddleGridItem(midLayout,le_workNum, NULL,6, 2, "", f);
    AddMiddleGridItem(midLayout,lab_workTime, NULL, 7, 1, "Work Time:", f);
    AddMiddleGridItem(midLayout,le_workTime, NULL,7, 2, "", f);
    AddMiddleGridItem(midLayout,lab_moduleId, NULL,8, 1, "Module ID:", f);
    AddMiddleGridItem(midLayout,le_moduleId, NULL,8 , 2, "", f);
    AddMiddleGridItem(midLayout,lab_deviceId, NULL,9, 1, "Device ID:", f);
    AddMiddleGridItem(midLayout,le_deviceId, NULL,9 , 2, "", f);
    */


    mid_standardItemModel = new QStandardItemModel(this);
    mid_tabview = new QTableView(this);
    QStringList headers;
    headers  << "TaskId" << "ProductName" << "ProductCode" << "Mtype3" << "DeviceType" << "OrderCode" << "WorkNum"
             << "WorkTime" << "ModuleId" << "DeviceId" << "MBOM";
    mid_standardItemModel->setHorizontalHeaderLabels(headers);
    mid_tabview->setModel(mid_standardItemModel);
    mid_tabview->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    mid_tabview->setColumnWidth(0,(width-50)/11);
    mid_tabview->setColumnWidth(1,(width-50)/11);
    mid_tabview->setColumnWidth(2,(width-50)/11);
    mid_tabview->setColumnWidth(3,(width-50)/11);
    mid_tabview->setColumnWidth(4,(width-50)/11);
    mid_tabview->setColumnWidth(5,(width-50)/11);
    mid_tabview->setColumnWidth(6,(width-50)/11);
    mid_tabview->setColumnWidth(7,(width-50)/11);
    mid_tabview->setColumnWidth(8,(width-50)/11);
    mid_tabview->setColumnWidth(9,(width-50)/11);
    mid_tabview->setColumnWidth(10,(width-50)/11);

    mid_tabview->setSelectionBehavior(QAbstractItemView::SelectRows);

    StyleDelegate *styled_delegate = new StyleDelegate(mid_tabview);
    mid_tabview->setItemDelegate(styled_delegate);

    DataBaseQuery query(DataBase::getDataBase());
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();

    QStringList keyList;
    keyList
            <<DBTab_ProductionOrder::_TAB_ORDER_TASK_ID
           <<DBTab_ProductionOrder::_TAB_ORDER_PRODUCT_NAME
          <<DBTab_ProductionOrder::_TAB_ORDER_PRODUCT_CODE
         <<DBTab_ProductionOrder::_TAB_ORDER_MTYPE3
        <<DBTab_ProductionOrder::_TAB_ORDER_DEVICE_TYPE
       <<DBTab_ProductionOrder::_TAB_ORDER_ORDER_CODE
      <<DBTab_ProductionOrder::_TAB_ORDER_WORK_NUM
     <<DBTab_ProductionOrder::_TAB_ORDER_WORK_TIME
    <<DBTab_ProductionOrder::_TAB_ORDER_MODULE_ID
    <<DBTab_ProductionOrder::_TAB_ORDER_DEVICE_ID
    <<DBTab_ProductionOrder::_TAB_ORDER_MBOM;
    QString option = "";
    query.selectTable(DBTab_ProductionOrder::TAB_ORDER,&keyList,option);
    QStringList list;
    list.clear();
    QString result = "";
    list = result.split(",");
    list.clear();
    int num = 0;
    if(query.first())
    {
        do
        {
            QSqlRecord rcd = query.record();
            for(int i =0;i<rcd.count();i++)
            {
                //                if(i==3)
                //                {
                //                    result = rcd.value(i).toDateTime().toString("yyyy-MM-dd hh:mm:ss");
                //                }
                //                else
                {
                    result = rcd.value(i).toString();
                }
                list.append(result);
            }
            for(int i=0;i<list.count();i++)
            {
                QString temp = list.at(i);
                mid_standardItemModel->setItem(num, i, new QStandardItem(temp));
                //设置单元格文本居中，数据设置为居中显示
                mid_standardItemModel->item(num, i)->setTextAlignment(Qt::AlignCenter);
            }
            num++;
            list.clear();
        }while(query.next());
    }
    num = 0;
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

    mid_tabview->verticalScrollBar()->setStyleSheet(
                "QScrollBar::vertical {\n"
                "border:0px solid red;\n"
                "width: 50px;\n"
                "background:white;}\n"
                "QScrollBar::handle:vertical {\n"
                "border: 0px solid gray;\n"
                "border-radius:0px;\n"
                "min-height:0px;\n"
                "margin:0px 0 0px 0;}\n"
                );

    mid_tabview->horizontalScrollBar()->setStyleSheet(
                "QScrollBar::Horizontal {\n"
                "border:0px solid red;\n"
                "height: 50px;\n"
                "background:white;}\n"
                "QScrollBar::handle:Horizontal {\n"
                "border: 0px solid gray;\n"
                "border-radius:0px;\n"
                "min-width"
                ":0px;\n"
                "margin:0px 0 0px 0;}\n"
                );
    mid_tabview->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mid_tabview->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mid_tabview->setStyleSheet("QHeaderView {\n"
                               "color: rgb(85, 0, 0);\n"
                               "font: bold 13pt;\n"
                               "background-color: rgb(170, 170, 255);\n"
                               /*设置表头空白区域背景色*/
                               "border: 0px solid rgb(144, 144, 144);\n"
                               "border:0px solid rgb(191,191,191);\n"
                               "border-left-color: rgba(255, 255, 255, 0);\n"
                               "border-top-color: rgba(255, 255, 255, 0);\n"
                               "border-radius:0px;\n"
                               "min-height:26px;\n"
                               "}\n"
                               "QTableView {\n"
                               "border:none;\n"
                               "background:white;\n"
                               "}\n"
                               "QTableView::item:selected { \n"
                               "color:white;\n"
                               "background:rgb(34,175,75);\n"
                               "}\n"
                               );

    midLayout->addWidget(mid_tabview);

    topLayout->addWidget(lineFrame,1,Qt::AlignTop);
    topLayout->addWidget(lab_title,1,Qt::AlignHCenter | Qt::AlignTop);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(midLayout);
    mainLayout->addLayout(bottomLayout);
    mainLayout->setStretch(0,3);
    mainLayout->setStretch(1,15);
    mainLayout->setStretch(2,3);
    mainLayout->setContentsMargins(0,0,0,0);

    bottomLayout->addWidget(btn_orderConfirm,1,Qt::AlignRight);
    bottomLayout->addWidget(btn_return,1,Qt::AlignRight);
    mainWgt->setLayout(mainLayout);


    mainWgt->resize(/*width*/400,/*height*/300);
    this->setFocus();
    connect(btn_return,SIGNAL(clicked(bool)),this,SIGNAL(exitcurr()));
    connect(btn_orderConfirm,SIGNAL(clicked(bool)),this,SIGNAL(postConfirmOrderSignals()));
    connect(btn_orderConfirm,SIGNAL(clicked(bool)),this,SLOT(testSlot()));//TODO for test
}

void ProductionOrderManagement::AddMiddleGridItem( QGridLayout *midlayout, QWidget *itemWgt, QLayoutItem * spacerItem, int row, int column, QString showText, QFont font)
{
    if (itemWgt && itemWgt->inherits("QLabel")) {
        qobject_cast< QLabel *>(itemWgt)->setFrameShape(QFrame::Box);
        qobject_cast< QLabel *>(itemWgt)->setFrameShadow(QFrame::Raised);
        if (itemWgt) {
            itemWgt->setFont(font);
            itemWgt->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            midlayout->addWidget(itemWgt,row,column);
            qobject_cast< QLabel *>(itemWgt)->setText(showText);
        }
    }
    else if (itemWgt && itemWgt->inherits("QLineEdit")) {

        if (itemWgt) {
            itemWgt->setFont(font);
            itemWgt->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            midlayout->addWidget(itemWgt,row,column);
            qobject_cast< QLineEdit *>(itemWgt)->setText(showText);
        }
    }
    else if (itemWgt == NULL) {
        midlayout->addItem(spacerItem,row,column);
    }
}

void ProductionOrderManagement::enableOrderCheck_Slots()
{
    btn_orderConfirm->setEnabled(true);
}

#include "DataBase/databasequery.h"
void ProductionOrderManagement::testSlot()
{
    DataBaseQuery query(DataBase::getDataBase());
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();

    query.resetDataBaseTableData(DBTab_ProductionOrder::TAB_ORDER);

    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

}


void StyleDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem view_option(option);
    if (view_option.state & QStyle::State_HasFocus) {
        view_option.state = view_option.state ^ QStyle::State_HasFocus;
    }
    QStyledItemDelegate::paint(painter, view_option, index);
}
