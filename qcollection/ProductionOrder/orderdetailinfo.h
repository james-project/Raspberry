#ifndef ORDERDETAILINFO_H
#define ORDERDETAILINFO_H

#include "Dialog/tdialog.h"
#include <QModelIndex>
#include "ProductionOrder/productionordermanagement.h"

namespace Ui {
class OrderDetailInfo;
}

class OrderDetailInfo : public TDialog
{
    Q_OBJECT

public:
    explicit OrderDetailInfo(const QModelIndex &index,QWidget *parent = 0);
    ~OrderDetailInfo();

signals:
    void confirmProductOrder_signals(ProductionVariant );
private slots:
    void on_btn_back_clicked();
    void on_btn_confirmCurrentOrder_clicked();
    void tetsssss();

private:
    Ui::OrderDetailInfo *ui;
    QModelIndex mIndex;
    ProductionVariant m_productOrderData;

};

#endif // ORDERDETAILINFO_H
