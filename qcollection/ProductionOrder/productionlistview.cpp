#include "productionlistview.h"
#include "ui_productionlistview.h"
#include "Setting/BasicSetting/basicsettingwidget.h"
#include "dataview/productionorderlistmodel.h"
#include "dataview/productionorderdelegate.h"
#include "Setting/other/datestring.h"
#include <QApplication>
#include <QDesktopWidget>
#include "Dialog/mymessagedialog.h"
#include "Device/DeviceState/devicestate.h"
#include "../mashwelder.h"

ProductionListView::ProductionListView(QWidget *parent, QLayout *layout) :
    SubWidget(parent,layout),
    ui(new Ui::ProductionListView),
    m_operatorCheckDialog(NULL)
{
    ui->setupUi(this);
    //ui->btn_confirm->setVisible(false);
    //ui->btn_del->setVisible(false);
}

ProductionListView::ProductionListView(QWidget *parent, QLayout *layout, int optMode) :
    SubWidget(parent,layout),
    ui(new Ui::ProductionListView),
    firstCheckDialog(NULL),
    m_operatorCheckDialogDelayTime(new QTimer(0)),
    m_operatorCheckDialog(NULL)
{
    ui->setupUi(this);
    QList<Item> items;
    int defaultVal;
    mOptMode= optMode;
    //ui->btn_confirm->setDisabled(true);
    this->setFocus();
    if(optMode == BasicSettingWidget::MASHWELDER_MODE)
    {
        ui->widget->hide(); //MulRecordlistModel::RECORD_NO //MARK0701
        items<<Item(tr("No"),2,ProductionOrderListModel::RECORD_NO) //TempRecordlistModel::_ID  //MARK0108 解决表清除记录后 No不从0开始的问题
            <<Item(tr("task_id"),5,ProductionOrderListModel::TASK_ID)
           <<Item(tr("product_name"),5,ProductionOrderListModel::PRODUCT_NAME)
          <<Item(tr("product_code"),3,ProductionOrderListModel::PRODUCT_CODE)
         <<Item(tr("Work_Num"),3,ProductionOrderListModel::WORK_NUM)
        <<Item(tr("Time"),8,ProductionOrderListModel::TIME);
        defaultVal = 2;
        //connect(ui->btn_del,SIGNAL(clicked()),this,SLOT(deleteCurrRecords()));
    }
    mDate = QDate::currentDate();
    QString option ;
    if(mOptMode == BasicSettingWidget::MASHWELDER_MODE)
    {
        //        option  =""; ProductionOrderListModel::createDayOption(mDate);
    }
    mView = NULL;
    if(mOptMode == BasicSettingWidget::MASHWELDER_MODE)
    {
        mView = new ReportListViewForOrder(items,option,defaultVal,this);
        ui->vListLayout->addWidget(mView);
    }
    //    if(QApplication::desktop()->locale()==QLocale::Persian)
    //    {
    //        ui->lab_day->setText(tr("Record List"));
    //        ui->lab_ymd->setText("("+DateString::dateToCurrFormat(mDate)+")");
    //    }
    //    else
    //    {
    ui->lab_day->setText(tr("Record List")+"("+DateString::dateToCurrFormat(mDate)+")");
    //    }
    if(mOptMode == BasicSettingWidget::MASHWELDER_MODE)
    {
        connect(mView->listView(),SIGNAL(clicked(QModelIndex)),this,SLOT(showInfo(QModelIndex)));
        mView->listView()->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }

    connect(ui->btn_back,              SIGNAL(longclicked()),                  this,                       SLOT(backReturn()));
}

ProductionListView::~ProductionListView()
{
    delete ui;
}


void ProductionListView::initOperatorCheckDialog()
{

     if (m_operatorCheckDialog == NULL)
     {
        m_operatorCheckDialog= new fullScreenModalDlg(QString("订单确认完成!\n请操作员刷工卡确认！"),this,false);
        m_operatorCheckDialog->setAttribute(Qt::WA_DeleteOnClose);
        m_operatorCheckDialog->show();
        DeviceState::getObj()->setDeviceStation(DeviceState::OPERATOR_WORK_STATION); //TODO 20180726 Add
    }
}

void ProductionListView::on_btn_up_clicked()
{
    mView->listView()->pageUp();
}

void ProductionListView::on_btn_down_clicked()
{
    mView->listView()->pageDown();
}

//void ProductionListView::on_btn_back_clicked()
//{
//    this->quit();
//}

void ProductionListView::deleteCurrRecords()
{
    if(ProductionOrderListModel::getModel()->rowCount()!=0)
    {
        if(MyMessageDialog::question(0, tr("Warning"),
                                     tr("Do you want to delete record?"))==MyMessageDialog::Accepted)
        {
            ProductionOrderListModel::getModel()->delItemFromRcd();
            //            emit delRcds();
        }
    }
}

//void ProductionListView::on_btn_confirm_clicked()
//{
//    //    emit postConfirmOrderSignals();
//}

void ProductionListView::enableOrderCheck_Slots()
{
    //ui->btn_confirm->setEnabled(true);
}

void ProductionListView::showOrderCheckTips()
{
    emit deleteDetailDialog();
    initOperatorCheckDialog();
    m_operatorCheckDialogDelayTime->start(60000*10);//10 minutes
    disconnect(m_operatorCheckDialogDelayTime,SIGNAL(timeout()),this,SLOT(showOperatorCheckDialog_delay()));//TODO 20180727 add this to solve the memory crashes
    connect(m_operatorCheckDialogDelayTime,SIGNAL(timeout()),this,SLOT(showOperatorCheckDialog_delay()));
    connect(this,SIGNAL(operatorCheckOK_signals()),this,SLOT(operatorCheckOk_Slots()));
}


void ProductionListView::showDeviceFirstCheckTips()
{
    QString tips = "首检权限确认通过，请确认工单！";
    static MyMessageDialog *dialog = NULL;
    if(dialog == NULL) {
        dialog = new MyMessageDialog(tips,tr("首检权限"),this,tr("否"),tr("确定"));
        dialog->hideCancelBtn();
    }
    if(dialog->exec() == QDialog::Accepted)
    {
        dialog = NULL;
        emit enableOrderConfirm();
    }
}

void ProductionListView::showInfo(const QModelIndex &index)
{

    OrderDetailInfo *dlg = new OrderDetailInfo(index,0);
    connect(dlg,SIGNAL(confirmProductOrder_signals(ProductionVariant)),this,SIGNAL(confirmCurrentOrder_FromDetaiInfo(ProductionVariant)));
    connect(this,SIGNAL(deleteDetailDialog()),dlg,SLOT(close()));
    dlg->exec();
}

void ProductionListView::operatorCheckOk_Slots()
{
    if (m_operatorCheckDialog)
    {
        m_operatorCheckDialog->close();
        m_operatorCheckDialog = NULL;

        emit materialConfirm_FromlistView();
    }
}

void ProductionListView::showOperatorCheckDialog_delay()
{
    if (m_operatorCheckDialogDelayTime) {
        if (m_operatorCheckDialogDelayTime->isActive()) {
            m_operatorCheckDialogDelayTime->stop();
        }
    }

    if (m_operatorCheckDialog) {
        m_operatorCheckDialog->close();
        delete m_operatorCheckDialog ;
        m_operatorCheckDialog = NULL;

        QString tips = "操作员刷卡超时~~,请重试～！";
        static MyMessageDialog *dialog = NULL;
        if(dialog == NULL) {
            dialog = new MyMessageDialog(tips,tr("警告"),this,tr("否"),tr("确定"));
        }

        if(dialog->exec() == QDialog::Accepted) {
            if (m_operatorCheckDialogDelayTime) {
                if (!m_operatorCheckDialogDelayTime->isActive()) {
                    m_operatorCheckDialogDelayTime->start();
                }
            }
            if (dialog) {
                dialog = NULL;
            }

            initOperatorCheckDialog();
        }
        else {
            if (m_operatorCheckDialogDelayTime) {
                if (!m_operatorCheckDialogDelayTime->isActive()) {
                    m_operatorCheckDialogDelayTime->start();
                }
            }
            if (dialog) {
                dialog = NULL;
            }
        }
    }


}

void ProductionListView::backReturn()
{
    MashWelder::_orderBegin = false;
    this->quit();
}
