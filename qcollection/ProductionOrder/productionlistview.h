#ifndef PRODUCTIONLISTVIEW_H
#define PRODUCTIONLISTVIEW_H

#include <QWidget>
#include "Setting/subwidget.h"
#include <QDate>
#include "reportlistviewfororder.h"
#include "Dialog/fullscreenmodaldlg.h"
#include "orderdetailinfo.h"
#include "ProductionOrder/productionordermanagement.h"
#include <QTimer>

namespace Ui {
class ProductionListView;
}

class ProductionListView : public SubWidget
{
    Q_OBJECT

public:
    explicit ProductionListView(QWidget *parent,QLayout *layout);
    explicit ProductionListView(QWidget *parent,QLayout *layout,int optMode  );
    ~ProductionListView();
    void initOperatorCheckDialog();

signals:
    void postConfirmOrderSignals();
    void materialConfirm_FromlistView();
    void enableOrderConfirm();
    void confirmCurrentOrder_FromDetaiInfo(ProductionVariant);
    void deleteDetailDialog();
    void operatorCheckOK_signals();

private slots:
    void on_btn_up_clicked();

    void on_btn_down_clicked();

//    void on_btn_back_clicked();
    void deleteCurrRecords();

    //void on_btn_confirm_clicked();
    void enableOrderCheck_Slots();
    void showOrderCheckTips();
    void showDeviceFirstCheckTips();
    void showInfo(const QModelIndex &index);
    void operatorCheckOk_Slots();
    void showOperatorCheckDialog_delay();
    void backReturn();


private:
    Ui::ProductionListView *ui;
    QDate mDate;
    int mOptMode;
    ReportListViewForOrder *mView;
    fullScreenModalDlg *firstCheckDialog;
    QTimer *m_operatorCheckDialogDelayTime;
    fullScreenModalDlg *m_operatorCheckDialog;

};

#endif // PRODUCTIONLISTVIEW_H
