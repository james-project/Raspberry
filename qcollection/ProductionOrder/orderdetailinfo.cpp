#include "orderdetailinfo.h"
#include "ui_orderdetailinfo.h"
#include "dataview/productionorderlistmodel.h"
#include "common.h"

OrderDetailInfo::OrderDetailInfo(const QModelIndex &index,QWidget *parent) :
    TDialog(parent),
    ui(new Ui::OrderDetailInfo),
    mIndex(index),
    m_productOrderData({"","","","","","","","","","","",""})
{
    ui->setupUi(this);
#ifndef QT_DEBUG
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
#endif
    this->setAttribute(Qt::WA_DeleteOnClose);
    this->setGeometry(WINDOW_GEOMETRY(0.7,0.7));//0.5,0.8

#ifdef QT_DEBUG
    this->setWindowFlags(Qt::FramelessWindowHint);
    //setFixedSize( 500,800);
#endif

//    mIndex = index;
    m_productOrderData.TaskId = index.data(ProductionOrderListModel::TASK_ID).toString();
    m_productOrderData.ProductName = index.data(ProductionOrderListModel::PRODUCT_NAME).toString();
    m_productOrderData.ProductCode = index.data(ProductionOrderListModel::PRODUCT_CODE).toString();
    m_productOrderData.WorkNum = index.data(ProductionOrderListModel::WORK_NUM).toString();
    m_productOrderData.OrderStartTime = index.data(ProductionOrderListModel::TIME).toDateTime().toString("yyyy-MM-dd hh:mm:ss");
    qDebug() << Q_FUNC_INFO << " m_productOrderData.OrderStartTime: " << m_productOrderData.OrderStartTime;

    ui->lab_taskId->setText(m_productOrderData.TaskId);
    ui->lab_product->setText(m_productOrderData.ProductName);
    ui->lab_productCode->setText(m_productOrderData.ProductCode);
    ui->lab_workNum->setText(m_productOrderData.WorkNum);
}

OrderDetailInfo::~OrderDetailInfo()
{
    delete ui;
}

void OrderDetailInfo::on_btn_back_clicked()
{
    this->close();
}

void OrderDetailInfo::on_btn_confirmCurrentOrder_clicked()
{
    emit confirmProductOrder_signals(m_productOrderData );
}

void OrderDetailInfo::tetsssss()
{
    qDebug() << "\n\n ~~~~~~~~~~~~~~~~~~ test ssssss";
}
