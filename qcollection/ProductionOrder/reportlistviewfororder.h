#ifndef REPORTLISTVIEWFORORDER_H
#define REPORTLISTVIEWFORORDER_H

#include <QWidget>
#include "dataview/productionorderlistmodel.h"
#include "dataview/productionorderdelegate.h"
#include "View/tlistview.h"
#include <QLabel>

namespace Ui {
class ReportListViewForOrder;
}

class ReportListViewForOrder : public QWidget
{
    Q_OBJECT

public:
    explicit ReportListViewForOrder(QList<Item> &items,QString option,int shortIndex,QWidget *parent = 0);
    ~ReportListViewForOrder();
    void setReportDate(QDate date);
    void setReportItem(QList<Item> &items);
     TListView *listView();
     virtual bool eventFilter(QObject *, QEvent *);

private:
    Ui::ReportListViewForOrder *ui;
    QList<Item> mItems;
    ProductionOrderDelegate *mDelegate;
    ProductionOrderListModel *mModel;
    QList<QLabel*> mLabelTbl;
};

#endif // REPORTLISTVIEWFORORDER_H
