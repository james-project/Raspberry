#ifndef PERSSIONDLG_H
#define PERSSIONDLG_H

#include <QDialog>

namespace Ui {
class PerssionDlg;
}

class PerssionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit PerssionDlg(QWidget *parent = 0);
    ~PerssionDlg();
protected:
    bool eventFilter(QObject *watched, QEvent *event);

private slots:
    void on_btn_ok_clicked();
    void on_btn_exit_clicked();
    void slot_getVal(int );
private:
    Ui::PerssionDlg *ui;
};

#endif // PERSSIONDLG_H
