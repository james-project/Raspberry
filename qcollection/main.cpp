#include <QtGlobal>
#if QT_VERSION >= 0x050000
#include <QApplication>
#else
#include <QtGui/QApplication>
#endif
#include "mainwindow.h"
#include <QTextCodec>
#include "DataBase/database.h"
#include <QDebug>
#include <QSplashScreen>
#include <QTextCodec>
#include "lock_dlg.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#if QT_VERSION < 0x050000
    QTextCodec::setCodecForTr(QTextCodec::codecForName("utf8"));
#endif
    DataBase::getDataBase();
//    QSplashScreen sp(QPixmap("E:/Project/logo.png"));
//    sp.show();
    MainWindow *mainWindow = MainWindow::getMainWindow();
    mainWindow->show();

    if( 4 ==  a.exec() )
    {
        QProcess::startDetached(qApp->applicationFilePath(), QStringList());
        return 0;
    }
    return 0;
}
