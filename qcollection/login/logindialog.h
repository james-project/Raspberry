#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include "tdialog.h"

namespace Ui {
class LoginDialog;
}

class LoginDialog : public TDialog
{
    Q_OBJECT
    
public:
    enum
    {
        OPERATOR=0,
        ADMIN2,
        TECHNICIAN,
        SUPER_ADMIN
    };
    explicit LoginDialog(QWidget *parent = 0);
    void initUser();
    static int permission();
    static void setPermission(int permission);

   static void setLogStatus(bool login);
   static bool LogStatus();

   static QString user();
    QString CurrentUser(); //MARK0309
    static bool recordHide();///zhj
    static qint64 getRFIDNumber();
    static QString technicianString(void);
    ~LoginDialog();
public slots:
    void Login(void);
    //void close(void);
    void clear(void);
    void acceptSampleInput(bool);


signals:
    void hasLogin();
    //void changeOperator(QString str);
private:
    int  checkPassword(int user_code,QString password);

private:
    void keyPressEvent(QKeyEvent *);
    Ui::LoginDialog *ui;
    static int mPermission;
    static QString mUser;
    static bool mRecordHide; ///zhj
    static bool mHasLogin;   ///为了解决logdialog界面时，摄像头界面显示在上面的问题   zhj
    static qint64 mRFID;   ///为了解决logdialog界面时，摄像头界面显示在上面的问题   zhj
    QMap<int ,bool> hideMap;


};

#endif // LOGINDIALOG_H
