#include "videoip.h"
#include "ui_videoip.h"
#include <QSettings>
#define DIR_SETTINGS "setting.ini"

const QString VideoIp::SETTING_VIDEO_IP_GROUP = "video";
const QString VideoIp::SETTING_VIDEO_IP = "video_ip";

VideoIp::VideoIp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VideoIp)
{
    ui->setupUi(this);
    ui->lineEdit->setText(getVideoIp());
    ui->btn_ok->setVisible(false);
}

VideoIp::~VideoIp()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    set.beginGroup(SETTING_VIDEO_IP_GROUP);
    set.setValue(SETTING_VIDEO_IP,ui->lineEdit->text());
    set.endGroup();
    delete ui;
}

void VideoIp::on_btn_ok_clicked()
{

}

QString VideoIp::getVideoIp()
{
    QSettings set(DIR_SETTINGS,QSettings::IniFormat);
    return set.value(SETTING_VIDEO_IP_GROUP+"/"+SETTING_VIDEO_IP,"").toString();
}
