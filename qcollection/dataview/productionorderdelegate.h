#ifndef PRODUCTIONORDERDELEGATE_H
#define PRODUCTIONORDERDELEGATE_H

#include <QItemDelegate>
#include "mymodel.h"

class ProductionOrderDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ProductionOrderDelegate(QList<Item> &items,QObject *parent = 0);
    ~ProductionOrderDelegate();
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,const QModelIndex &index) const;
    void setItems(QList<Item> &items);
signals:

public slots:
private:
    QList<Item> mItems;
    int mTotalp;
    int mItemsCount;
    int *posTab;
public:
//    static QString _iOrderLineNum;
//    static QString  _taskId;
//    static QString _productName;
//    static QString _productCode;
};

#endif // PRODUCTIONORDERDELEGATE_H
