#ifndef PRODUCTIONORDERLISTMODEL_H
#define PRODUCTIONORDERLISTMODEL_H

#include <QAbstractListModel>
#include "DataBase/databasequery.h"
#include "DataBase/dbtab_productionorder.h"

class ProductionOrderListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum
    {
        RECORD_NO=0,
        //以下为数据库字段
        _ID,
        TASK_ID,
        PRODUCT_NAME,
        PRODUCT_CODE,
        MTYPE3,
        DEVICE_TYPE,
        ORDER_CODE,
        WORK_NUM,
        WORK_TIME,
        MODULE_ID,
        DEVICE_ID,
        MBOM,
        FLAG,
        TIME,
        ROLE_END

    };
    explicit ProductionOrderListModel(QObject *parent = 0);

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    static ProductionOrderListModel *getModel();
    void clearAllItem();

    void selectTabel(void);
    void delItemFromRcd();

private:
    static ProductionOrderListModel *mModel;
    DataBaseQuery *mQuery;
    QString mTableName;
    QString mOption;
    QString mShortItem;
    bool    mDescShort;
};

#endif // PRODUCTIONORDERLISTMODEL_H
