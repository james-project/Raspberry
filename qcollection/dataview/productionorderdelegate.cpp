#include "productionorderdelegate.h"
#include "productionorderlistmodel.h"
#include "Setting/other/drawtext.h"
#include "Setting/other/datestring.h"
#include "Setting/other/timestring.h"
#include "common.h"

//QString ProductionOrderDelegate::_iOrderLineNum;
//QString ProductionOrderDelegate::_taskId;
//QString ProductionOrderDelegate::_productName;
//QString ProductionOrderDelegate::_productCode;



ProductionOrderDelegate::ProductionOrderDelegate(QList<Item> &items,QObject *parent) :
    QItemDelegate(parent)
{
    mItems = items;
    mTotalp = 0;
    mItemsCount = mItems.count();
    posTab = new int[mItemsCount];
    for(int i=0;i<mItemsCount;i++)
    {
        mTotalp += items.at(i).mMaxLen;
        posTab[i] = mTotalp;
    }
}

ProductionOrderDelegate::~ProductionOrderDelegate()
{
    delete posTab;
}

void ProductionOrderDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QFont f("DejaVu Sans",18);
    painter->setFont(f);
    painter->save();
#define MARGIN 1
    QRect  r = option.rect;
    {
        qDebug() << "\n\n\n\n\n  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  index row :" << index.row();
        if(index.row()%2==0)
        {
            QBrush brush(Qt::white);
            painter->setBrush(brush);
            painter->setPen(Qt::NoPen);
        }
        else
        {
            QBrush brush(QColor(220,220,220));
            painter->setBrush(brush);
            painter->setPen(Qt::NoPen);
        }
    }

    painter->drawRect(r);
    painter->setPen(Qt::black);
    QString str;
    int w = r.width()-MARGIN*(mItemsCount-1);
    int pos =0;
#define pos(n) ((w*(item)+tp/2)/tp+(n-1)*MARGIN)
    for(int i=0;i<mItemsCount;i++)
    {
        const Item &item = mItems.at(i);
        int role = item.mRole;
        //float wgt;
        pos+=MARGIN;
        r.setLeft(pos);
        pos = (w*posTab[i] +mTotalp/2)/mTotalp+(i)*MARGIN;
        r.setRight(pos);

        painter->drawLine(r.right(),r.top()+1,r.right(),r.bottom()+1);
        switch(role)
        {
        case ProductionOrderListModel::_ID:
            str = QString::number(index.data(role).toInt());
            DrawText::drawText(painter,r,Qt::AlignVCenter,str.right(4));
            break;

        case ProductionOrderListModel::RECORD_NO:
            str = QString::number(index.data(role).toInt());
            DrawText::drawText(painter,r,Qt::AlignVCenter,str);
            break;

        case ProductionOrderListModel::TASK_ID:
        case ProductionOrderListModel::PRODUCT_NAME:
        case ProductionOrderListModel::PRODUCT_CODE:
        case ProductionOrderListModel::MTYPE3:
        case ProductionOrderListModel::DEVICE_TYPE:
        case ProductionOrderListModel::ORDER_CODE:
        case ProductionOrderListModel::WORK_NUM:
        case ProductionOrderListModel::MODULE_ID:
        case ProductionOrderListModel::DEVICE_ID:
        case ProductionOrderListModel::MBOM:
            str = index.data(role).toString();
//            if( role == ProductionOrderListModel::WORK_NUM ) //读数据
//            {
//                _iOrderLineNum = str;
//            }
//            if( role == ProductionOrderListModel::TASK_ID )
//            {
//                _taskId = str;
//            }
//            if( role == ProductionOrderListModel::PRODUCT_NAME )
//            {
//                _productName = str;
//            }
//            if( role == ProductionOrderListModel::PRODUCT_CODE )
//            {
//                _productCode = str;
//            }



            qDebug() << "================================  ProductionOrderListModel::str: " << str ;


            DrawText::drawText(painter,r,Qt::AlignLeft|Qt::AlignVCenter,str);
            break;
        case ProductionOrderListModel::TIME:
            //            str = TimeString::timetoString(index.data(role).toTime());
            //yyyy-MM-dd hh:mm:ss
            str = index.data(role).toDateTime().toString("yyyy-MM-dd hh:mm:ss");
            qDebug() << "================================  ProductionOrderListModel::TIME: " << str << index.data(role).toDateTime();
            DrawText::drawText(painter,r,Qt::AlignHCenter|Qt::AlignVCenter,str);
            break;
        }
    }
    painter->restore();
}

QSize ProductionOrderDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{

#define ITEM_HEIGHT 30+(SCREEN_HIGH -480)/32

    Q_UNUSED(option);
    Q_UNUSED(index);

    return QSize(0,ITEM_HEIGHT);
}

void ProductionOrderDelegate::setItems(QList<Item> &items)
{
    mItems = items;
    mTotalp = 0;
    mItemsCount = mItems.count();
    if(posTab)
        delete posTab;
    posTab = new int[mItemsCount];
    for(int i=0;i<mItemsCount;i++)
    {
        mTotalp += items.at(i).mMaxLen;
        posTab[i] = mTotalp;
    }
}
