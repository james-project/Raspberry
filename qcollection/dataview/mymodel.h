#ifndef MYMODEL_H
#define MYMODEL_H

#include <QAbstractListModel>
#include "DataBase/databasequery.h"


#define CAR_COLUMN     3
#define ID_COLUMN      1
#define PRODUCT_COLUMN ID_COLUMN
#define COMPANY_COLUMN ID_COLUMN
class Item
{
public:
    enum
    {
        NORMAL_TYPE = 0,
        WEIGHT_FLOAT,
        PRICE_FLOAT
    };

    Item(QString name,int len,int role,int type=0)
    {
        mName = name;
        mMaxLen = len;
        mRole = role;
        mType = type;
    }

    QString mName;
    int mMaxLen;
    int mRole;
    int mType;
};


class MyModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MyModel(int column=1,QObject *parent = 0);
     ~MyModel(){}

    bool addItem(QMap<QString,QVariant> &listData);
    bool updateItem(int id,QMap<QString,QVariant> &listData);
    bool deleteItem(int id);
    virtual void setColumn(int column=0);
    virtual void setOption(QString option);
    virtual void serchText(const QString &item,const QString &text);
//    virtual void serchText(const QStringList &list,const QString &text);
    void easySearchText(const QString &item,const QString &text);
    void easySearchText(const QStringList &list,const QString &text);
    enum
    {
        COLUMN= Qt::UserRole,
        INDEX,
        ISVALID,
        NAME, //
        NUM, //NAME 和 NUM
        ID,
        ROLE_END,

    };

    enum data
    {
        DATA_ADD = -2
    };

    virtual void clearAllItem();
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int count() const;
    virtual int dataCount() const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int setShortMode(const QString str,bool desc=0);
    virtual QString getShortItem();
    //virtual bool findItem(int code);
    virtual QModelIndex findItem(int code,QVariant data1);
    virtual int getFreeCode();
    virtual bool deleteItem(const QModelIndex&index);
    virtual bool updateItem(const QModelIndex&index,QMap<QString, QVariant> &listData);
    QVariant data(const QModelIndex &index, int role) const;
//    static MyModel* getModel(int modelType,int colum=0);

signals:

protected:
    virtual void selectTab();
    int model_type;
    QString mTableName;
    QString mOption;
    QString mShortItem;
    bool mShortDesc;

    int mColumnCount;
    DataBaseQuery *mQuery;



private:


    
public slots:
    
};

#endif // MYMODEL_H
