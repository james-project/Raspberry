#include "productionorderlistmodel.h"
#include <QDebug>

ProductionOrderListModel::ProductionOrderListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    mQuery = new DataBaseQuery(DataBase::getDataBase());
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();
    mOption ="";
    mShortItem = DBTab_ProductionOrder::_ID;
    mTableName = DBTab_ProductionOrder::TAB_ORDER;
    mDescShort = false;
    selectTabel();
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();
}

QVariant ProductionOrderListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}

int ProductionOrderListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
//    if (parent.isValid())
//        return 0;

    // FIXME: Implement me!

    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();

    int row_size = mQuery->size();
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

    return /*mQuery->size()*/row_size;
}

QVariant ProductionOrderListModel::data(const QModelIndex &index, int role) const
{
//    if (!index.isValid())
//        return QVariant();

//    // FIXME: Implement me!
//    return QVariant();

    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();

    int pos = index.row();
    if(!index.isValid() )
    {
        return QVariant();
    }
    if(role==RECORD_NO)
    {
        return pos+1;
    }
    else if(role>=_ID && role<ROLE_END)
    {
        DataBase::getDataBase().open();
        DataBase::getDataBase().transaction();
        mQuery->seek(pos);
        if(mQuery->isValid()) {
            QVariant data = mQuery->value(role-_ID);


            qDebug() << "\n\n\n  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   mQuery.data:"<< data;
            DataBase::getDataBase().commit();
            DataBase::getDataBase().close();
            return /*mQuery->value(role-_ID)*/data;
        }
        else {
            DataBase::getDataBase().commit();
            DataBase::getDataBase().close();
            return QVariant();
        }
    }
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();
    return QVariant();
}


ProductionOrderListModel *ProductionOrderListModel::mModel = NULL;
ProductionOrderListModel *ProductionOrderListModel::getModel()
{
    if(mModel==NULL)
    {
        mModel=new ProductionOrderListModel();
    }
    return mModel;
}

void ProductionOrderListModel::clearAllItem()
{
    beginResetModel();
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();
    mQuery->resetDataBaseTableData(DBTab_ProductionOrder::TAB_ORDER);
    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();
    endResetModel();
}

void ProductionOrderListModel::selectTabel()
{
    DataBase::getDataBase().open();
    DataBase::getDataBase().transaction();

    beginResetModel();
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mDescShort);


        while(mQuery->next())
        {
//            qDebug() << "++++++++++++++++++++++++selectTabel==   " << mQuery->value(0).toString();
        }
     endResetModel();

    DataBase::getDataBase().commit();
    DataBase::getDataBase().close();
}

void ProductionOrderListModel::delItemFromRcd()
{
    beginResetModel();
    clearAllItem();
    endResetModel();
}


