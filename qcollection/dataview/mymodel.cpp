#include "mymodel.h"
#include "DataBase/database.h"
#include "DataBase/dbtabbase.h"
#include <QDebug>
#include <sys/time.h>




MyModel::MyModel(int column,QObject *parent) :
    QAbstractListModel(parent)
{
    mOption = QString("");
    mShortItem = QString("");
    mShortDesc = 0;

    if(column>0) //保持不变
        mColumnCount = column;
    if(mColumnCount<1||mColumnCount>5)
        mColumnCount =5;
}


bool MyModel::addItem(QMap<QString,QVariant> &listData)//增加一条记录
{
   beginResetModel();
   bool ret = mQuery->insert(mTableName,listData);
   endResetModel();
   mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
   return ret;
}


bool MyModel::updateItem(int id, QMap<QString, QVariant> &listData)
{
    QString option = DBTabBase::_ID;
    option += " = ";
    option += QString::number(id);

    //this->submit()
    beginResetModel();
    bool ret = mQuery->updateItem(mTableName,listData,option);
    endResetModel();
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
    return ret;
}

QModelIndex MyModel::findItem(int item,QVariant data1)
{
    ////qDebug()<<"findItem";
    QModelIndex index = QModelIndex();  //1106 add = QModelIndex()

    for(int i = 0;i<rowCount();i++)
    {
        for(int j=0;j<mColumnCount;j++)
        {

            index = this->index(i,j);
            if(index.data(item)==data1)
            {
                if(i*mColumnCount+j>=dataCount())
                {
                    return QModelIndex();
                }
                else
                {
                    return index;
                }
            }
        }
    }
    return QModelIndex();
}

int MyModel::getFreeCode()
{
    return 0;
}


bool MyModel::deleteItem(int id)
{
    QString option = DBTabBase::_ID;
    option += " = ";
    option += QString::number(id);

    beginResetModel();
    bool ret = mQuery->deleteItem(mTableName,option);
    endResetModel();
    //mQuery->selectTable(mTableName,NULL,mOption);
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
    return ret;
}
void MyModel::setOption(QString option)
{
    beginResetModel();
    mOption = option;
    selectTab();
    endResetModel();
}

void MyModel::selectTab()
{
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
}

void MyModel::serchText(const QString &item,const QString &text)
{
    beginResetModel();
    mOption = item + " LIKE \'%" + text + "%\'";
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
    endResetModel();
}



void MyModel::easySearchText(const QString &item,const QString &text)
{
    struct timeval tpstart,tpend;

    QString option;
    if(text!="")
    {
        mOption = item + " LIKE \'%";
        for(int loop=0;loop<text.length();loop++)
        {
            mOption +=text.at(loop);
            mOption +='%';
        }
        mOption +='\'';
        gettimeofday(&tpstart,0);
        beginResetModel();

        mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
        gettimeofday(&tpend,0);
        endResetModel();

        qDebug()<<"used 1:"<<(tpend.tv_usec -tpstart.tv_usec);
    }
    else
    {
        setOption("");
    }
}


void MyModel::easySearchText(const QStringList &list,const QString &text)
{
    struct timeval tpstart,tpend;

    if(text!="")
    {
        for(int i =0 ;i<list.count();i++)
        {
            if(i ==0)
            {
                mOption = list.at(i) + " LIKE \'%";
            }
            else
            {
                mOption += list.at(i) + " LIKE \'%";
            }

            for(int loop=0;loop<text.length();loop++)
            {
                mOption +=text.at(loop);
                mOption +='%';
            }
            mOption +='\'';

            if(i!=list.count()-1)
            {
                mOption +=" or ";
            }
        }

        gettimeofday(&tpstart,0);
        beginResetModel();
        mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
        gettimeofday(&tpend,0);

        endResetModel();
        qDebug()<<"used 1:"<<(tpend.tv_usec -tpstart.tv_usec);
    }
    else
    {
        setOption("");
    }
}


void MyModel::clearAllItem()
{
    beginResetModel();
    mQuery->resetDataBaseTableData(mTableName);
    endResetModel();
}

bool MyModel::deleteItem(const QModelIndex &index)
{
    return MyModel::deleteItem(index.data(ID).toInt());

}

bool MyModel::updateItem(const QModelIndex &index,QMap<QString, QVariant> &listData)
{
    return MyModel::updateItem(index.data(ID).toInt(),listData);
}


void MyModel::setColumn(int column)
{

    if(column>0) //==0保持不变
    {
        beginResetModel();
        mColumnCount = column;
        if(mColumnCount<1||mColumnCount>5)
            mColumnCount =5;
        endResetModel();
    }
    else
    {
        beginResetModel();
        if(mColumnCount<1||mColumnCount>5)
            mColumnCount =5;
        endResetModel();
    }


}


int MyModel::rowCount(const QModelIndex &parent) const
{
//    Q_UNUSED(parent);
    /*
    if(mCountChange==true)
    {
        int len = 0;
        if(mQuery->last())
        {
             len = mQuery->at() + 1;
        }
       // mCount = len;
        //mCountChange= false;
    }
    return  (mCount+mColumnCount-1)/mColumnCount;
    */

    return (count()+mColumnCount-1)/mColumnCount;
}



int MyModel::count() const /*要画的*/
{
    int len = dataCount();
    if(mColumnCount==1)
        return len;
    else
        return len;

}

int MyModel::dataCount() const /*实际数据数*/
{
    int len = 0;
    if(mQuery->last())
    {
         len = mQuery->at() + 1;
    }
    return len;
}

int MyModel::setShortMode(const QString str, bool desc)
{
    beginResetModel();
    mShortItem = str;
    mShortDesc = desc;
    mQuery->selectTable(mTableName,NULL,mOption,mShortItem,mShortDesc);
    endResetModel();
    return 1;
}

QString MyModel::getShortItem()
{
    return mShortItem ;
}



int MyModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return  mColumnCount;
}


QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if(role==COLUMN)
    {
        return mColumnCount;
    }
    else if(role==INDEX)
    {
        return mColumnCount*index.row()+index.column();
    }
    else if(role==ISVALID)
    {
        if(mColumnCount*index.row()+index.column()>=MyModel::count()-1)
            return false;
        else
            return true;
    }
    return QVariant();
}

//MyModel *MyModel::getModel(int modelType,int column)
//{
//    switch(modelType)
//    {
//    case DataBase::IDNUM_PRODUCT:
//        return ProductModel::getModel(column);
//    case  DataBase::IDNUM_COMPANY:
//        return CompanyModel::getModel(column);
//    case DataBase::IDNUM_PLATE:
//        return CarModel::getModel(column);
//        break;
//    case DataBase::IDNUM_USER:
//        return UserModel::getModel(column);
//        break;
//    case DataBase::IDGROUPS:
//        return IDGroupsModel::getModel(column);
//        break;
//    default:
//        return IdModel::getModel(modelType);
//        break;

//    }
//    return NULL;
//}
