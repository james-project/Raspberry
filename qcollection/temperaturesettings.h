#ifndef TEMPERATURESETTINGS_H
#define TEMPERATURESETTINGS_H

#include <QDialog>

namespace Ui {
class TemperatureSettings;
}

class TemperatureSettings : public QDialog
{
    Q_OBJECT

public:
    explicit TemperatureSettings(QWidget *parent = 0);
    ~TemperatureSettings();

private:
    Ui::TemperatureSettings *ui;
};

#endif // TEMPERATURESETTINGS_H
