#ifndef LOCK_DLG_H
#define LOCK_DLG_H

#include <QDialog>
#include <QKeyEvent>

namespace Ui {
class Lock_Dlg;
}

class Lock_Dlg : public QDialog
{
    Q_OBJECT

public:
    explicit Lock_Dlg( QString strText,  QWidget *parent = 0);
    ~Lock_Dlg();

private:
    Ui::Lock_Dlg *ui;
};

#endif // LOCK_DLG_H
