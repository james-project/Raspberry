CONFIG += Http

HEADERS += \
    $$PWD/HttpClient/httpclient.h \
    $$PWD/HttpClient/pingthread.h \
    $$PWD/HttpServer/httpconnection.h \
    $$PWD/HttpServer/http_parser.h \
    $$PWD/HttpServer/httprequest.h \
    $$PWD/HttpServer/httpresponse.h \
    $$PWD/HttpServer/httpserver.h

SOURCES += \
    $$PWD/HttpClient/httpclient.cpp \
    $$PWD/HttpClient/pingthread.cpp \
    $$PWD/HttpServer/httpconnection.cpp \
    $$PWD/HttpServer/http_parser.cpp \
    $$PWD/HttpServer/httprequest.cpp \
    $$PWD/HttpServer/httpresponse.cpp \
    $$PWD/HttpServer/httpserver.cpp
