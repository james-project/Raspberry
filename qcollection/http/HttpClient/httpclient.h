#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include "pingthread.h"
#include <QObject>//
#include "../../Device/DeviceState/devicestate.h"
#include <QDebug>

class HttpClient : public QObject
{
    Q_OBJECT
public:
    explicit HttpClient(QString deviceType,QString deviceId,QObject *parent = 0);
    static HttpClient *getobj(QString deviceType,QString deviceId);
    ~HttpClient();

    void posNotGood(QString deviceType, QString deviceId, QString taskId, QString property, int iNotGood);
    //notgood
    void posNotGood(QString deviceType, QString deviceId, QString TaskId, QString property/*task*/, QString strNotGood);
    //gongzhuang
    void posGongzhuang( QString type, QString id, QString taskId, QJsonObject obj );
    //material
    void posMaterial( QString type, QString id, QString taskId, QJsonObject obj );

    void postWarningInfo( QString type, QString id, QString taskId, QJsonObject obj , bool b);

    void postWarningInfo( QString type, QString id, QString taskId, QJsonObject obj );
    //Operator
    void posOperator( QString type, QString id, QString taskId, QJsonObject obj, DeviceState::DeviceStion );
    //7新接口
    void pos7Datas(QString deviceType, QString deviceId, QString TaskId, QJsonArray arr );
    //6设备关机
    void posShutDown(QString deviceType, QString deviceId, QString property, QString value );
    //5更新当日任务
    void posTodayTask(QString deviceType, QString deviceId, QString TaskId, QString property/*task*/,QJsonObject , int Type = 0 ); //Type为0则为点焊，1位冲压
    //4更新设备状态
    void posOrderInfo( QString deviceType,QString deviceId, QString property/*task*/, QString value);
    /*!
     * \brief registerDeviceInfo : 向Http Server发送注册信息
     * \param deviceType
     * \param deviceId
     * \param property
     * \param value
     */
    void registerDeviceInfo(QString deviceType = "Device A",QString deviceId = "A2",QString property="register",QString value="testing11111");

    /*!
     * \brief posDeviceInfo : 向Http Server发送IP MAC地址 和 current item等等
     * \param deviceType
     * \param deviceId
     * \param property
     * \param value
     */
    void posDeviceInfo(QString deviceType = "Device A",QString deviceId = "A2",QString property="register",QString value="testing11111");

    /*!
     * \brief posDeviceDetail : 向Http Server发送设备明细
     * \param deviceType
     * \param deviceId
     * \param property
     * \param value
     * \param sequence
     */
    void posDeviceDetail(QString deviceType = "Device A",QString deviceId = "A2",QString property="register",QString value="testing11111");

    void putDeviceDetail(QString deviceType = "Device A",QString deviceId = "A2",QString property="register",QString value="testing11111");


    /*!
     * \brief putUpdateDevice ：更新设备--发送下一个设备前，需线更新
     * \param deviceType
     * \param deviceId
     * \param property
     * \param value
     */
    void putUpdateDevice(QString deviceType = "Device A",QString deviceId = "A2",QString property="register",QString value="testing11111");


    /*!
     * \brief getUserInfo : get 方式获取用户信息
     * \param deviceType
     * \param deviceId
     * \param property
     * \param value
     */
    void getUserInfo(QString userId="23456789");

    /*!
     * \brief postGetUserInfo  :  post way get user permission;
     * \param userId
     * \param station
     * \param deviceType
     * \param deviceId
     * \param taskId : 向服务器获取用户信息的时候增加taskId－－订单task
     */
    void postGetUserInfo(QString userId, QString station, QString deviceType, QString deviceId, QString taskId);

    void postOrderState(QString orderState, QString deviceType, QString deviceId, QString taskId);
    void putOrderState(QString orderState, QString deviceType, QString deviceId, QString taskId);


    QString macAddress();
    QString getLocalHostIP();
    bool ping(QString ip);

    int getUserPermission(QString permissionString);
    int getUserPermission();

signals:    
    void startHttpWork();
    void ipStatusSignals(bool);
    void registerOK(bool);
    void sendUserPerssion(QString);
    void sendStation_OfHttp(QString);
    void postSerialNum_Signals();
    void updateDeviceState_Signals();
    void orderConfirmOK();
    void getUserStrirng(QString);


public slots:
    void replyFinish(QNetworkReply*);
private slots:
    void receiIpStatus(bool);
private:
   //static HttpClient *mInstance;
   static HttpClient *m_Instance;
   QNetworkAccessManager *network_manager;
   QString mServerSetting;
   int m_permission;
   QString m_deviceType;
   QString m_deviceId;
   HttpClient(){
       qDebug()<<"Singleton construction!!";
   }

   class GC {
    public:
        GC()
        {
          qDebug() << "GC construction!!";
        }
        ~GC()
        {
            qDebug() << "GC destruction!!";
            // We can destory all the resouce here, eg:db connector, file handle and so on
            if (m_Instance != NULL)
            {
                delete m_Instance;
                m_Instance = NULL;
                qDebug() << "Singleton destruction!!";
            }
        }
    };
    static GC gc;  //垃圾回收类的静态成员
};

#endif // HTTPCLIENT_H
