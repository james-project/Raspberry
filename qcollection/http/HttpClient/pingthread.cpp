#include "pingthread.h"
#include <QProcess>
#include <QDebug>//

PingThread::PingThread(QString ip,QThread *parent) :
    m_ip(ip)
    ,QThread(parent),
    REGISTER(false)
{
    //this->start();
}

PingThread::~PingThread()
{
    //do nothing
}

void PingThread::run()
{
   //msleep(3000);
   while (true)
   {
        bool status  = ping(m_ip);
        emit ipstatus(status);
        msleep(30000);
    }
}

bool PingThread::ping(QString ip)
{
    m_mutex.lock();
    int exitCode;
    int tempInt;
    const QString strArg = "ping -s 1 -c 1 " + ip; //linux platform
    tempInt = QProcess::execute(strArg);
    if (0==tempInt&&REGISTER){
        emit ipstatus(true);
        REGISTER = true;
    }
    //Linux know issue, usually fail during first ping
    const QString strArg_second = "ping -s 1 -c 4 " + ip;
    exitCode = QProcess::execute(strArg_second);//
    qDebug() << "\n\n ~~ ping Arg: " << strArg_second<<exitCode;
    if(0 == exitCode)
    {
        //it's alive
        //qDebug() << "\n\n\nPingThread shell ping " + ip + " sucessed";
        m_mutex.unlock();
        return true;
    }
    else
    {
        //qDebug() << "\n\n\nPingThread shell ping " + ip + " failed";
        m_mutex.unlock();
        return false;
    }
}
