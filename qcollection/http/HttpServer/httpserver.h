#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H
#define HTTPSERVER_VERSION_MAJOR 0
#define HTTPSERVER_VERSION_MINOR 1
#define HTTPSERVER_VERSION_PATCH 0
#include <QObject>
#include <QHostAddress>
class QTcpServer;
class HttpRequest;
class HttpResponse;
typedef QHash<QString, QString> HeaderHash;
extern QHash<int, QString> STATUS_CODES;
class HttpServer : public QObject
{
	Q_OBJECT
public:
	HttpServer(QObject *parent = 0);
	virtual ~HttpServer();
	bool listen(const QHostAddress &address = QHostAddress::Any, quint16 port=0);
	bool listen(quint16 port);
    bool response(QString);
signals:
    void dataRequest(QString);
    void dataSend(QByteArray,QByteArray);
private slots:
	void newConnection();
	void onRequest(HttpRequest*, HttpResponse*);
    void arrayByteReceive(QByteArray,QByteArray);//MARK0808 Add
private:
	QTcpServer *m_tcpServer;
    HttpResponse* mResp;
};
#endif
