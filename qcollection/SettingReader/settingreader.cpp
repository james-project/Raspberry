#include "settingreader.h"
#include <QSettings>
#include "common.h"

const QString SettingReader::SETTING_GROUP_NOMAL = "Group_Normal";
SettingReader::SettingReader(QObject *parent) : QObject(parent)
{

}

SettingReader *SettingReader::mSettingReader = NULL;
SettingReader *SettingReader::getobj()
{
    if (mSettingReader == NULL) {
        mSettingReader = new SettingReader();
    }
    return mSettingReader;
}

#define Group_currMode "Group_currMode"
#define currentWorkMode "CurrWorkMode"
void SettingReader::setCurrWorkMode(int mode)
{
    QSettings set(DIR_SETTINGS, QSettings::IniFormat);
    set.beginGroup(Group_currMode);
    set.setValue(currentWorkMode,mode);
    set.endGroup();
}

int SettingReader::getCurrWorkMode()
{
    QSettings set(DIR_SETTINGS, QSettings::IniFormat);
    set.beginGroup(Group_currMode);
    int currMode = set.value(currentWorkMode,0).toInt();
    set.endGroup();
    return currMode;
}




#define coefFilterMode "CoefMode"

void SettingReader::setFilterCoef(float coefvalue)
{
    QSettings set(DIR_SETTINGS, QSettings::IniFormat);
    set.beginGroup(Group_currMode);
    set.setValue(coefFilterMode,coefvalue);
    set.endGroup();
}

float SettingReader::getFilterCoef()
{
    QSettings set(DIR_SETTINGS, QSettings::IniFormat);
    set.beginGroup(Group_currMode);
    float coefvalue = set.value(coefFilterMode,0.001).toFloat();
    set.endGroup();
    return coefvalue;
}
