#ifndef SETTINGREADER_H
#define SETTINGREADER_H



#include <QObject>
#include "globalvar.h"

class SettingReader : public QObject
{
    Q_OBJECT
public:
    static const QString SETTING_GROUP_NOMAL;
    explicit SettingReader(QObject *parent = 0);
    static SettingReader *getobj();

    static void setCurrWorkMode(int mode);
    static int getCurrWorkMode();

    static void setFilterCoef(float);
    static float getFilterCoef();


signals:

public slots:
private:
    static SettingReader *mSettingReader;
};

#endif // SETTINGREADER_H
