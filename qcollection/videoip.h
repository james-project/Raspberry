#ifndef VIDEOIP_H
#define VIDEOIP_H

#include <QWidget>

namespace Ui {
class VideoIp;
}

class VideoIp : public QWidget
{
    Q_OBJECT

public:
    explicit VideoIp(QWidget *parent = 0);
    ~VideoIp();

    static QString getVideoIp();

    static const QString SETTING_VIDEO_IP_GROUP;
    static const QString SETTING_VIDEO_IP;
private:
    Ui::VideoIp *ui;
private slots:
    void on_btn_ok_clicked();
};

#endif // VIDEOIP_H
