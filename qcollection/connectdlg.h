#ifndef CONNECTDLG_H
#define CONNECTDLG_H

#include <QDialog>

namespace Ui {
class connectDlg;
}

class connectDlg : public QDialog
{
    Q_OBJECT

public:
    explicit connectDlg(QWidget *parent = 0);
    ~connectDlg();

private:
    Ui::connectDlg *ui;
private slots:
    void on_pushButton_clicked();
};

#endif // CONNECTDLG_H
