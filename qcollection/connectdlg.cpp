#include "connectdlg.h"
#include "ui_connectdlg.h"

connectDlg::connectDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::connectDlg)
{
    ui->setupUi(this);
    setWindowState( Qt::WindowMaximized);
    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
}

connectDlg::~connectDlg()
{
    delete ui;
}

void connectDlg::on_pushButton_clicked()
{
    this->done(0);
}
