#ifndef DEVICEIDSETTINGS_H
#define DEVICEIDSETTINGS_H

#include <QWidget>

namespace Ui {
class DeviceIdSettings;
}

class DeviceIdSettings : public QWidget
{
    Q_OBJECT

public:
    explicit DeviceIdSettings(QWidget *parent = 0);
    ~DeviceIdSettings();
    static QString getDeviceId();

private:
    Ui::DeviceIdSettings *ui;
    static const QString SETTING_DEVICE_ID_GROUP;
    static const QString SETTING_DEVICE_ID;

private slots:
    void on_btn_ok_clicked();
};

#endif // DEVICEIDSETTINGS_H
