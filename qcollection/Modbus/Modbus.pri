CONFIG += MODBUS

HEADERS += \
    $$PWD/ModbusRTU/modbusrtumaster.h \
    $$PWD/ModbusRTU/serialport.h

SOURCES += \
    $$PWD/ModbusRTU/modbusrtumaster.cpp \
    $$PWD/ModbusRTU/serialport.cpp
