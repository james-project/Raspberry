#ifndef __VERSION_H__
#define __VERSION_H__

#define VER_MODEL     "MODEL"
#define VER_BOARD     "BOARD"
#define VER_REFERENCE "REFERENCE"
#define VER_NUMBER    "0.9.19.0"

#define APP_NAME        "ModbusRTU-ASCII Tester"

#define APP_CONFIG_FILE "config.ini"


#define PROGRAM_TITLE                           APP_NAME
#define PROGRAM_WIDTH                           800
#define PROGRAM_HEIGHT                          480

#define KOR_TRANSLATION_FILE_PATH               "data/translation/ko_kr.qm"
#define ENG_TRANSLATION_FILE_PATH               "data/translation/en_us.qm"


#endif /* __VERSION_H__ */
