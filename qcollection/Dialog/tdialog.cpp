#include "tdialog.h"
#include "common.h"
#include <QDebug>

TDialog::TDialog(QWidget *parent) :
    QDialog(parent)
{
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    this->setAttribute(Qt::WA_DeleteOnClose);
//    this->setFocusPolicy(Qt::StrongFocus);
//    this->setFocus();
    //qDebug()<<"###################################################"
}


#ifdef GRAB_SCREEN
#include <mainwindow.h>
#endif




void TDialog::keyPressEvent(QKeyEvent *event)
{

    int key = event->key();
    if(key == Qt::Key_Enter || key== Qt::Key_Return)
    {
       this->accept();
    }
    else if(key == Qt::Key_Escape)
    {
        this->reject();

        //       this->close();
    }
#ifdef GRAB_SCREEN
   else if(event->key()==KEY_ZERO)
   {
       MainWindow::grabScreen();
       return;
   }
#endif
    else
    {
        QDialog::keyPressEvent(event);
    }
}
