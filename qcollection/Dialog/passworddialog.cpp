#include "passworddialog.h"
#include "ui_passworddialog.h"
#include  <QDesktopWidget>
#include "mymessagedialog.h"
#include <QDebug>
#include "common.h"
#include <QSettings>
//#include "SysSetting/syssettingwidget.h"

PassWordDialog::PassWordDialog(/*const QString pwString,*/QWidget *parent) :
    TDialog(parent),/*mPw(pwString),*/
    ui(new Ui::PassWordDialog)
{
    ui->setupUi(this);
    this->setEnabled(true);
    //ui->lePW->setEnabled(true);
    ui->lePW->setFocus();
    connect(ui->btnOk,SIGNAL(clicked()),this,SLOT(clickOkBtn()));
    connect(ui->btnCancel,SIGNAL(clicked()),this,SLOT(clickCancelBtn()));
}

PassWordDialog::~PassWordDialog()
{
    qDebug("PassWordDialog destroy");
    delete ui;
}

void PassWordDialog::clickOkBtn()
{
    emit passwdInputedFinish(ui->lePW->text());
    /*
    if(ui->lePW->text().compare(mPw)==0)
    {
        accept();
    }
    else if(ui->lePW->text()==BACKDOOR_PASSWD)
    {
        accept();
    }
    else if(ui->lePW->text()==SUPPER_PASSWD
            ||ui->lePW->text()=="۰۰۰۰۰۰") //波斯语000000
    {
        QSettings set(DIR_SETTINGS,QSettings::IniFormat);
        set.beginGroup(SysSettingWidget::GROUP_NAME);
        QString str =set.value(SysSettingWidget::PASSWORD_SETTING_NAME).toString();
        set.endGroup();
        if(str.isEmpty())
        {
            accept();
        }
        else
        {
            MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
        }
    }
    else
    {
            MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
    }*/


//    if(ui->lePW->text()==SUPPER_PASSWD
//            ||ui->lePW->text()=="۰۰۰۰۰۰") //波斯语000000
//    {
//        QSettings set(DIR_SETTINGS,QSettings::IniFormat);
//        set.beginGroup(SysSettingWidget::GROUP_NAME);
//        QString str =set.value(SysSettingWidget::PASSWORD_SETTING_NAME).toString();
//        set.endGroup();
//        if(str.isEmpty())
//        {
//            accept();
//        }
//        else
//        {
//            MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
//        }
//    }
//    else if(ui->lePW->text()==BACKDOOR_PASSWD)
//    {
//        accept();
//    }
//    else
//    {
//        //qDebug()<<mPw;
//        //qDebug()<<ui->lePW->text();
//        if(ui->lePW->text().compare(mPw)==0)
//        {
//            accept();
//        }
//        else
//        {
//            MyMessageDialog::warning(0,tr("Error"),tr("Password is wrong"));
//        }
//    }
}

void PassWordDialog::clickCancelBtn()
{
    reject();
}

#ifdef GRAB_SCREEN
#include <mainwindow.h>
#endif
void PassWordDialog::keyPressEvent(QKeyEvent *keyEvent)
{
    if(keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return)
    {
        clickOkBtn();
    }
    else if(keyEvent->key() == Qt::Key_Escape)
    {
        clickCancelBtn();
    }
#ifdef GRAB_SCREEN
   else if(keyEvent->key()==KEY_ZERO)
   {
       MainWindow::grabScreen();
       return;
   }
#endif

}

void PassWordDialog::clearPassword()  //MARK0405 Add
{
    ui->lePW->clear();
}
