#include "mymessagedialog.h"
#include "ui_mymessagedialog.h"
#include <QDebug>
#include <QMessageBox>
#include <QTimer>
#include "common.h"
#include <QDesktopWidget>
#include <QKeyEvent>


MyMessageDialog::MyMessageDialog(const QString &string,
                                 const QString &title,
                                 QWidget *parent,
                                 QString cancelBtnText,
                                 QString confirmBtnText) :
    TDialog(parent),
    ui(new Ui::MyMessageDialog)
{
    ui->setupUi(this);

//    this->setWindowTitle(title);
    ui->label->setText(string);
    ui->label->setWordWrap(true);
    ui->label_title->setText(title);
    ui->btn_cancel->setText(cancelBtnText);
    ui->btn_confirm->setText(confirmBtnText);
    connect(ui->btn_cancel,SIGNAL(clicked()),this,SLOT(reject()));
    connect(ui->btn_confirm,SIGNAL(clicked()),this,SLOT(accept()));
    this->setGeometry(WINDOW_GEOMETRY(0.4,0.4));
    this->clearFocus();
    ui->btn_confirm->clearFocus();
    ui->btn_ok->setVisible(false);
}

void MyMessageDialog::hideCancelBtn()
{
    ui->btn_cancel->hide();
    ui->horizontalLayout->setStretch(0,2);
    ui->horizontalLayout->setStretch(1,1);
    ui->horizontalLayout->setStretch(2,1);
    ui->horizontalLayout->setStretch(3,2);
}

void MyMessageDialog::hideConfirmBtn()
{
    ui->btn_confirm->hide();
}

void MyMessageDialog::hideBtn()
{
    ui->btnFrame->hide();
}

void MyMessageDialog::setTextForConfirmBtn(QString str)
{
    ui->btn_confirm->setText(str);
}
void MyMessageDialog::setTextForCancelBtn(QString str)
{
    ui->btn_cancel->setText(str);
}


int MyMessageDialog::question(QWidget *parent,
                    const QString &title,
                    const QString& text,
                    const QString& buttonConfirmText,
                    const QString& buttonCancelText)
{
    MyMessageDialog *dlg = new MyMessageDialog(text,title,parent);
    dlg->setTextForConfirmBtn(buttonConfirmText);
    dlg->setTextForCancelBtn(buttonCancelText);
    return dlg->exec();
}

int MyMessageDialog::warning(QWidget *parent,
                    const QString &title,
                    const QString& text)
{
    MyMessageDialog *dlg = new MyMessageDialog(text,title,parent);
    dlg->hideCancelBtn();
    dlg->timer.start(1999);
    connect(&(dlg->timer),SIGNAL(timeout()),dlg,SLOT(clickConfirm()));
    return dlg->exec();
}

void  MyMessageDialog::message(QWidget *parent,
                             const QString &title,
                             const QString& text,
                             int msec)
{
     MyMessageDialog *dlg = new MyMessageDialog(text,title,parent);

     dlg->hideBtn();
     QTimer::singleShot(msec, dlg, SLOT(accept()));
     dlg->exec();
}

MyMessageDialog * MyMessageDialog::messageShow(QWidget *parent,
                         const QString &title,
                         const QString& text)
{
    MyMessageDialog *dlg = new MyMessageDialog(text,title,parent);
    dlg->hideBtn();
    dlg->show();
    dlg->repaint();
    return dlg;
}


MyMessageDialog::~MyMessageDialog()
{
    delete ui;
}

void MyMessageDialog::showOkBtn()
{
    ui->btn_ok->setVisible(true);
}

void MyMessageDialog::on_btn_ok_clicked()
{
    emit sig_btnOk();
}
