#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include "tdialog.h"
#include <QKeyEvent>


namespace Ui {
class PassWordDialog;
}

class PassWordDialog : public TDialog
{
    Q_OBJECT
    
public:
    explicit PassWordDialog(/*const QString pw,*/QWidget *parent = 0);
    ~PassWordDialog();

    void keyPressEvent(QKeyEvent  *event);
    void clearPassword(); //MARK0405

public slots:
    void clickCancelBtn(void);
    void clickOkBtn(void);
signals:
    void passwdInputedFinish(const QString &passwd);
private:
    QString mPw;
    Ui::PassWordDialog *ui;
};

#endif // PASSWORDDIALOG_H
