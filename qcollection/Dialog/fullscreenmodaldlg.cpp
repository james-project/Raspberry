#include "fullscreenmodaldlg.h"
#include "ui_fullscreenmodaldlg.h"
#include "common.h"
#ifdef ANDROID_PAY
#include "Dialog/mymessagedialog.h"
#endif

fullScreenModalDlg::fullScreenModalDlg(QString content,QWidget *parent,bool showBtn) :
    QDialog(parent),
    ui(new Ui::fullScreenModalDlg)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    this->setFixedSize(SCREEN_WIDTH,SCREEN_HIGH/*-BAR_HIGH*/);
    setAttribute(Qt::WA_DeleteOnClose);
    if(showBtn == false)
    {
        ui->btn_cancel->hide();
    }
    ui->lab_content->setText(content);
}

fullScreenModalDlg::~fullScreenModalDlg()
{
    delete ui;
}
void fullScreenModalDlg::setBtnText(QString str)
{
    ui->btn_cancel->setText(str);
}

void fullScreenModalDlg::closeDlg()
{
    this->reject();
}

void fullScreenModalDlg::confirmFirstCheck()
{
    this->reject();
}

void fullScreenModalDlg::materialconfirmOK()
{
    this->reject();
}

void fullScreenModalDlg::productionCheckPass()
{
    this->reject();
}

void fullScreenModalDlg::firstQCCheckPass()
{
    this->reject();
}
void fullScreenModalDlg::posResult(bool, QString, float)
{
    this->accept();
}
void fullScreenModalDlg::on_btn_cancel_clicked()
{    
    this->reject();
}
