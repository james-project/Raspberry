#ifndef TDIALOG_H
#define TDIALOG_H

#include <QDialog>
#include <QKeyEvent>

class TDialog : public QDialog
{
    Q_OBJECT
public:
    explicit TDialog(QWidget *parent = 0);

signals:

public slots:

protected:
    virtual void keyPressEvent(QKeyEvent *);

};

#endif // TDIALOG_H
