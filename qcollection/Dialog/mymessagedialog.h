#ifndef MYMESSAGEDIALOG_H
#define MYMESSAGEDIALOG_H

#include "tdialog.h"
#include <QTimer>
#include <QKeyEvent>


namespace Ui {
class MyMessageDialog;
}

class MyMessageDialog : public TDialog
{
    Q_OBJECT
    
public:
    explicit MyMessageDialog(const QString &string,
                             const QString &title="Form",
                             QWidget *parent = 0,
                             QString cancelBtnText=tr("Cancel"),
                             QString confirmBtnText=tr("OK"));
    ~MyMessageDialog();
    enum
    {
        RESULT_CANCEL=0,
        RESULT_OK=1
    };

    void showOkBtn();
    void hideCancelBtn();
    void hideConfirmBtn();
    void hideBtn();
    void setTextForConfirmBtn(QString str);
    void setTextForCancelBtn(QString str);
    static int question(QWidget *parent,
                         const QString &title,
                         const QString& text,
                         const QString& buttonConfirmText=QString(tr("&Yes")),
                         const QString& buttonCancelText=QString(tr("&No")));
    static int warning(QWidget *parent,
                        const QString &title,
                        const QString& text);
    static void message(QWidget *parent,
                                 const QString &title,
                                 const QString& text,
                                    int msec = 1000);

    static MyMessageDialog *messageShow(QWidget *parent,
                                    const QString &title,
                                    const QString& text
                                       );
signals:
    void sig_btnOk();
    void sig_btnOk_clicked();
private:
    Ui::MyMessageDialog *ui;

private slots:


    void on_btn_ok_clicked();

private:
    QTimer timer;
};

#endif // MYMESSAGEDIALOG_H
