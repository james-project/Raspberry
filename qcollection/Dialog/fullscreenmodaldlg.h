#ifndef FULLSCREENMODALDLG_H
#define FULLSCREENMODALDLG_H
#include <QDialog>
namespace Ui {
class fullScreenModalDlg;
}
class fullScreenModalDlg : public QDialog
{
    Q_OBJECT
public:
    explicit fullScreenModalDlg(QString content,QWidget *parent = 0,bool showBtn=true);
    ~fullScreenModalDlg();
    void setBtnText(QString str);
private slots:
    void closeDlg();
    void confirmFirstCheck();
    void materialconfirmOK();
    void productionCheckPass();
    void firstQCCheckPass();
private slots:
    void posResult(bool,QString,float);
    void on_btn_cancel_clicked();
signals:
    void productionChekPass_signals();
    void firstQCCheckPass_signals();
private:
    Ui::fullScreenModalDlg *ui;
};
#endif // FULLSCREENMODALDLG_H
