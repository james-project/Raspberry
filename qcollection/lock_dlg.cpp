#include "lock_dlg.h"
#include "ui_lock_dlg.h"
#include <QDebug>

Lock_Dlg::Lock_Dlg( QString strText, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Lock_Dlg)
{
    ui->setupUi(this);
    ui->lbl_text->clear();
    ui->lbl_text->setText(strText);
    setWindowState( Qt::WindowMaximized);
    setWindowFlags( Qt::FramelessWindowHint);
    if(parent != NULL )
    {
            setFixedSize( parent->width(), parent->height());
    }
}

Lock_Dlg::~Lock_Dlg()
{
    delete ui;
}
