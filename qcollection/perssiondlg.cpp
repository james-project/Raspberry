#include "perssiondlg.h"
#include "ui_perssiondlg.h"
#include <QMessageBox>
#include "./input/inputdialog.h"

PerssionDlg::PerssionDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PerssionDlg)
{
    ui->setupUi(this);
    setWindowState( Qt::WindowMaximized);
    ui->lineEdit->installEventFilter(this);
    setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
}

PerssionDlg::~PerssionDlg()
{
    delete ui;
}

void PerssionDlg::on_btn_ok_clicked()
{
    if( ui->lineEdit->text() != "1992")
    {
        ui->lineEdit->clear();
        ui->lineEdit->setPlaceholderText("密码错误，请重新输入!");
    }
    else
    {
        done(0);
    }
}

void PerssionDlg::on_btn_exit_clicked()
{
    this->done(-1);
}

bool PerssionDlg::eventFilter(QObject *watched, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if( watched == ui->lineEdit)
        {
            QWidget *back = new QWidget(this);
            back->setGeometry(0,0,SCREEN_WIDTH,SCREEN_HIGH);
            back->setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint); //Windows上设为透明色 需有Qt::FramelessWindowHint
            back->setAttribute(Qt::WA_TranslucentBackground,true); //Windows上设为透明色
            back->setWindowModality(Qt::WindowModal);
            back->show();
            inputDialog *inputdialog = new inputDialog(back,"模拟输入值",false,5,0,0,true);
            inputdialog->setWindowModality(Qt::WindowModal);
            inputdialog->hideSome();
            inputdialog->show();
            connect(inputdialog,SIGNAL(inputValue(int)),this,SLOT(slot_getVal(int)));
            connect(inputdialog,SIGNAL(exitInputdialog()),back,SLOT(deleteLater()));
            connect(inputdialog,SIGNAL(exitInputdialog()),inputdialog,SLOT(deleteLater()));
        }

    }
    return QWidget::eventFilter(watched,event);
}

void PerssionDlg::slot_getVal( int val )
{
    ui->lineEdit->setText( QString::number(val));
}

