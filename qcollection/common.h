#ifndef COMMON_H
#define COMMON_H
#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>
#define COM_DIR "/home/pi/"
#define PATH_DATABASE COM_DIR
#define DATABASE_NAME  "gplc.db"


#define FLOAT_ZERO 0.000001
#define FLOAT_IS_ZERO(x)    ( (x)<=FLOAT_ZERO && (x) >=-FLOAT_ZERO)
#define FLOAT_BIGGER_ZERO(x)  ( (x)>FLOAT_ZERO )
#define FLOAT_SMALLER_ZERO(x)  ( (x)<-FLOAT_ZERO )
#define SCREEN_WIDTH (QApplication::desktop()->width())
#define SCREEN_HIGH  (QApplication::desktop()->height())
#define WINDOW_SIZE(radio_x,radio_y)  SCREEN_WIDTH*radio_x,SCREEN_HIGH*radio_y
#define WINDOW_GEOMETRY(radio_x,radio_y) (SCREEN_WIDTH-SCREEN_WIDTH*radio_x)/2,\
                                           (SCREEN_HIGH-SCREEN_HIGH*radio_y)/2,\
                                            WINDOW_SIZE(radio_x,radio_y)
#define WINDOW_GEOMETRY_SHIFT(radio_x,radio_y,y_pos) (SCREEN_WIDTH-SCREEN_WIDTH*radio_x)/2,\
    (SCREEN_HIGH*y_pos),WINDOW_SIZE(radio_x,radio_y)
#define HEIGHT(h_7,h_10)  ( (h_7)+(SCREEN_HIGH -480)/(320/((h_10)-(h_7))) )
#define DIALOG_WIDTH_RATIO 0.7
#define DIALOG_HEIGHT_RATIO 0.7
#define DIALOG_WIDTH SCREEN_WIDTH*DIALOG_WIDTH_RATIO
#define DIALOG_HIGH SCREEN_HIGH*DIALOG_HEIGHT_RATIO
#define DIALOG_X SCREEN_WIDTH*(1-DIALOG_WIDTH_RATIO)/2
#define DIALOG_Y SCREEN_HIGH*(1-DIALOG_HEIGHT_RATIO)/2

#define DIR_SETTINGS "setting.ini"
#define APP_CONFIG_FILE "config.ini"

//延时，TIME_OUT是串口读写的延时
#define TIME_OUT 50//50

//连续发送定时器计时间隔,200ms
#define OBO_TIMER_INTERVAL 200

//载入文件时，如果文件大小超过TIP_FILE_SIZE字节则提示文件过大是否继续打开
#define TIP_FILE_SIZE 5000
//载入文件最大长度限制在MAX_FILE_SIZE字节内
#define MAX_FILE_SIZE 10000

#define USB_PORT0 "1-1.5:1.0"
#define USB_PORT1 "1-1.4:1.0"
#define USB_PORT2 "1-1.2:1.0"

#define PROCESS_CONTROL //流程管控


extern QString toString(float d, int deci, int max_n=8);
extern float inputvalue_float;
extern int inputvalue_int;

#include <stdio.h>
static int inline TSystem(const char *command){
    int error;
    if((error=system(command))<0)
        printf("Error!System :%s error!",command);
    return error;
}
static int inline TSystem2(const QString &command){
    int error;
    if((error=system(command.toLatin1().data()))<0){
    }
    return error;
}

#endif // COMMON_H
